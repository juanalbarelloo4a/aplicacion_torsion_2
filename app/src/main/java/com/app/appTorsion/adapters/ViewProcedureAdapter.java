package com.app.appTorsion.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.app.appTorsion.R;
import com.app.appTorsion.models.AbstractProcedureActivity;

import java.util.List;

public class ViewProcedureAdapter extends PagerAdapter {

    private List<AbstractProcedureActivity> procedures;
    private LayoutInflater layoutInflater;
    private Context context;

    public ViewProcedureAdapter(List<AbstractProcedureActivity> procedures, Context context) {
        this.procedures = procedures;
        this.context = context;
    }

    @Override
    public int getCount() {
        return procedures.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_procedure, container, false);

        ImageView imageView;
        TextView title, desc;

        imageView = view.findViewById(R.id.procedureImage);
        title = view.findViewById(R.id.title);
        desc = view.findViewById(R.id.desc);

        imageView.setImageResource(procedures.get(position).getImage());
        title.setText(procedures.get(position).getTitle());
        desc.setText(Html.fromHtml(procedures.get(position).getDescription()));

        container.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }

}
