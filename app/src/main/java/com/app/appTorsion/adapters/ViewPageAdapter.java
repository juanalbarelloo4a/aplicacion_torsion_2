package com.app.appTorsion.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.app.appTorsion.R;
import com.app.appTorsion.activities.ExerciseActivity;
import com.app.appTorsion.models.Exercise;

import java.util.List;

public class ViewPageAdapter extends PagerAdapter {

    private List<Exercise> exercises;
    private LayoutInflater layoutInflater;
    private Context context;

    public ViewPageAdapter(List<Exercise> exercises, Context context) {
        this.exercises = exercises;
        this.context = context;
    }

    @Override
    public int getCount() {
        return exercises.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item, container, false);

        ImageView imageView;
        TextView title;

        imageView = view.findViewById(R.id.procedureImage);
        title = view.findViewById(R.id.title);


        imageView.setImageResource(exercises.get(position).getImage());
        title.setText(exercises.get(position).getTitle());


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(context, DetailActivity.class);
                //intent.putExtra("param", models.get(position).getTitle());
                //context.startActivity(intent);
                // finish();
                Intent iniciar = new Intent(context, ExerciseActivity.class);
                iniciar.putExtra("exercise", exercises.get(position));
                context.startActivity(iniciar);
            }
        });

        container.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }

}
