package com.app.appTorsion;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.app.appTorsion.activities.ExercisesActivity;
import com.app.appTorsion.opciones.Ayuda;
import com.app.appTorsion.opciones.EcuacionesTorsion;
import com.app.appTorsion.opciones.IntroduccionTorsion;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class ventanaInicial extends AppCompatActivity {

    GoogleSignInClient mGoogleSignInClient;

    private Button btnIntro;
    private Button btnEcuaciones;
    private Button btnEjemplo;
    private Button btnCasosEstudio;
    private Button btnAyuda;
    private Button btnCerrarSesion;
    private Button btnManual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_inicial);

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        btnIntro = findViewById(R.id.btnIntro);

        btnIntro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ventanaInicial.this, IntroduccionTorsion.class));
            }
        });

        btnEcuaciones = findViewById(R.id.btnEcuaciones);

        btnEcuaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ventanaInicial.this, EcuacionesTorsion.class));
            }
        });



        btnCasosEstudio = findViewById(R.id.btnCasosEstudio);

        btnCasosEstudio.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
                startActivity(new Intent(ventanaInicial.this, ExercisesActivity.class));
            }
        });


        btnAyuda = findViewById(R.id.btnAyuda);

        btnAyuda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ventanaInicial.this, Ayuda.class));
            }
        });

        btnCerrarSesion = findViewById(R.id.btnCerrarSesion);

        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signOut();
            }
        });

    }
    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(ventanaInicial.this,"Se ha cerrado la sesión",Toast.LENGTH_LONG).show();
                        startActivity(new Intent(ventanaInicial.this, MainActivity.class));
                        finish();
                    }
                });
    }

    // create an action bar button
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // R.menu.mymenu is a reference to an xml file named mymenu.xml which should be inside your res/menu directory.
        // If you don't have res/menu, just create a directory named "menu" inside res
        getMenuInflater().inflate(R.menu.actionbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.btnCerrarSesion) {
            signOut();
        }
        return super.onOptionsItemSelected(item);
    }


}