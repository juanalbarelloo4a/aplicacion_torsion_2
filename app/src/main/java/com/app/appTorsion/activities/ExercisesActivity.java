package com.app.appTorsion.activities;

import android.animation.ArgbEvaluator;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.app.appTorsion.R;
import com.app.appTorsion.adapters.ViewPageAdapter;
import com.app.appTorsion.models.Exercise;
import com.app.appTorsion.soluciones.solucionEj;
import com.app.appTorsion.soluciones.solucionEj2;
import com.app.appTorsion.soluciones.solucionEj3;
import com.app.appTorsion.soluciones.solucionEj4;
import com.app.appTorsion.soluciones.solucionEj5;
import com.app.appTorsion.soluciones.solucionEj6;

import java.util.ArrayList;
import java.util.List;

public class ExercisesActivity extends AppCompatActivity  {


    ViewPager viewPager;
    ViewPageAdapter adapter;
    List<Exercise> exercises;
    Integer[] colors = null;
    ArgbEvaluator argbEvaluator = new ArgbEvaluator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercises);


        exercises = new ArrayList<>();
        Exercise ex1 = new Exercise(
                R.drawable.fig1,
                "Caso de estudio 1",
                "Con respecto a la imagen, ingresar los datos para: \n" +
                        "Módulo 1: Hallar el par de Torsión T máximo que puede aplicarse en A, a partir del esfuerzo cortante permisible del Vástago sólido AB, su valor es 12 ksi, " +
                        "está hecho de acero y su diámetro es 1.5 in. La manga CD, su esfuerzo cortante permisible es de 7 ksi, y está hecha de latón. " +
                        "Los radios internos y radios externos de las secciones AB y BC, se obtienen de los diámetros de cada sección.",
                "Módulo 2: Hallar el esfuerzo cortante permisible y ángulos de torsión, a partir del esfuerzo cortante permisible de las secciones AB y BCD, " +
                        "rigidez de los materiales y longitudes de cada sección.",
                new solucionEj());
        exercises.add(ex1);

        Exercise ex2 = new Exercise(
                R.drawable.fig2,
                "Caso de estudio 2",
                "Con respecto a la imagen, ingresar los datos para: \n" +
                        "Módulo 1: Hallar los diámetros mínimos dAB y dBC de las secciones AB y BC, de un eje sólido, el cual está hecho de latón y su esfuerzo cortante permisible es de 55 MPa. ",
                "Módulo 2: Hallar el esfuerzo cortante permisible y ángulos de torsión, " +
                        "a partir del esfuerzo cortante permisible de las secciones AB y BCD, rigidez del material, radio interno, radio externo y longitudes de cada sección.",

                new solucionEj2());
        exercises.add(ex2);

        Exercise ex3 = new Exercise(
                R.drawable.fig3,
                "Caso de estudio 3",
                "Con respecto a la imagen, ingresar los datos para: \n" +
                        "Módulo 1: Hallar el máximo esfuerzo cortante en los ejes AB, BC y CD, a partir de los valores de los torques y radios de las secciones AB, BC y CD. " +
                        "Bajo condiciones normales de operación, el motor eléctrico ejerce un par de torsión de 2.8 kN*m en el eje AB, si se sabe que cada eje es sólido.",
                "Módulo 2: Hallar el valor de los torques, los diámetros y los ángulos de torsión, a partir del esfuerzo cortante permisible de las secciones AB, BC y CD, " +
                        "rigidez del material y longitudes de cada sección AB, BC y CD.",
                new solucionEj3());
        exercises.add(ex3);

        Exercise ex4 = new Exercise(
                R.drawable.fig4,
                "Caso de estudio 4",
                "Con respecto a la imagen, ingresar los datos para: \n" +
                        "Hallar los diámetros requeridos de los ejes AB y CD, fuerza y torque en engranes, si se sabe que el esfuerzo cortante permisible es de 8 ksi. " +
                        "A partir de los dos ejes sólidos y los engranes que se emplean para transmitir 16 hp desde el motor A hasta la máquina herramienta en D, a una velocidad de 1260 rpm. " +
                        "El torque, los caballos de fuerza (hp), la velocidad y los radios de engrane B y C, son datos requeridos.\n","",
                new solucionEj4());
        exercises.add(ex4);

        Exercise ex5 = new Exercise(
                R.drawable.fig5,
                "Caso de estudio 5",
                "Con respecto a la imagen, ingresar los datos para: \n" +
                        "Hallar esfuerzos cortantes permisibles, ángulos de torsión y desplazamiento en cualquier punto en la superficie en D. " +
                        "El ensamble está fabricado de acero A-36 y consiste en una barra solida de 20 mm de diámetro, la cual se encuentra fija en el interior de un tubo mediante un disco rígido en B. " +
                        "El tubo tiene un diámetro exterior de 40 mm y el grosor de la pared es de 5 mm. " +
                        "Los valores de los torques de las secciones ABC y BCD, los radios externos e internos y longitud de la sección ABC, radio y longitud de la sección BCD y la rigidez del material, " +
                        "son datos requeridos.\n","",
                new solucionEj5());
        exercises.add(ex5);

        Exercise ex6 = new Exercise(
                R.drawable.fig6,
                "Caso de estudio 6",
                "Con respecto a la imagen, ingresar los datos para: \n" +
                        "Hallar esfuerzos cortantes permisibles, ángulos de torsión, ángulo de giro en el extremo A del eje AB y ángulo de giro en el extremo E del eje EF, si se sabe que G = 77 GPa. " +
                        "Además, los tres ejes sólidos, cada uno con 18 mm de diámetro, se conectan mediante los engranes. Los valores de los torques A y B, los radios AB, CD y EF, " +
                        "los radios de engrane AB, CD y EF, las longitudes AB, CD y EF y la rigidez del material, son datos requeridos.\n","",
                new solucionEj6());
        exercises.add(ex6);

        adapter = new ViewPageAdapter(exercises, this);

        viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(adapter);
        viewPager.setPadding(130, 0, 130, 0);

        Integer[] colors_temp = {
                getResources().getColor(R.color.thorzionPrimary),
                getResources().getColor(R.color.thorzionPrimary),
                getResources().getColor(R.color.thorzionPrimary),
                getResources().getColor(R.color.thorzionPrimary),
                getResources().getColor(R.color.thorzionPrimary),
                getResources().getColor(R.color.thorzionPrimary)
        };

        colors = colors_temp;

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (position < (adapter.getCount() -1) && position < (colors.length - 1)) {
                    viewPager.setBackgroundColor(

                            (Integer) argbEvaluator.evaluate(
                                    positionOffset,
                                    colors[position],
                                    colors[position + 1]
                            )
                    );
                }

                else {
                    viewPager.setBackgroundColor(colors[colors.length - 1]);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

}