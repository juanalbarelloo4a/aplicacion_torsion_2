package com.app.appTorsion.activities;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.app.appTorsion.R;
import com.app.appTorsion.models.Exercise;
import com.app.appTorsion.soluciones.solucionEj;

public class ExerciseActivity extends AppCompatActivity {

    private Exercise exercise;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);
        exercise = (Exercise) getIntent().getSerializableExtra("exercise");

        ImageView imageViewExercise;
        TextView titleExercise, descExercise, desc2Exercise;

        imageViewExercise = findViewById(R.id.exerciseImage);
        titleExercise = findViewById(R.id.titleLabel);
        descExercise = findViewById(R.id.contentLabel);
        desc2Exercise = findViewById(R.id.content2Label);

        imageViewExercise.setImageResource(exercise.getImage());
        titleExercise.setText(exercise.getTitle());
        descExercise.setText(exercise.getDescription());
        desc2Exercise.setText(exercise.getDescription2());

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = exercise.getLayoutFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("exercise", exercise);
        fragment.setArguments(bundle);
        fragmentTransaction.add(R.id.fragmentContainer, fragment);
        fragmentTransaction.commit();

    }
}