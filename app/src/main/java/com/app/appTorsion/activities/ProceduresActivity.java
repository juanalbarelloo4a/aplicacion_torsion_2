package com.app.appTorsion.activities;

import android.animation.ArgbEvaluator;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.app.appTorsion.R;
import com.app.appTorsion.adapters.ViewPageAdapter;
import com.app.appTorsion.adapters.ViewProcedureAdapter;
import com.app.appTorsion.helpers.Thorzion;
import com.app.appTorsion.models.AbstractProcedureActivity;
import com.app.appTorsion.models.Exercise;
import com.app.appTorsion.models.ProcedureActivity;
import com.app.appTorsion.soluciones.solucionEj;
import com.app.appTorsion.soluciones.solucionEj2;
import com.app.appTorsion.soluciones.solucionEj3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProceduresActivity extends AppCompatActivity {

    ViewPager viewPager;
    ViewProcedureAdapter adapter;
    List<AbstractProcedureActivity> procedures;
    Integer[] colors = null;
    ArgbEvaluator argbEvaluator = new ArgbEvaluator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercises);

        Exercise exercise = (Exercise) getIntent().getSerializableExtra("exercise");

        procedures = exercise.getActivities();


        adapter = new ViewProcedureAdapter(procedures, this);

        viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(adapter);
        viewPager.setPadding(130, 0, 130, 0);

        Integer[] colors_temp = {
                getResources().getColor(R.color.thorzionPrimary),
                getResources().getColor(R.color.thorzionPrimary),
                getResources().getColor(R.color.thorzionPrimary),
                getResources().getColor(R.color.thorzionPrimary),
                getResources().getColor(R.color.thorzionPrimary),
                getResources().getColor(R.color.thorzionPrimary)
        };

        colors = colors_temp;

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (position < (adapter.getCount() -1) && position < (colors.length - 1)) {
                    viewPager.setBackgroundColor(

                            (Integer) argbEvaluator.evaluate(
                                    positionOffset,
                                    colors[position],
                                    colors[position + 1]
                            )
                    );
                }

                else {
                    viewPager.setBackgroundColor(colors[colors.length - 1]);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }
}