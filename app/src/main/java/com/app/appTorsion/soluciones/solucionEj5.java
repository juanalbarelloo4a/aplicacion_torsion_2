 package com.app.appTorsion.soluciones;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.app.appTorsion.R;
import com.app.appTorsion.activities.ExercisesActivity;
import com.app.appTorsion.activities.ProceduresActivity;
import com.app.appTorsion.helpers.Thorzion;
import com.app.appTorsion.models.Exercise;
import com.app.appTorsion.models.ProcedureActivity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class solucionEj5 extends Fragment implements Serializable {
    Button atras;
    Button resolver;
    Button solucion;

    EditText txtTorqueEj5, txtTorque2Ej5, txtRadioEj5, txtRadioExtEj5, txtRadioIntEj5, txtLongEj5, txtLong2Ej5, txtRigEj5;

    TextView lblTorqueEj5, lblRadioEj5, lblLongsEj5, lblRigEj5;
    TextView lblTorque2Ej5, lblRadio2Ej5, lblLongs2Ej5, lblRadio3Ej5;

    TextView txtSol1Ej5, txtSol2Ej5, txtSol3Ej5, txtSol4Ej5, txtSol5Ej5, txtSol6Ej5;

    Spinner opciones, orientacionCaso1, orientacionCaso2;
    Spinner opciones2;

    private Exercise exercise;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_solucion_ej5, container, false);
        exercise = (Exercise) getArguments().getSerializable("exercise");

        txtSol1Ej5 = (TextView) view.findViewById(R.id.txtSol1Ej5);
        txtSol2Ej5 = (TextView) view.findViewById(R.id.txtSol2Ej5);
        txtSol3Ej5 = (TextView) view.findViewById(R.id.txtSol3Ej5);
        txtSol4Ej5 = (TextView) view.findViewById(R.id.txtSol4Ej5);
        txtSol5Ej5 = (TextView) view.findViewById(R.id.txtSol5Ej5);
        txtSol6Ej5 = (TextView) view.findViewById(R.id.txtSol6Ej5);

        txtTorqueEj5 = (EditText) view.findViewById(R.id.txtTorqueEj5);
        txtTorque2Ej5 = (EditText) view.findViewById(R.id.txtTorque2Ej5);
        txtRadioEj5 = (EditText) view.findViewById(R.id.txtRadioEj5);
        txtRadioExtEj5 = (EditText) view.findViewById(R.id.txtRadioExtEj5);
        txtRadioIntEj5 = (EditText) view.findViewById(R.id.txtRadioIntEj5);
        txtLongEj5 = (EditText) view.findViewById(R.id.txtLongEj5);
        txtLong2Ej5 = (EditText) view.findViewById(R.id.txtLong2Ej5);
        txtRigEj5 = (EditText) view.findViewById(R.id.txtRigEj5);

        lblTorqueEj5 = (TextView) view.findViewById(R.id.lblTorqueEj5);
        lblRadioEj5 = (TextView) view.findViewById(R.id.lblRadioEj5);
        lblLongsEj5 = (TextView) view.findViewById(R.id.lblLongsEj5);
        lblTorque2Ej5 = (TextView) view.findViewById(R.id.lblTorque2Ej5);
        lblRadio2Ej5 = (TextView) view.findViewById(R.id.lblRadio2Ej5);
        lblLongs2Ej5 = (TextView) view.findViewById(R.id.lblLongs2Ej5);
        lblRadio3Ej5 = (TextView) view.findViewById(R.id.lblRadio3Ej5);
        lblRigEj5 = (TextView) view.findViewById(R.id.lblRigEj5);

        resolver = (Button) view.findViewById(R.id.btnRes5);
        solucion = (Button) view.findViewById(R.id.btnSol5);

        resolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                exercise.setActivities(new ArrayList<>());

                double torqueEj5 = Double.parseDouble(txtTorqueEj5.getText().toString());
                double torque2Ej5 = Double.parseDouble(txtTorque2Ej5.getText().toString());
                double radioEj5 = Double.parseDouble(txtRadioEj5.getText().toString());
                double radioExtEj5 = Double.parseDouble(txtRadioExtEj5.getText().toString());
                double radioIntEj5 = Double.parseDouble(txtRadioIntEj5.getText().toString());
                double longEj5 = Double.parseDouble(txtLongEj5.getText().toString());
                double long2Ej5 = Double.parseDouble(txtLong2Ej5.getText().toString());
                double rigEj5 = Double.parseDouble(txtRigEj5.getText().toString());

                torqueEj5 = orientacionCaso1.getSelectedItem().toString().equals(orientacionCaso2.getSelectedItem().toString())?
                        torqueEj5+torque2Ej5:
                        torqueEj5-torque2Ej5;

                String nomneclatureAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.ANGLE_UNITS.RAD.getNomenclature():Thorzion.ANGLE_UNITS.DEG.getNomenclature();

                double angleSum = orientacionCaso1.getSelectedItem().toString().equals(orientacionCaso2.getSelectedItem().toString())?
                        Thorzion.getSumAngles(Thorzion.getThorzionAngleRigid(torqueEj5, longEj5, radioExtEj5, radioIntEj5, rigEj5),
                                Thorzion.getThorzionAngleRigid(torque2Ej5, long2Ej5, radioEj5 ,rigEj5)):
                        Thorzion.getSumAnglesRest(Thorzion.getThorzionAngleRigid(torqueEj5, longEj5, radioExtEj5, radioIntEj5, rigEj5),
                                Thorzion.getThorzionAngleRigid(torque2Ej5, long2Ej5, radioEj5 ,rigEj5));

                double unidadAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torqueEj5, longEj5, radioExtEj5, radioIntEj5, rigEj5):Thorzion.getThorzionAngleRigid(torqueEj5, longEj5, radioExtEj5, radioIntEj5, rigEj5)*180/Math.PI;

                double unidadAngle2 = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torque2Ej5, long2Ej5, radioEj5 ,rigEj5):Thorzion.getThorzionAngleRigid(torque2Ej5, long2Ej5, radioEj5 ,rigEj5)*180/Math.PI;

                double anglesumSent = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        angleSum:angleSum*180/Math.PI;


                double angleSumD = angleSum < 0?
                    Thorzion.getDespPoint(radioEj5, (angleSum*-1)):
                    Thorzion.getDespPoint(radioEj5, (angleSum*1));

                double angleSumDSent = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        angleSumD:angleSumD*180/Math.PI;

                DecimalFormat format = new DecimalFormat("##.###E0");
                DecimalFormat format2 = new DecimalFormat("#.###");
                DecimalFormat formatD = new DecimalFormat("#.######");
                DecimalFormat format3 = new DecimalFormat("#.######");


                String nomneclatureDistance = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getMeterNomenclature():Thorzion.getFootNomenclature();




                String nomneclaturePresure = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature():Thorzion.PRESSURE_UNITS.KSI.getNomenclature();

                ProcedureActivity paso1 = new ProcedureActivity(
                        "Hallar τ en la sección ABC",
                        "Procedemos a reemplazar los valores en la ecuación <b> T = ("+torqueEj5 + " " +lblTorqueEj5.getText()+", Cext = "+radioExtEj5 + " " + lblRadioEj5.getText() +", Cint = "+   radioIntEj5 +
                                lblRadioEj5.getText() + ", J = " + format.format(Thorzion.getInertiaConstant(radioExtEj5,radioIntEj5)) + lblRadioEj5.getText()+"^4 + " +
                                "</b>, " + "obteniendo como resultado un Esfuerzo Cortante Permisible = <b>"+
                                format.format(Thorzion.getTaoMaximus(torqueEj5,radioExtEj5,radioIntEj5)) + " " + nomneclaturePresure+ " En la sección ABC"+ "</b>",
                        R.drawable.ec2);

                ProcedureActivity paso2 = new ProcedureActivity(
                        "Hallar τ en la sección BCD",
                                "Procedemos a reemplazar los valores en la ecuación <b> T = "+torque2Ej5 + " " +lblTorqueEj5.getText()+", C = "+ radioEj5+ "" +
                                        lblRadioEj5.getText() +""+ lblRadioEj5.getText() + ", J = " + format.format(Thorzion.getInertiaConstant(radioEj5)) + lblRadioEj5.getText()+"^4 + " +
                                        "</b>, " + "obteniendo como resultado un Esfuerzo Cortante Permisible = <b>"+
                                        format.format(Thorzion.getTaoMaximus(torque2Ej5,radioEj5))  +" "+  nomneclaturePresure + " En la sección BCD" + "</b>",
                                R.drawable.ec1);

                ProcedureActivity paso3 = new ProcedureActivity(
                        "Hallar ángulo de torsión en la sección ABC",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T ="+torqueEj5 + " " +lblTorqueEj5.getText()+", L = " + longEj5 + "" + lblLongsEj5.getText() +
                                ", J = " + format.format(Thorzion.getInertiaConstant(radioExtEj5,radioIntEj5)) + lblRadioEj5.getText()+ ", G = " + rigEj5 + ""+ lblRigEj5.getText()+""+ " </b>, " +
                                "obteniendo como resultado un ángulo = <b>"+ format2.format(unidadAngle)  + " " +nomneclatureAngle+" En la sección ABC" + "</b>",
                        R.drawable.ec5);

                ProcedureActivity paso4 = new ProcedureActivity(
                        "Hallar ángulo de torsión en la sección BCD",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T ="+torque2Ej5 + " " +lblTorqueEj5.getText()+", L = " + long2Ej5 + "" + lblLongsEj5.getText() +
                                ", J = " + format.format(Thorzion.getInertiaConstant(radioEj5)) + lblRadioEj5.getText()+ ", G = " + rigEj5 + ""+ lblRigEj5.getText()+""+ " </b>, " +
                                "obteniendo como resultado un ángulo = <b>"+ format2.format(unidadAngle2)  + " "+nomneclatureAngle + " En la sección BCD" + "</b>",
                        R.drawable.ec5);

                ProcedureActivity paso5 = new ProcedureActivity(
                        "Hallar ángulo de torsión en el punto D",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> Φab ="+unidadAngle+ " " +nomneclatureAngle+", " +
                                "Φbd = " + unidadAngle2  + " " + nomneclatureAngle + " </b>, " +
                                "obteniendo como resultado un ángulo = <b>"+ format2.format(anglesumSent) +" "+nomneclatureAngle+ " En el punto D"+"</b>",
                        R.drawable.ec6);

                ProcedureActivity paso6 = new ProcedureActivity(
                        "Hallar desplazamiento en cualquier punto en la superficie de D",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b>  C = "+ radioEj5+ "" + lblRadioEj5.getText() +""+", Φd = " + anglesumSent +" "+ nomneclatureAngle + " </b>, " +
                                "obteniendo como resultado un que el desplazamiento es igual a = <b>"+ formatD.format(angleSumDSent) +" "+nomneclatureDistance+ " En la superficie de D"+"</b>",


                        R.drawable.ec7);

                exercise.addActivity(paso1);
                exercise.addActivity(paso2);
                exercise.addActivity(paso3);
                exercise.addActivity(paso4);
                exercise.addActivity(paso5);
                exercise.addActivity(paso6);

                Intent iniciar = new Intent(getActivity(), ProceduresActivity.class);
                exercise.setLayoutID(null);
                iniciar.putExtra("exercise", exercise);
                startActivity(iniciar);
            }
        });

        solucion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                exercise.setActivities(new ArrayList<>());

                double torqueEj5 = Double.parseDouble(txtTorqueEj5.getText().toString());
                double torque2Ej5 = Double.parseDouble(txtTorque2Ej5.getText().toString());
                double radioEj5 = Double.parseDouble(txtRadioEj5.getText().toString());
                double radioExtEj5 = Double.parseDouble(txtRadioExtEj5.getText().toString());
                double radioIntEj5 = Double.parseDouble(txtRadioIntEj5.getText().toString());
                double longEj5 = Double.parseDouble(txtLongEj5.getText().toString());
                double long2Ej5 = Double.parseDouble(txtLong2Ej5.getText().toString());
                double rigEj5 = Double.parseDouble(txtRigEj5.getText().toString());

                torqueEj5 = orientacionCaso1.getSelectedItem().toString().equals(orientacionCaso2.getSelectedItem().toString())?
                        torqueEj5+torque2Ej5:
                        torqueEj5-torque2Ej5;

                double angleSum = orientacionCaso1.getSelectedItem().toString().equals(orientacionCaso2.getSelectedItem().toString())?
                        Thorzion.getSumAngles(Thorzion.getThorzionAngleRigid(torqueEj5, longEj5, radioExtEj5, radioIntEj5, rigEj5),
                                Thorzion.getThorzionAngleRigid(torque2Ej5, long2Ej5, radioEj5 ,rigEj5)):
                        Thorzion.getSumAnglesRest(Thorzion.getThorzionAngleRigid(torqueEj5, longEj5, radioExtEj5, radioIntEj5, rigEj5),
                                Thorzion.getThorzionAngleRigid(torque2Ej5, long2Ej5, radioEj5 ,rigEj5));

                String nomneclatureAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.ANGLE_UNITS.RAD.getNomenclature():Thorzion.ANGLE_UNITS.DEG.getNomenclature();

                double unidadAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torqueEj5, longEj5, radioExtEj5, radioIntEj5, rigEj5):Thorzion.getThorzionAngleRigid(torqueEj5, longEj5, radioExtEj5, radioIntEj5, rigEj5)*180/Math.PI;

                double unidadAngle2 = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torque2Ej5, long2Ej5, radioEj5 ,rigEj5):Thorzion.getThorzionAngleRigid(torque2Ej5, long2Ej5, radioEj5 ,rigEj5)*180/Math.PI;

                double anglesumSent = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        angleSum:angleSum*180/Math.PI;


                double angleSumD = angleSum < 0?
                        Thorzion.getDespPoint(radioEj5, (angleSum*-1)):
                        Thorzion.getDespPoint(radioEj5, (angleSum*1));

                double angleSumDSent = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        angleSumD:angleSumD*180/Math.PI;

                DecimalFormat format = new DecimalFormat("##.###E0");
                DecimalFormat format2 = new DecimalFormat("#.###");
                DecimalFormat formatD = new DecimalFormat("#.######");

                String nomneclatureDistance = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getMeterNomenclature():Thorzion.getFootNomenclature();

                String nomneclaturePresure = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature():Thorzion.PRESSURE_UNITS.KSI.getNomenclature();


                txtSol1Ej5.setText(" R/: " + format.format(Thorzion.getTaoMaximus(torqueEj5,radioExtEj5,radioIntEj5)) + " " + nomneclaturePresure+ " Para el esfuerzo cortante permisible en la sección ABC");
                txtSol2Ej5.setText(" R/: " + format.format(Thorzion.getTaoMaximus(torque2Ej5,radioEj5))  +" "+  nomneclaturePresure + " Para el esfuerzo cortante permisible en la sección BCD");
                txtSol3Ej5.setText(" R/: " + format2.format(unidadAngle)  + " " +nomneclatureAngle + " Para el angulo en la sección ABC");
                txtSol4Ej5.setText(" R/: " + format2.format(unidadAngle2) + " " +nomneclatureAngle + " Para el angulo en la sección BC");
                txtSol5Ej5.setText(" R/: " + format2.format(anglesumSent) +" "+nomneclatureAngle+ " Para el angulo en D");
                txtSol6Ej5.setText(" R/: " + formatD.format(angleSumDSent) +" "+nomneclatureDistance + " Para el desplazamiento en cualquier punto en D");



                exercise.setLayoutID(null);
            }
        });

        atras = (Button) view.findViewById(R.id.btnAtr5);

        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent atras = new Intent(getActivity(), ExercisesActivity.class);
                exercise.setLayoutID(null);
                atras.putExtra("exercise", exercise);
                startActivity(atras);

            }
        });

        opciones = (Spinner) view.findViewById(R.id.opcSistEj5);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.opciones, android.R.layout.simple_spinner_item);
        opciones.setAdapter(adapter);

        opciones2 = (Spinner) view.findViewById(R.id.opcMedEj5);

        orientacionCaso1 =(Spinner)view.findViewById(R.id.opcOriTuboEj5);
        ArrayAdapter<CharSequence> adapterC1 = ArrayAdapter.createFromResource(getActivity(), R.array.SentidoTorque, android.R.layout.simple_spinner_item);
        orientacionCaso1.setAdapter(adapterC1);

        orientacionCaso2 =(Spinner)view.findViewById(R.id.opcOriBarraEj5);
        ArrayAdapter<CharSequence> adapterC2 = ArrayAdapter.createFromResource(getActivity(), R.array.SentidoTorque, android.R.layout.simple_spinner_item);
        orientacionCaso2.setAdapter(adapterC2);

        // Inflate the layout for this fragment
        return view;
    }
         @Override
        public void onStart () {
            super.onStart();
            this.opciones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    // your code here

                    double torqueEj5 = Double.parseDouble(txtTorqueEj5.getText().toString());
                    txtTorqueEj5.setText("0", TextView.BufferType.EDITABLE);
                    double torque2Ej5 = Double.parseDouble(txtTorque2Ej5.getText().toString());
                    txtTorque2Ej5.setText("0", TextView.BufferType.EDITABLE);
                    txtRadioEj5.setText("0", TextView.BufferType.EDITABLE);
                    txtRadioExtEj5.setText("0", TextView.BufferType.EDITABLE);
                    txtRadioIntEj5.setText("0", TextView.BufferType.EDITABLE);


                    if (position == 0) {
                        torqueEj5 = Thorzion.TORQUE_UNITS.POUND_INCH.unitToInternational(torqueEj5);

                        lblTorqueEj5.setText(Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature());
                        lblTorque2Ej5.setText(Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature());
                        lblRigEj5.setText(Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature());

                        opciones2 = (Spinner) getView().findViewById(R.id.opcMedEj5);
                        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.metric, android.R.layout.simple_spinner_item);
                        opciones2.setAdapter(adapter2);
                        opciones2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                double radioEj5 = Double.parseDouble(txtRadioEj5.getText().toString());
                                double radioExtEj5 = Double.parseDouble(txtRadioExtEj5.getText().toString());
                                double radioIntEj5 = Double.parseDouble(txtRadioIntEj5.getText().toString());
                                double longEj5 = Double.parseDouble(txtLongEj5.getText().toString());
                                double long2Ej5 = Double.parseDouble(txtLong2Ej5.getText().toString());
                                double rigEj5 = Double.parseDouble(txtRigEj5.getText().toString());

                                DecimalFormat numberFormat = new DecimalFormat("#.####");
                                switch (position) {
                                    case 0:
                                        radioEj5 = Thorzion.milimeterToMeter(radioEj5);
                                        radioExtEj5 = Thorzion.milimeterToMeter(radioExtEj5);
                                        radioIntEj5 = Thorzion.milimeterToMeter(radioIntEj5);
                                        longEj5 = Thorzion.milimeterToMeter(longEj5);
                                        long2Ej5 = Thorzion.milimeterToMeter(long2Ej5);

                                        txtRadioEj5.setText(numberFormat.format(radioEj5), TextView.BufferType.EDITABLE);
                                        txtRadioExtEj5.setText(numberFormat.format(radioExtEj5), TextView.BufferType.EDITABLE);
                                        txtRadioIntEj5.setText(numberFormat.format(radioIntEj5), TextView.BufferType.EDITABLE);
                                        txtLongEj5.setText(numberFormat.format(longEj5), TextView.BufferType.EDITABLE);
                                        txtLong2Ej5.setText(numberFormat.format(long2Ej5), TextView.BufferType.EDITABLE);

                                        lblRadioEj5.setText(Thorzion.getMeterNomenclature());
                                        lblLongsEj5.setText(Thorzion.getMeterNomenclature());
                                        lblRadio2Ej5.setText(Thorzion.getMeterNomenclature());
                                        lblLongs2Ej5.setText(Thorzion.getMeterNomenclature());
                                        lblRadio3Ej5.setText(Thorzion.getMeterNomenclature());


                                        break;
                                    case 1:
                                        radioEj5 = Thorzion.meterToMilimeter(radioEj5);
                                        radioExtEj5 = Thorzion.meterToMilimeter(radioExtEj5);
                                        radioIntEj5 = Thorzion.meterToMilimeter(radioIntEj5);
                                        longEj5 = Thorzion.meterToMilimeter(longEj5);
                                        long2Ej5 = Thorzion.meterToMilimeter(long2Ej5);

                                        txtRadioEj5.setText(numberFormat.format(radioEj5), TextView.BufferType.EDITABLE);
                                        txtRadioExtEj5.setText(numberFormat.format(radioExtEj5), TextView.BufferType.EDITABLE);
                                        txtRadioIntEj5.setText(numberFormat.format(radioIntEj5), TextView.BufferType.EDITABLE);
                                        txtLongEj5.setText(numberFormat.format(longEj5), TextView.BufferType.EDITABLE);
                                        txtLong2Ej5.setText(numberFormat.format(long2Ej5), TextView.BufferType.EDITABLE);

                                        lblRadioEj5.setText(Thorzion.getMiliMeterNomenclature());
                                        lblLongsEj5.setText(Thorzion.getMiliMeterNomenclature());
                                        lblRadio2Ej5.setText(Thorzion.getMiliMeterNomenclature());
                                        lblLongs2Ej5.setText(Thorzion.getMiliMeterNomenclature());
                                        lblRadio3Ej5.setText(Thorzion.getMiliMeterNomenclature());

                                        break;
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } else if (position == 1) {
                        torqueEj5 = Thorzion.TORQUE_UNITS.NEWTON_METER.unitToImperial(torqueEj5);

                        TextView lblTorqueEj5 = (TextView) getView().findViewById(R.id.lblTorqueEj5);
                        lblTorqueEj5.setText(Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature());
                        TextView lblTorque2Ej5 = (TextView) getView().findViewById(R.id.lblTorque2Ej5);
                        lblTorque2Ej5.setText(Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature());
                        lblRigEj5.setText(Thorzion.PRESSURE_UNITS.KSI.getNomenclature());

                        opciones2 = (Spinner) getView().findViewById(R.id.opcMedEj5);
                        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.ingles, android.R.layout.simple_spinner_item);
                        opciones2.setAdapter(adapter2);

                        opciones2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                double radioEj5 = Double.parseDouble(txtRadioEj5.getText().toString());
                                double radioExtEj5 = Double.parseDouble(txtRadioExtEj5.getText().toString());
                                double radioIntEj5 = Double.parseDouble(txtRadioIntEj5.getText().toString());
                                double longEj5 = Double.parseDouble(txtLongEj5.getText().toString());
                                double long2Ej5 = Double.parseDouble(txtLong2Ej5.getText().toString());

                                DecimalFormat numberFormat = new DecimalFormat("#.####");
                                switch (position) {
                                    case 0:
                                        radioEj5 = Thorzion.inchToFoot(radioEj5);
                                        radioExtEj5 = Thorzion.inchToFoot(radioExtEj5);
                                        radioIntEj5 = Thorzion.inchToFoot(radioIntEj5);
                                        longEj5 = Thorzion.inchToFoot(longEj5);
                                        long2Ej5 = Thorzion.inchToFoot(long2Ej5);

                                        txtRadioEj5.setText(numberFormat.format(radioEj5), TextView.BufferType.EDITABLE);
                                        txtRadioExtEj5.setText(numberFormat.format(radioExtEj5), TextView.BufferType.EDITABLE);
                                        txtRadioIntEj5.setText(numberFormat.format(radioIntEj5), TextView.BufferType.EDITABLE);
                                        txtLongEj5.setText(numberFormat.format(longEj5), TextView.BufferType.EDITABLE);
                                        txtLong2Ej5.setText(numberFormat.format(long2Ej5), TextView.BufferType.EDITABLE);

                                        lblRadioEj5.setText(Thorzion.getFootNomenclature());
                                        lblLongsEj5.setText(Thorzion.getFootNomenclature());
                                        lblRadio2Ej5.setText(Thorzion.getFootNomenclature());
                                        lblLongs2Ej5.setText(Thorzion.getFootNomenclature());
                                        lblRadio3Ej5.setText(Thorzion.getFootNomenclature());

                                        break;
                                    case 1:
                                        radioEj5 = Thorzion.footToInch(radioEj5);
                                        radioExtEj5 = Thorzion.footToInch(radioExtEj5);
                                        radioIntEj5 = Thorzion.footToInch(radioIntEj5);
                                        longEj5 = Thorzion.footToInch(longEj5);
                                        long2Ej5 = Thorzion.footToInch(long2Ej5);

                                        txtRadioEj5.setText(numberFormat.format(radioEj5), TextView.BufferType.EDITABLE);
                                        txtRadioExtEj5.setText(numberFormat.format(radioExtEj5), TextView.BufferType.EDITABLE);
                                        txtRadioIntEj5.setText(numberFormat.format(radioIntEj5), TextView.BufferType.EDITABLE);
                                        txtLongEj5.setText(numberFormat.format(longEj5), TextView.BufferType.EDITABLE);
                                        txtLong2Ej5.setText(numberFormat.format(long2Ej5), TextView.BufferType.EDITABLE);

                                        lblRadioEj5.setText(Thorzion.getInchNomenclature());
                                        lblLongsEj5.setText(Thorzion.getInchNomenclature());
                                        lblRadio2Ej5.setText(Thorzion.getInchNomenclature());
                                        lblLongs2Ej5.setText(Thorzion.getInchNomenclature());
                                        lblRadio3Ej5.setText(Thorzion.getInchNomenclature());

                                        break;
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }

                    txtTorqueEj5.setText(torqueEj5 + "", TextView.BufferType.EDITABLE);
                    txtTorque2Ej5.setText(torque2Ej5 + "", TextView.BufferType.EDITABLE);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }
            });
    }
}
