package com.app.appTorsion.soluciones;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.app.appTorsion.R;
import com.app.appTorsion.activities.ExercisesActivity;
import com.app.appTorsion.activities.ProceduresActivity;
import com.app.appTorsion.helpers.Thorzion;
import com.app.appTorsion.models.Exercise;
import com.app.appTorsion.models.ProcedureActivity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class solucionEj4_1 extends Fragment implements Serializable {

    Button atras;
    Button resolver;
    Button solucion;

    EditText txtTaoEj4, txtHP, txtVel, txtRadioEngB, txtRadioEngC;

    TextView lblTaoEj4Nom, lblHPNom, lblVelNom, lblRadiosNom;
    TextView txtSol4, txtSol4_B,txtSol4_C,txtSol4_D;

    Spinner opciones4;
    Spinner opciones2;

    private Exercise exercise;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_solucion_ej4_1, container, false);
        exercise = (Exercise) getArguments().getSerializable("exercise");

        txtSol4 = (TextView) view.findViewById(R.id.txtSol4);
        txtSol4_B = (TextView) view.findViewById(R.id.txtSol4_B);
        txtSol4_C = (TextView) view.findViewById(R.id.txtSol4_C);
        txtSol4_D = (TextView) view.findViewById(R.id.txtSol4_D);

        txtTaoEj4 = (EditText) view.findViewById(R.id.txtTaoEj4);
        txtHP = (EditText) view.findViewById(R.id.txtHP);
        txtVel = (EditText) view.findViewById(R.id.txtVel);
        txtRadioEngB = (EditText) view.findViewById(R.id.txtRadioEngB);
        txtRadioEngC = (EditText) view.findViewById(R.id.txtRadioEngC);

        lblTaoEj4Nom = (TextView) view.findViewById(R.id.lblTaoEj4Nom);
        lblHPNom = (TextView) view.findViewById(R.id.lblHPNom);
        lblVelNom = (TextView) view.findViewById(R.id.lblVelNom);
        lblRadiosNom = (TextView) view.findViewById(R.id.lblRadiosNom);

        resolver = (Button) view.findViewById(R.id.btnRes4);
        solucion = (Button) view.findViewById(R.id.btnSol4);

        resolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                exercise.setActivities(new ArrayList<>());

                double taoEj4 = Double.parseDouble(txtTaoEj4.getText().toString());
                double HP = Double.parseDouble(txtHP.getText().toString());
                double Vel = Double.parseDouble(txtVel.getText().toString());
                double radioEngB = Double.parseDouble(txtRadioEngB.getText().toString());
                double radioEngC = Double.parseDouble(txtRadioEngC.getText().toString());

                String nomneclatureTorque = Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature();

                String nomneclatureDistance = opciones4.getSelectedItem().toString().equals("S.Ingles")?
                        Thorzion.getMiliMeterNomenclature():Thorzion.getMiliMeterNomenclature();

                DecimalFormat formato = new DecimalFormat();
                formato.setMaximumFractionDigits(1); //Numero maximo de decimales a mostrar

                ProcedureActivity paso1 = new ProcedureActivity(
                        "Obtener Torque generado por motor",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera, P =" + Thorzion.getClearPower(HP) + " " + lblHPNom.getText() + ", W = " + Thorzion.getAngularSpeed(Vel) + " " + lblVelNom.getText()
                                + "obteniendo un resultado de T = " +formato.format(Thorzion.getClearTorque(Thorzion.getClearPower(HP),Thorzion.getAngularSpeed(Vel))) + " " + nomneclatureTorque,
                        R.drawable.ec3);


                ProcedureActivity paso2 = new ProcedureActivity(
                        "Obtener diametro eje AB",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b>T=(" + taoEj4 + " " + lblTaoEj4Nom.getText() +
                                " T = " + Thorzion.getTorquePwASp(HP,Thorzion.getAngularSpeed(Vel)) + "" + nomneclatureTorque +
                                " </b>, obteniendo como resultado un <b> d = " +
                                formato.format(Thorzion.getDiameter(Thorzion.getRadius(taoEj4, Thorzion.getTorquePwASp(Thorzion.getClearPower(HP),Thorzion.getAngularSpeed(Vel)))))+ nomneclatureDistance+"</b>",
                        R.drawable.ec3);
/*
                ProcedureActivity paso3 = new ProcedureActivity(
                        "Análisis Resultados",
                        "Se debe tener en cuenta que es necesario obtener el menor torque posible por medio del esfuerzo cortante de ambos materiales, " +
                                "el cual al ser aplicado tanto en el vástago de acero, como en la manga de latón," +
                                " no provoque que alguno de los dos materiales sufra una rotura o deformación al verse sometido a un torque excesivo",

                        R.drawable.ec3);

                ProcedureActivity paso4 = new ProcedureActivity(
                        "Resultado y conclusión",
                        "Obteniendo ambos resultados, podemos concluir que el Torque que satisface al sistema para el vástago de acero y la manga de latón es igual a: " +
                                ((Thorzion.getTorque(tao,radioExterno,radioInterno) > Thorzion.getTorque(taoCaso2,radioExternoCaso2,radioInternoCaso2))?
                                        ""+ String.format("%.2f", Thorzion.getTorque(taoCaso2,radioExternoCaso2,radioInternoCaso2)):
                                        ""+ String.format("%.2f", Thorzion.getTorque(tao,radioExterno,radioInterno))) + " " + nomneclatureTorque,
                        R.drawable.ec3);
*/
                exercise.addActivity(paso1);
                exercise.addActivity(paso2);
                //exercise.addActivity(paso3);
                //exercise.addActivity(paso4);

                Intent iniciar = new Intent(getContext(), ProceduresActivity.class);
                exercise.setLayoutID(null);
                iniciar.putExtra("exercise", exercise);
                startActivity(iniciar);
            }
        });



        solucion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                exercise.setActivities(new ArrayList<>());

                double taoEj4 = Double.parseDouble(txtTaoEj4.getText().toString());
                double HP = Double.parseDouble(txtHP.getText().toString());
                double Vel = Double.parseDouble(txtVel.getText().toString());
                double radioEngB = Double.parseDouble(txtRadioEngB.getText().toString());
                double radioEngC = Double.parseDouble(txtRadioEngC.getText().toString());

                String nomneclatureTorque = Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature();

                String nomneclatureDistance = opciones4.getSelectedItem().toString().equals("S.Ingles")?
                        Thorzion.getMiliMeterNomenclature():Thorzion.getMiliMeterNomenclature();

                String nomneclatureForce = Thorzion.FORCE_UNITS.N.getNomenclature();

                DecimalFormat formato = new DecimalFormat();
                formato.setMaximumFractionDigits(2); //Numero maximo de decimales a mostrar


                txtSol4.setText(" R/: " + formato.format(Thorzion.getDiameter(Thorzion.getRadius(taoEj4,
                        Thorzion.getTorquePwASp(Thorzion.getClearPower(HP),Thorzion.getAngularSpeed(Vel))))) + " " + nomneclatureDistance + " Diametro en AB");
                txtSol4_B.setText(" R/: " + formato.format(Thorzion.getForce(Thorzion.getTorquePwASp
                        (Thorzion.getClearPower(HP),Thorzion.getAngularSpeed(Vel)),radioEngB)) + " " + nomneclatureForce + " Fuerza en B y en C");
                txtSol4_C.setText(" R/: " + formato.format(Thorzion.getTorqueMesh(Thorzion.getForce
                        (Thorzion.getTorquePwASp(Thorzion.getClearPower(HP),Thorzion.getAngularSpeed(Vel)),radioEngB),radioEngC)) + " " + nomneclatureTorque + " Torque en engrane C");
                txtSol4_D.setText(" R/: " + formato.format(Thorzion.getDiameter(Thorzion.getRadius(taoEj4,Thorzion.getTorqueMesh(Thorzion.getForce
                        (Thorzion.getTorquePwASp(Thorzion.getClearPower(HP),Thorzion.getAngularSpeed(Vel)),radioEngB),radioEngC)))) + " " + nomneclatureDistance + " Diametro en BC");

                exercise.setLayoutID(null);

            }
        });

        atras = (Button) view.findViewById(R.id.btnAtr4);

        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent atras = new Intent(getActivity(), ExercisesActivity.class);
                exercise.setLayoutID(null);
                atras.putExtra("exercise", exercise);
                startActivity(atras);

            }
        });


        opciones4 =(Spinner)view.findViewById(R.id.opcSistEj4_1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.opciones4, android.R.layout.simple_spinner_item);
        opciones4.setAdapter(adapter);

        opciones2 =(Spinner)view.findViewById(R.id.opcMedEj4_1);

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onStart(){
        super.onStart();
        this.opciones4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here

                double tao4 = Double.parseDouble(txtTaoEj4.getText().toString());

                txtRadioEngB.setText("0", TextView.BufferType.EDITABLE);
                txtRadioEngC.setText("0", TextView.BufferType.EDITABLE);
                txtHP.setText("0", TextView.BufferType.EDITABLE);
                txtVel.setText("0", TextView.BufferType.EDITABLE);

                lblHPNom.setText(Thorzion.POWER_UNITS.HP.getNomenclature());
                lblVelNom.setText(Thorzion.SPEED_UNITS.RPM.getNomenclature());

                if(position == 0){
                    tao4 = Thorzion.PRESSURE_UNITS.KSI.unitToInternational(tao4);

                    TextView lblTaoEj4Nom = (TextView) getView().findViewById(R.id.lblTaoEj4Nom);
                    lblTaoEj4Nom.setText(Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature());

                    opciones2 =(Spinner)getView().findViewById(R.id.opcMedEj4_1);
                    ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.ingles, android.R.layout.simple_spinner_item);
                    opciones2.setAdapter(adapter2);

                    opciones2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            double radioEngB = Double.parseDouble(txtRadioEngB.getText().toString());
                            double radioEngC = Double.parseDouble(txtRadioEngC.getText().toString());

                            DecimalFormat numberFormat = new DecimalFormat("#.####");
                            switch(position){
                                case 0:
                                    radioEngB = Thorzion.milimeterToMeter(radioEngB);
                                    radioEngC = Thorzion.milimeterToMeter(radioEngC);
                                    txtRadioEngB.setText(numberFormat.format(radioEngB), TextView.BufferType.EDITABLE);
                                    txtRadioEngC.setText(numberFormat.format(radioEngC), TextView.BufferType.EDITABLE);
                                    lblRadiosNom.setText(Thorzion.getFootNomenclature());

                                    break;
                                case 1:
                                    radioEngB = Thorzion.meterToMilimeter(radioEngB);
                                    radioEngC = Thorzion.meterToMilimeter(radioEngC);
                                    txtRadioEngB.setText(numberFormat.format(radioEngB), TextView.BufferType.EDITABLE);
                                    txtRadioEngC.setText(numberFormat.format(radioEngC), TextView.BufferType.EDITABLE);
                                    lblRadiosNom.setText(Thorzion.getInchNomenclature());

                                    break;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }

                else if(position == 1){
                    tao4 = Thorzion.PRESSURE_UNITS.KSI.unitToInternational(tao4);

                    TextView lblTaoEj4Nom = (TextView) getView().findViewById(R.id.lblTaoEj4Nom);
                    lblTaoEj4Nom.setText(Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature());

                    opciones2 =(Spinner)getView().findViewById(R.id.opcMedEj4_1);
                    ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.ingles, android.R.layout.simple_spinner_item);
                    opciones2.setAdapter(adapter2);

                    opciones2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            double radioEngB = Double.parseDouble(txtRadioEngB.getText().toString());
                            double radioEngC = Double.parseDouble(txtRadioEngC.getText().toString());

                            DecimalFormat numberFormat = new DecimalFormat("#.####");
                            switch(position){
                                case 0:
                                    radioEngB = Thorzion.milimeterToMeter(radioEngB);
                                    radioEngC = Thorzion.milimeterToMeter(radioEngC);
                                    txtRadioEngB.setText(numberFormat.format(radioEngB), TextView.BufferType.EDITABLE);
                                    txtRadioEngC.setText(numberFormat.format(radioEngC), TextView.BufferType.EDITABLE);
                                    lblRadiosNom.setText(Thorzion.getFootNomenclature());

                                    break;
                                case 1:
                                    radioEngB = Thorzion.meterToMilimeter(radioEngB);
                                    radioEngC = Thorzion.meterToMilimeter(radioEngC);
                                    txtRadioEngB.setText(numberFormat.format(radioEngB), TextView.BufferType.EDITABLE);
                                    txtRadioEngC.setText(numberFormat.format(radioEngC), TextView.BufferType.EDITABLE);
                                    lblRadiosNom.setText(Thorzion.getInchNomenclature());

                                    break;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }

                txtTaoEj4.setText(tao4+"", TextView.BufferType.EDITABLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }
}