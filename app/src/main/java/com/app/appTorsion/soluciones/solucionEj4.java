package com.app.appTorsion.soluciones;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.app.appTorsion.R;
import com.app.appTorsion.activities.ExerciseActivity;
import com.app.appTorsion.activities.ExercisesActivity;
import com.app.appTorsion.activities.ProceduresActivity;
import com.app.appTorsion.helpers.Thorzion;
import com.app.appTorsion.models.Exercise;
import com.app.appTorsion.models.ProcedureActivity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class solucionEj4 extends Fragment implements Serializable {

    Button atras;
    Button resolver;
    Button solucion;
    Button modulo2;

    EditText txtTaoEj4, txtHP, txtVel, txtRadioEngB, txtRadioEngC;

    TextView lblTaoEj4Nom, lblHPNom, lblVelNom, lblRadiosNom, lblRadiosNom2;
    TextView txtSol4, txtSol4_B,txtSol4_C,txtSol4_D;

    Spinner opciones3;
    Spinner opciones2;

    private Exercise exercise;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_solucion_ej4, container, false);
        exercise = (Exercise) getArguments().getSerializable("exercise");

        txtSol4 = (TextView) view.findViewById(R.id.txtSol4);
        txtSol4_B = (TextView) view.findViewById(R.id.txtSol4_B);
        txtSol4_C = (TextView) view.findViewById(R.id.txtSol4_C);
        txtSol4_D = (TextView) view.findViewById(R.id.txtSol4_D);

        txtTaoEj4 = (EditText) view.findViewById(R.id.txtTaoEj4);
        txtHP = (EditText) view.findViewById(R.id.txtHP);
        txtVel = (EditText) view.findViewById(R.id.txtVel);
        txtRadioEngB = (EditText) view.findViewById(R.id.txtRadioEngB);
        txtRadioEngC = (EditText) view.findViewById(R.id.txtRadioEngC);

        lblTaoEj4Nom = (TextView) view.findViewById(R.id.lblTaoEj4Nom);
        lblHPNom = (TextView) view.findViewById(R.id.lblHPNom);
        lblVelNom = (TextView) view.findViewById(R.id.lblVelNom);
        lblRadiosNom = (TextView) view.findViewById(R.id.lblRadiosNom);
        lblRadiosNom2 = (TextView) view.findViewById(R.id.lblRadiosNom2);

        resolver = (Button) view.findViewById(R.id.btnRes4);
        solucion = (Button) view.findViewById(R.id.btnSol4);

        resolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                exercise.setActivities(new ArrayList<>());

                double taoEj4 = Double.parseDouble(txtTaoEj4.getText().toString());
                double HP = Double.parseDouble(txtHP.getText().toString());
                double Vel = Double.parseDouble(txtVel.getText().toString());
                double radioEngB = Double.parseDouble(txtRadioEngB.getText().toString());
                double radioEngC = Double.parseDouble(txtRadioEngC.getText().toString());

                String nomneclatureTorque = Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature();

                String nomneclatureDistance = opciones3.getSelectedItem().toString().equals("S.Ingles")?
                        Thorzion.getInchNomenclature():Thorzion.getInchNomenclature();

                DecimalFormat formato = new DecimalFormat();
                formato.setMaximumFractionDigits(1); //Numero maximo de decimales a mostrar

                ProcedureActivity paso1 = new ProcedureActivity(
                        "Obtener Torque generado por motor",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera, P =" + Thorzion.getClearPower(HP) + " " + lblHPNom.getText() + ", W = " + Thorzion.getAngularSpeed(Vel) + " " + lblVelNom.getText()
                        + "obteniendo un resultado de Torque = " +formato.format(Thorzion.getClearTorque(Thorzion.getClearPower(HP),Thorzion.getAngularSpeed(Vel))) + " " + nomneclatureTorque+" Generado por el motor",
                        R.drawable.ec9);


                ProcedureActivity paso2 = new ProcedureActivity(
                        "Obtener diametro eje AB",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b>τ =" + taoEj4 + " " + lblTaoEj4Nom.getText() +
                                " T = " + Thorzion.getTorquePwASp(HP,Thorzion.getAngularSpeed(Vel)) + "" + nomneclatureTorque +
                                " </b>, obteniendo como resultado un <b> Diametro = " +
                                formato.format(Thorzion.getDiameter(Thorzion.getRadius(taoEj4, Thorzion.getTorquePwASp(Thorzion.getClearPower(HP),Thorzion.getAngularSpeed(Vel)))))+ " "+nomneclatureDistance+
                                " Para el eje AB"+"</b>",
                        R.drawable.ec4);

                ProcedureActivity paso3 = new ProcedureActivity(
                        "Obtener Torque en engrane C",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b>F = " + Thorzion.getForce(Thorzion.getTorquePwASp(HP,Thorzion.getAngularSpeed(Vel)),radioEngC) + " " + nomneclatureTorque +
                                " </b>, obteniendo como resultado un <b> Torque = " +
                                formato.format(Thorzion.getTorqueMesh(Thorzion.getForce(Thorzion.getTorquePwASp(Thorzion.getClearPower(HP),
                                        Thorzion.getAngularSpeed(Vel)),radioEngB),radioEngC)) + " " + nomneclatureTorque+" para el engrane C"+"</b>",
                        R.drawable.ec8);

                ProcedureActivity paso4 = new ProcedureActivity(
                        "Obtener Diametro en BC",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T ="+formato.format(Thorzion.getTorqueMesh(Thorzion.getForce(Thorzion.getTorquePwASp(Thorzion.getClearPower(HP),
                                Thorzion.getAngularSpeed(Vel)),radioEngB),radioEngC))+" "+ nomneclatureTorque + ", τ = " +taoEj4+
                                " "+ lblTaoEj4Nom.getText() +" "+" </b>, obteniendo como resultado un <b> Diametro = " +
                                formato.format(Thorzion.getDiameter(Thorzion.getRadius(taoEj4,Thorzion.getTorqueMesh(Thorzion.getForce
                                        (Thorzion.getTorquePwASp(Thorzion.getClearPower(HP),Thorzion.getAngularSpeed(Vel)),radioEngB),radioEngC))))+" "+ nomneclatureDistance+" para el eje en BC",
                        R.drawable.ec4);

                exercise.addActivity(paso1);
                exercise.addActivity(paso2);
                exercise.addActivity(paso3);
                exercise.addActivity(paso4);

                Intent iniciar = new Intent(getContext(), ProceduresActivity.class);
                exercise.setLayoutID(null);
                iniciar.putExtra("exercise", exercise);
                startActivity(iniciar);
            }
        });



        solucion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                exercise.setActivities(new ArrayList<>());

                double taoEj4 = Double.parseDouble(txtTaoEj4.getText().toString());
                double HP = Double.parseDouble(txtHP.getText().toString());
                double Vel = Double.parseDouble(txtVel.getText().toString());
                double radioEngB = Double.parseDouble(txtRadioEngB.getText().toString());
                double radioEngC = Double.parseDouble(txtRadioEngC.getText().toString());

                String nomneclatureTorque = Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature();

                String nomneclatureDistance = opciones3.getSelectedItem().toString().equals("S.Ingles")?
                        Thorzion.getInchNomenclature():Thorzion.getInchNomenclature();

                String nomneclatureForce = Thorzion.FORCE_UNITS.LB.getNomenclature();

                DecimalFormat formato = new DecimalFormat();
                formato.setMaximumFractionDigits(2); //Numero maximo de decimales a mostrar


                txtSol4.setText(" R/: " + formato.format(Thorzion.getDiameter(Thorzion.getRadius(taoEj4,
                        Thorzion.getTorquePwASp(Thorzion.getClearPower(HP),Thorzion.getAngularSpeed(Vel))))) + " " + nomneclatureDistance + " Para el diametro en AB");
                txtSol4_B.setText(" R/: " + formato.format(Thorzion.getForce(Thorzion.getTorquePwASp
                        (Thorzion.getClearPower(HP),Thorzion.getAngularSpeed(Vel)),radioEngB)) + " " + nomneclatureForce + " Para la fuerza en B y en C");
                txtSol4_C.setText(" R/: " + formato.format(Thorzion.getTorqueMesh(Thorzion.getForce
                        (Thorzion.getTorquePwASp(Thorzion.getClearPower(HP),Thorzion.getAngularSpeed(Vel)),radioEngB),radioEngC)) + " " + nomneclatureTorque + " Para el Torque en engrane C");
                txtSol4_D.setText(" R/: " + formato.format(Thorzion.getDiameter(Thorzion.getRadius(taoEj4,Thorzion.getTorqueMesh(Thorzion.getForce
                        (Thorzion.getTorquePwASp(Thorzion.getClearPower(HP),Thorzion.getAngularSpeed(Vel)),radioEngB),radioEngC)))) + " " + nomneclatureDistance + " Para el diametro en BC");

                exercise.setLayoutID(null);

            }
        });

        atras = (Button) view.findViewById(R.id.btnAtr4);

        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent atras = new Intent(getActivity(), ExercisesActivity.class);
                exercise.setLayoutID(null);
                atras.putExtra("exercise", exercise);
                startActivity(atras);

            }
        });



        opciones3 =(Spinner)view.findViewById(R.id.opcSistEj4);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.opciones3, android.R.layout.simple_spinner_item);
        opciones3.setAdapter(adapter);

        opciones2 =(Spinner)view.findViewById(R.id.opcMedEj4);

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onStart(){
        super.onStart();
        this.opciones3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here

                double tao4 = Double.parseDouble(txtTaoEj4.getText().toString());

                txtRadioEngB.setText("0", TextView.BufferType.EDITABLE);
                txtRadioEngC.setText("0", TextView.BufferType.EDITABLE);
                txtHP.setText("0", TextView.BufferType.EDITABLE);
                txtVel.setText("0", TextView.BufferType.EDITABLE);

                lblHPNom.setText(Thorzion.POWER_UNITS.HP.getNomenclature());
                lblVelNom.setText(Thorzion.SPEED_UNITS.RPM.getNomenclature());

                if(position == 0){
                    tao4 = Thorzion.PRESSURE_UNITS.PASCAL.unitToImperial(tao4);

                    TextView lblTaoEj4Nom = (TextView) getView().findViewById(R.id.lblTaoEj4Nom);
                    lblTaoEj4Nom.setText(Thorzion.PRESSURE_UNITS.KSI.getNomenclature());

                    opciones2 =(Spinner)getView().findViewById(R.id.opcMedEj4);
                    ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.ingles, android.R.layout.simple_spinner_item);
                    opciones2.setAdapter(adapter2);

                    opciones2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            double radioEngB = Double.parseDouble(txtRadioEngB.getText().toString());
                            double radioEngC = Double.parseDouble(txtRadioEngC.getText().toString());

                            DecimalFormat numberFormat = new DecimalFormat("#.####");
                            switch(position){
                                case 0:
                                    radioEngB = Thorzion.inchToFoot(radioEngB);
                                    radioEngC = Thorzion.inchToFoot(radioEngC);
                                    txtRadioEngB.setText(numberFormat.format(radioEngB), TextView.BufferType.EDITABLE);
                                    txtRadioEngC.setText(numberFormat.format(radioEngC), TextView.BufferType.EDITABLE);
                                    lblRadiosNom.setText(Thorzion.getFootNomenclature());
                                    lblRadiosNom2.setText(Thorzion.getFootNomenclature());


                                    break;
                                case 1:
                                    radioEngB = Thorzion.footToInch(radioEngB);
                                    radioEngC = Thorzion.footToInch(radioEngC);
                                    txtRadioEngB.setText(numberFormat.format(radioEngB), TextView.BufferType.EDITABLE);
                                    txtRadioEngC.setText(numberFormat.format(radioEngC), TextView.BufferType.EDITABLE);
                                    lblRadiosNom.setText(Thorzion.getInchNomenclature());
                                    lblRadiosNom2.setText(Thorzion.getInchNomenclature());


                                    break;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }

                else if(position == 1){
                    tao4 = Thorzion.PRESSURE_UNITS.PASCAL.unitToImperial(tao4);

                    TextView lblTaoEj4Nom = (TextView) getView().findViewById(R.id.lblTaoEj4Nom);
                    lblTaoEj4Nom.setText(Thorzion.PRESSURE_UNITS.KSI.getNomenclature());

                    opciones2 =(Spinner)getView().findViewById(R.id.opcMedEj4);
                    ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.ingles, android.R.layout.simple_spinner_item);
                    opciones2.setAdapter(adapter2);

                    opciones2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            double radioEngB = Double.parseDouble(txtRadioEngB.getText().toString());
                            double radioEngC = Double.parseDouble(txtRadioEngC.getText().toString());

                            DecimalFormat numberFormat = new DecimalFormat("#.####");
                            switch(position){
                                case 0:
                                    radioEngB = Thorzion.inchToFoot(radioEngB);
                                    radioEngC = Thorzion.inchToFoot(radioEngC);
                                    txtRadioEngB.setText(numberFormat.format(radioEngB), TextView.BufferType.EDITABLE);
                                    txtRadioEngC.setText(numberFormat.format(radioEngC), TextView.BufferType.EDITABLE);
                                    lblRadiosNom.setText(Thorzion.getFootNomenclature());
                                    lblRadiosNom2.setText(Thorzion.getFootNomenclature());


                                    break;
                                case 1:
                                    radioEngB = Thorzion.footToInch(radioEngB);
                                    radioEngC = Thorzion.footToInch(radioEngC);
                                    txtRadioEngB.setText(numberFormat.format(radioEngB), TextView.BufferType.EDITABLE);
                                    txtRadioEngC.setText(numberFormat.format(radioEngC), TextView.BufferType.EDITABLE);
                                    lblRadiosNom.setText(Thorzion.getInchNomenclature());
                                    lblRadiosNom2.setText(Thorzion.getInchNomenclature());


                                    break;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }

                txtTaoEj4.setText(tao4+"", TextView.BufferType.EDITABLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }
}