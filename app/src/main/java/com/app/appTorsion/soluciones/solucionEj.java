package com.app.appTorsion.soluciones;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.app.appTorsion.R;
import com.app.appTorsion.activities.ExercisesActivity;
import com.app.appTorsion.activities.ProceduresActivity;
import com.app.appTorsion.helpers.Thorzion;
import com.app.appTorsion.models.Exercise;
import com.app.appTorsion.models.ProcedureActivity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class solucionEj extends Fragment implements Serializable {

    Button atras;
    Button resolver;
    Button solucion;

    EditText txtTao, txtRadioInterno, txtRadioExterno, txtTaoCaso2, txtRadioInternoCaso2, txtRadioExternoCaso2;

    EditText txtTorqueEj1, txtTorque2Ej1, txtRigEj1, txtRig2Ej1, txtLongEj1, txtLong2Ej1;

    TextView lblRadioIntEj1, lblRadioExtEj1, lblTaoEj1,txtSol;
    TextView lblTorqueEj1, lblLongEj1, lblRigEj1, txtSol2, txtSol3;

    TextView lblRadioInt2Ej1, lblRadioExt2Ej1, lblTao2Ej1;
    TextView lblTorque2Ej1, lblLong2Ej1, lblRig2Ej1;

    Spinner opciones;
    Spinner opciones2;

    private Exercise exercise;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_solucion_ej, container, false);
        exercise = (Exercise) getArguments().getSerializable("exercise");

        txtSol = (TextView) view.findViewById(R.id.txtSolEj1);
        txtTao = (EditText) view.findViewById(R.id.txtTao);
        txtRadioInterno = (EditText) view.findViewById(R.id.txtRadioInterno);
        txtRadioExterno = (EditText) view.findViewById(R.id.txtRadioExterno);
        txtTaoCaso2 = (EditText) view.findViewById(R.id.txtTaoCaso2);
        txtRadioInternoCaso2 = (EditText) view.findViewById(R.id.txtRadioInternoCaso2);
        txtRadioExternoCaso2 = (EditText) view.findViewById(R.id.txtRadioExternoCaso2);

        txtTorqueEj1 = (EditText) view.findViewById(R.id.txtTorqueEj1);
        txtTorque2Ej1 = (EditText) view.findViewById(R.id.txtTorque2Ej1);
        txtRigEj1 = (EditText) view.findViewById(R.id.txtRigEj1);
        txtRig2Ej1 = (EditText) view.findViewById(R.id.txtRig2Ej1);
        txtLongEj1 = (EditText) view.findViewById(R.id.txtLongEj1);
        txtLong2Ej1 = (EditText) view.findViewById(R.id.txtLong2Ej1);
        txtSol2 = (TextView) view.findViewById(R.id.txtSol2Ej1);
        txtSol3 = (TextView) view.findViewById(R.id.txtSol3Ej1);

        lblRadioIntEj1 = (TextView) view.findViewById(R.id.lblRadioIntEj1);
        lblRadioExtEj1 = (TextView) view.findViewById(R.id.lblRadioExtEj1);
        lblTaoEj1 = (TextView) view.findViewById(R.id.lblTaoEj1);
        lblTorqueEj1 = (TextView) view.findViewById(R.id.lblTorqueEj1);
        lblLongEj1 = (TextView) view.findViewById(R.id.lblLongEj1);
        lblRigEj1 = (TextView) view.findViewById(R.id.lblRigEj1);

        lblRadioInt2Ej1 = (TextView) view.findViewById(R.id.lblRadioInt2Ej1);
        lblRadioExt2Ej1 = (TextView) view.findViewById(R.id.lblRadioExt2Ej1);
        lblTao2Ej1 = (TextView) view.findViewById(R.id.lblTao2Ej1);
        lblTorque2Ej1 = (TextView) view.findViewById(R.id.lblTorque2Ej1);
        lblLong2Ej1 = (TextView) view.findViewById(R.id.lblLong2Ej1);
        lblRig2Ej1 = (TextView) view.findViewById(R.id.lblRig2Ej1);

        resolver = (Button) view.findViewById(R.id.btnRes);
        solucion = (Button) view.findViewById(R.id.btnSol);

        resolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                exercise.setActivities(new ArrayList<>());

                double tao = Double.parseDouble(txtTao.getText().toString());
                double radioExterno = Double.parseDouble(txtRadioExterno.getText().toString());
                double radioInterno = Double.parseDouble(txtRadioInterno.getText().toString());
                double taoCaso2 = Double.parseDouble(txtTaoCaso2.getText().toString());
                double radioExternoCaso2 = Double.parseDouble(txtRadioExternoCaso2.getText().toString());
                double radioInternoCaso2 = Double.parseDouble(txtRadioInternoCaso2.getText().toString());

                double torqueEj1 = Double.parseDouble(txtTorqueEj1.getText().toString());
                double torque2Ej1 = Double.parseDouble(txtTorque2Ej1.getText().toString());
                double rigEj1 = Double.parseDouble(txtRigEj1.getText().toString());
                double rig2Ej1 = Double.parseDouble(txtRig2Ej1.getText().toString());
                double longEj1 = Double.parseDouble(txtLongEj1.getText().toString());
                double long2Ej1 = Double.parseDouble(txtLong2Ej1.getText().toString());

                DecimalFormat format1 = new DecimalFormat("#.##");
                DecimalFormat format3 = new DecimalFormat("#.###");
                DecimalFormat format2 = new DecimalFormat("#.######");

                String nomneclatureTorque = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature():Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature();

                String nomneclaturePresure = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature():Thorzion.PRESSURE_UNITS.KSI.getNomenclature();

                String nomneclatureAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.ANGLE_UNITS.RAD.getNomenclature():Thorzion.ANGLE_UNITS.DEG.getNomenclature();

                double unidadAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torqueEj1,longEj1,radioExterno,radioInterno,rigEj1):Thorzion.getThorzionAngleRigid(torqueEj1,longEj1,radioExterno,radioInterno,rigEj1)*180/Math.PI;

                double unidadAngle2 = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torque2Ej1,long2Ej1,radioExternoCaso2,radioInternoCaso2,rig2Ej1):Thorzion.getThorzionAngleRigid(torque2Ej1,long2Ej1,radioExternoCaso2,radioInternoCaso2,rig2Ej1)*180/Math.PI;


                ProcedureActivity paso1 = new ProcedureActivity(
                        "Modulo 1",
                        "En este módulo se procederá a encontrar los torques correspondientes al vástago y a la manga por medio del esfuerzo cortante permisible y los radios",
                        R.drawable.mod1);

                ProcedureActivity paso2 = new ProcedureActivity(
                        "Modulo 1: Hallar Torque sección AB",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> τ = " + tao + " " + lblTaoEj1.getText()  + ","+
                                 "  J  = " + format2.format(Thorzion.getInertiaConstant(radioExterno,radioInterno)) + lblRadioExtEj1.getText()+"^4, y "
                                 + "C = " + radioExterno + lblRadioExtEj1.getText() + "\n" +
                                " </b>, obteniendo como resultado un<b> Torque = " +
                                format1.format(Thorzion.getTorque(tao,radioExterno,radioInterno)) + " " + nomneclatureTorque+" Para el torque sección AB. "+"</b>",
                        R.drawable.ec3);


                ProcedureActivity paso3 = new ProcedureActivity(
                        " Modulo 1: Hallar Torque sección BCD",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> τ = (" + taoCaso2 + " " + lblTaoEj1.getText() +","+
                                " J = " +  format2.format( Thorzion.getInertiaConstant(radioExternoCaso2,radioInternoCaso2)) + lblRadioExtEj1.getText()+"^4, y "
                                + " C = " + radioExternoCaso2 + lblRadioExtEj1.getText() +
                                " </b>, obteniendo como resultado un <b>Torque = " +
                                format1.format(Thorzion.getTorque(taoCaso2,radioExternoCaso2,radioInternoCaso2)) + " " + nomneclatureTorque+" Para el torque sección BCD"+"</b>",
                        R.drawable.ec3);


                ProcedureActivity paso4 = new ProcedureActivity(
                        "Modulo 1: Resultado y conclusión",
                        "Obteniendo ambos resultados, podemos concluir que el Torque que satisface al sistema para el vástago de acero y la manga de latón es igual a: " +
                                ((Thorzion.getTorque(tao,radioExterno,radioInterno) > Thorzion.getTorque(taoCaso2,radioExternoCaso2,radioInternoCaso2))?
                                        ""+ format1.format(Thorzion.getTorque(taoCaso2,radioExternoCaso2,radioInternoCaso2)):
                                        ""+ format1.format(Thorzion.getTorque(tao,radioExterno,radioInterno))) + " " + nomneclatureTorque + " Para el torque en el sistema",
                        R.drawable.ec3);

                ProcedureActivity paso5 = new ProcedureActivity(
                        "Modulo 2",
                        "En este módulo se procederá a encontrar los esfuerzos cortantes permisibles y los ángulos de torsión del vástago y la manga por medio del" +
                                "torque, el módulo de rigidez del material, los radios y las longitudes de cada uno de los objetos",

                        R.drawable.mod2);

                ProcedureActivity paso6 = new ProcedureActivity(
                        "Modulo 2: Hallar esfuerzo cortante permisible sección AB",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T = " + torqueEj1 + " " + lblTorqueEj1.getText()  + ","+
                                "  J  = " + format2.format(Thorzion.getInertiaConstant(radioExterno,radioInterno)) + lblRadioExtEj1.getText()+"^4, y "
                                + "C = " + radioExterno + lblRadioExtEj1.getText() + "\n" +
                                " </b>, obteniendo como resultado un<b> τ = " +
                                format1.format(Thorzion.getTaoMaximus(torqueEj1,radioExterno,radioInterno)) + " " + nomneclaturePresure+" Para el esfuerzo cortante permisible en la sección AB. "+"</b>",
                        R.drawable.ec2);

                ProcedureActivity paso7 = new ProcedureActivity(
                        "Modulo 2: Hallar esfuerzo cortante permisible sección BCD",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T = " + torque2Ej1 + " " + lblTorqueEj1.getText()  + ","+
                                "  J  = " + format2.format(Thorzion.getInertiaConstant(radioExternoCaso2,radioInternoCaso2)) + lblRadioExtEj1.getText()+"^4, y "
                                + "C = " + radioExternoCaso2 + lblRadioExtEj1.getText() + "\n" +
                                " </b>, obteniendo como resultado un<b> τ = " +
                                format1.format(Thorzion.getTaoMaximus(torque2Ej1,radioExternoCaso2,radioInternoCaso2)) + " " + nomneclaturePresure+" Para el esfuerzo cortante permisible en la sección BCD"+"</b>",
                        R.drawable.ec2);

                ProcedureActivity paso8 = new ProcedureActivity(
                        "Modulo 2: Hallar ángulo de torsión sección AB",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T=("+torqueEj1 + " " +lblTorqueEj1.getText()+", L = " + longEj1 + "" + lblLongEj1.getText() +
                                ", J = " + format2.format(Thorzion.getInertiaConstant(radioExterno,radioInterno)) + lblRadioExtEj1.getText()+ ", G = " + rigEj1 + ""+ lblRigEj1.getText()+""+ " </b>, " +
                                "obteniendo como resultado un ángulo = <b>"+ format3.format(unidadAngle) + " " + nomneclatureAngle + " Para la sección AB. "+"</b>",
                        R.drawable.ec5);

                ProcedureActivity paso9 = new ProcedureActivity(
                        "Hallar ángulo de torsión sección BCD",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T=(" +torque2Ej1 + " " +lblTorqueEj1.getText()+", L = " + long2Ej1 + "" + lblLongEj1.getText() +
                                ", J = " + format2.format(Thorzion.getInertiaConstant(radioExternoCaso2,radioInternoCaso2)) + lblRadioExtEj1.getText()+ ", G = " + rig2Ej1 + ""+ lblRigEj1.getText()+""+ " </b>, " +
                                "obteniendo como resultado un ángulo = <b>"+ format3.format(unidadAngle2) + " " + nomneclatureAngle+" Para la sección BCD" + "</b>",
                        R.drawable.ec5);



                exercise.addActivity(paso1);
                exercise.addActivity(paso2);
                exercise.addActivity(paso3);
                exercise.addActivity(paso4);
                exercise.addActivity(paso5);
                exercise.addActivity(paso6);
                exercise.addActivity(paso7);
                exercise.addActivity(paso8);
                exercise.addActivity(paso9);

                Intent iniciar = new Intent(getContext(), ProceduresActivity.class);
                exercise.setLayoutID(null);
                iniciar.putExtra("exercise", exercise);
                startActivity(iniciar);
            }
        });

        solucion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                exercise.setActivities(new ArrayList<>());

                double tao = Double.parseDouble(txtTao.getText().toString());
                double radioExterno = Double.parseDouble(txtRadioExterno.getText().toString());
                double radioInterno = Double.parseDouble(txtRadioInterno.getText().toString());
                double taoCaso2 = Double.parseDouble(txtTaoCaso2.getText().toString());
                double radioExternoCaso2 = Double.parseDouble(txtRadioExternoCaso2.getText().toString());
                double radioInternoCaso2 = Double.parseDouble(txtRadioInternoCaso2.getText().toString());

                double torqueEj1 = Double.parseDouble(txtTorqueEj1.getText().toString());
                double torque2Ej1 = Double.parseDouble(txtTorque2Ej1.getText().toString());
                double rigEj1 = Double.parseDouble(txtRigEj1.getText().toString());
                double rig2Ej1 = Double.parseDouble(txtRig2Ej1.getText().toString());
                double longEj1 = Double.parseDouble(txtLongEj1.getText().toString());
                double long2Ej1 = Double.parseDouble(txtLong2Ej1.getText().toString());

                DecimalFormat format1 = new DecimalFormat("#.##");
                DecimalFormat format2 = new DecimalFormat("#.###");

                String nomneclatureTorque = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature():Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature();

                String nomneclaturePresure = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature():Thorzion.PRESSURE_UNITS.KSI.getNomenclature();

                String nomneclatureAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.ANGLE_UNITS.RAD.getNomenclature():Thorzion.ANGLE_UNITS.DEG.getNomenclature();

                double unidadAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torqueEj1,longEj1,radioExterno,radioInterno,rigEj1):Thorzion.getThorzionAngleRigid(torqueEj1,longEj1,radioExterno,radioInterno,rigEj1)*180/Math.PI;

                double unidadAngle2 = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torque2Ej1,long2Ej1,radioExternoCaso2,radioInternoCaso2,rig2Ej1):Thorzion.getThorzionAngleRigid(torque2Ej1,long2Ej1,radioExternoCaso2,radioInternoCaso2,rig2Ej1)*180/Math.PI;


                txtSol.setText(" R/: "+format1.format(Thorzion.getTorque(tao,radioExterno,radioInterno)) + " " + nomneclatureTorque+" Para el torque sección AB, " +
                        format1.format(Thorzion.getTorque(taoCaso2,radioExternoCaso2,radioInternoCaso2)) + " " + nomneclatureTorque+" Para el torque sección BCD" );
                txtSol2.setText(" R/: " + format1.format(Thorzion.getTaoMaximus(torqueEj1,radioExterno,radioInterno))+ " " + nomneclaturePresure + " Para el esfuerzo cortante permisible en la sección AB, " +
                        format1.format(Thorzion.getTaoMaximus(torque2Ej1,radioExternoCaso2,radioInternoCaso2)) + " " + nomneclaturePresure + " Para el esfuerzo cortante permisible en la sección BCD.");

                txtSol3.setText(" R/: " + format2.format(unidadAngle) + " " + nomneclatureAngle + " Para el ángulo en la sección AB, " +
                        format2.format(unidadAngle2) + " " + nomneclatureAngle + " Para el ángulo en la sección BCD.");


                exercise.setLayoutID(null);

            }
        });

        atras = (Button) view.findViewById(R.id.btnAtr);

        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent atras = new Intent(getActivity(), ExercisesActivity.class);
                exercise.setLayoutID(null);
                atras.putExtra("exercise", exercise);
                startActivity(atras);

            }
        });

        opciones =(Spinner)view.findViewById(R.id.opcSistEj1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.opciones, android.R.layout.simple_spinner_item);
        opciones.setAdapter(adapter);

        opciones2 =(Spinner)view.findViewById(R.id.opcMedEj1);

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onStart(){
        super.onStart();
        this.opciones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here

                double tao = Double.parseDouble(txtTao.getText().toString());

                txtRadioInterno.setText("0", TextView.BufferType.EDITABLE);
                txtRadioExterno.setText("0", TextView.BufferType.EDITABLE);

                double taoCaso2 = Double.parseDouble(txtTaoCaso2.getText().toString());

                txtRadioInternoCaso2.setText("0", TextView.BufferType.EDITABLE);
                txtRadioExternoCaso2.setText("0", TextView.BufferType.EDITABLE);

                double torqueEj1 = Double.parseDouble(txtTorqueEj1.getText().toString());
                double torque2Ej1 = Double.parseDouble(txtTorque2Ej1.getText().toString());

                txtLongEj1.setText("0", TextView.BufferType.EDITABLE);
                txtLong2Ej1.setText("0", TextView.BufferType.EDITABLE);

                double rigEj1 = Double.parseDouble(txtRigEj1.getText().toString());
                double rig2Ej1 = Double.parseDouble(txtRig2Ej1.getText().toString());


                if(position == 0){
                    tao = Thorzion.PRESSURE_UNITS.KSI.unitToInternational(tao);
                    taoCaso2 = Thorzion.PRESSURE_UNITS.KSI.unitToInternational(taoCaso2);
                    torqueEj1 = Thorzion.TORQUE_UNITS.POUND_INCH.unitToInternational(torqueEj1);
                    torque2Ej1 = Thorzion.TORQUE_UNITS.POUND_INCH.unitToInternational(torque2Ej1);
                    rigEj1 = Thorzion.PRESSURE_UNITS.KSI.unitToInternational(tao);
                    rig2Ej1 = Thorzion.PRESSURE_UNITS.KSI.unitToInternational(tao);

                    lblTaoEj1.setText(Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature());
                    lblTorqueEj1.setText(Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature());
                    lblRigEj1.setText(Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature());

                    lblTao2Ej1.setText(Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature());
                    lblTorque2Ej1.setText(Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature());
                    lblRig2Ej1.setText(Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature());

                    opciones2 =(Spinner)getView().findViewById(R.id.opcMedEj1);
                    ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.metric, android.R.layout.simple_spinner_item);
                    opciones2.setAdapter(adapter2);
                    opciones2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            double radioExterno = Double.parseDouble(txtRadioExterno.getText().toString());
                            double radioInterno = Double.parseDouble(txtRadioInterno.getText().toString());

                            double radioExternoCaso2 = Double.parseDouble(txtRadioExternoCaso2.getText().toString());
                            double radioInternoCaso2 = Double.parseDouble(txtRadioInternoCaso2.getText().toString());

                            double longEj1 = Double.parseDouble(txtLongEj1.getText().toString());
                            double long2Ej1 = Double.parseDouble(txtLong2Ej1.getText().toString());

                            DecimalFormat numberFormat = new DecimalFormat("#.####");
                            switch(position){
                                case 0:
                                    radioExterno = Thorzion.milimeterToMeter(radioExterno);
                                    radioInterno = Thorzion.milimeterToMeter(radioInterno);
                                    txtRadioInterno.setText(numberFormat.format(radioInterno), TextView.BufferType.EDITABLE);
                                    txtRadioExterno.setText(numberFormat.format(radioExterno), TextView.BufferType.EDITABLE);

                                    radioExternoCaso2 = Thorzion.milimeterToMeter(radioExternoCaso2);
                                    radioInternoCaso2 = Thorzion.milimeterToMeter(radioInternoCaso2);
                                    txtRadioInternoCaso2.setText(numberFormat.format(radioInternoCaso2), TextView.BufferType.EDITABLE);
                                    txtRadioExternoCaso2.setText(numberFormat.format(radioExternoCaso2), TextView.BufferType.EDITABLE);

                                    longEj1 = Thorzion.milimeterToMeter(longEj1);
                                    long2Ej1 = Thorzion.milimeterToMeter(long2Ej1);
                                    txtLongEj1.setText(numberFormat.format(longEj1), TextView.BufferType.EDITABLE);
                                    txtLong2Ej1.setText(numberFormat.format(long2Ej1), TextView.BufferType.EDITABLE);

                                    lblRadioIntEj1.setText(Thorzion.getMeterNomenclature());
                                    lblRadioExtEj1.setText(Thorzion.getMeterNomenclature());
                                    lblLongEj1.setText(Thorzion.getMeterNomenclature());

                                    lblRadioInt2Ej1.setText(Thorzion.getMeterNomenclature());
                                    lblRadioExt2Ej1.setText(Thorzion.getMeterNomenclature());
                                    lblLong2Ej1.setText(Thorzion.getMeterNomenclature());

                                    break;
                                case 1:
                                    radioExterno = Thorzion.meterToMilimeter(radioExterno);
                                    radioInterno = Thorzion.meterToMilimeter(radioInterno);
                                    txtRadioInterno.setText(numberFormat.format(radioInterno), TextView.BufferType.EDITABLE);
                                    txtRadioExterno.setText(numberFormat.format(radioExterno), TextView.BufferType.EDITABLE);

                                    radioExternoCaso2 = Thorzion.meterToMilimeter(radioExternoCaso2);
                                    radioInternoCaso2 = Thorzion.meterToMilimeter(radioInternoCaso2);
                                    txtRadioInternoCaso2.setText(numberFormat.format(radioInternoCaso2), TextView.BufferType.EDITABLE);
                                    txtRadioExternoCaso2.setText(numberFormat.format(radioExternoCaso2), TextView.BufferType.EDITABLE);

                                    longEj1 = Thorzion.meterToMilimeter(longEj1);
                                    long2Ej1 = Thorzion.meterToMilimeter(long2Ej1);
                                    txtLongEj1.setText(numberFormat.format(longEj1), TextView.BufferType.EDITABLE);
                                    txtLong2Ej1.setText(numberFormat.format(long2Ej1), TextView.BufferType.EDITABLE);

                                    lblRadioIntEj1.setText(Thorzion.getMiliMeterNomenclature());
                                    lblRadioExtEj1.setText(Thorzion.getMiliMeterNomenclature());
                                    lblLongEj1.setText(Thorzion.getMiliMeterNomenclature());

                                    lblRadioInt2Ej1.setText(Thorzion.getMiliMeterNomenclature());
                                    lblRadioExt2Ej1.setText(Thorzion.getMiliMeterNomenclature());
                                    lblLong2Ej1.setText(Thorzion.getMiliMeterNomenclature());

                                    break;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
                else if(position == 1){
                    tao = Thorzion.PRESSURE_UNITS.PASCAL.unitToImperial(tao);
                    taoCaso2 = Thorzion.PRESSURE_UNITS.PASCAL.unitToImperial(taoCaso2);
                    torqueEj1 = Thorzion.TORQUE_UNITS.NEWTON_METER.unitToImperial(torqueEj1);
                    torque2Ej1 = Thorzion.TORQUE_UNITS.NEWTON_METER.unitToImperial(torque2Ej1);
                    rigEj1 = Thorzion.PRESSURE_UNITS.PASCAL.unitToInternational(tao);
                    rig2Ej1 = Thorzion.PRESSURE_UNITS.PASCAL.unitToInternational(tao);

                    TextView lblNomenclatureTao = (TextView) getView().findViewById(R.id.lblTaoEj1);
                    lblNomenclatureTao.setText(Thorzion.PRESSURE_UNITS.KSI.getNomenclature());
                    lblTorqueEj1.setText(Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature());
                    lblRigEj1.setText(Thorzion.PRESSURE_UNITS.KSI.getNomenclature());

                    TextView lblNomenclature2Tao = (TextView) getView().findViewById(R.id.lblTao2Ej1);
                    lblNomenclature2Tao.setText(Thorzion.PRESSURE_UNITS.KSI.getNomenclature());
                    lblTorque2Ej1.setText(Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature());
                    lblRig2Ej1.setText(Thorzion.PRESSURE_UNITS.KSI.getNomenclature());

                    opciones2 =(Spinner)getView().findViewById(R.id.opcMedEj1);
                    ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.ingles, android.R.layout.simple_spinner_item);
                    opciones2.setAdapter(adapter2);

                    opciones2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            double radioExterno = Double.parseDouble(txtRadioExterno.getText().toString());
                            double radioInterno = Double.parseDouble(txtRadioInterno.getText().toString());

                            double radioExternoCaso2 = Double.parseDouble(txtRadioExternoCaso2.getText().toString());
                            double radioInternoCaso2 = Double.parseDouble(txtRadioInternoCaso2.getText().toString());

                            double longEj1 = Double.parseDouble(txtLongEj1.getText().toString());
                            double long2Ej1 = Double.parseDouble(txtLong2Ej1.getText().toString());

                            DecimalFormat numberFormat = new DecimalFormat("#.####");
                            switch(position){
                                case 0:
                                    radioExterno = Thorzion.inchToFoot(radioExterno);
                                    radioInterno = Thorzion.inchToFoot(radioInterno);
                                    txtRadioInterno.setText(numberFormat.format(radioInterno), TextView.BufferType.EDITABLE);
                                    txtRadioExterno.setText(numberFormat.format(radioExterno), TextView.BufferType.EDITABLE);

                                    radioExternoCaso2 = Thorzion.inchToFoot(radioExternoCaso2);
                                    radioInternoCaso2 = Thorzion.inchToFoot(radioInternoCaso2);
                                    txtRadioInternoCaso2.setText(numberFormat.format(radioInternoCaso2), TextView.BufferType.EDITABLE);
                                    txtRadioExternoCaso2.setText(numberFormat.format(radioExternoCaso2), TextView.BufferType.EDITABLE);

                                    longEj1 = Thorzion.inchToFoot(longEj1);
                                    long2Ej1 = Thorzion.inchToFoot(long2Ej1);
                                    txtLongEj1.setText(numberFormat.format(longEj1), TextView.BufferType.EDITABLE);
                                    txtLong2Ej1.setText(numberFormat.format(long2Ej1), TextView.BufferType.EDITABLE);

                                    lblRadioIntEj1.setText(Thorzion.getFootNomenclature());
                                    lblRadioExtEj1.setText(Thorzion.getFootNomenclature());
                                    lblLongEj1.setText(Thorzion.getFootNomenclature());

                                    lblRadioInt2Ej1.setText(Thorzion.getFootNomenclature());
                                    lblRadioExt2Ej1.setText(Thorzion.getFootNomenclature());
                                    lblLong2Ej1.setText(Thorzion.getFootNomenclature());

                                    break;
                                case 1:
                                    radioExterno = Thorzion.footToInch(radioExterno);
                                    radioInterno = Thorzion.footToInch(radioInterno);
                                    txtRadioInterno.setText(numberFormat.format(radioInterno), TextView.BufferType.EDITABLE);
                                    txtRadioExterno.setText(numberFormat.format(radioExterno), TextView.BufferType.EDITABLE);

                                    radioExternoCaso2 = Thorzion.footToInch(radioExternoCaso2);
                                    radioInternoCaso2 = Thorzion.footToInch(radioInternoCaso2);
                                    txtRadioInternoCaso2.setText(numberFormat.format(radioInternoCaso2), TextView.BufferType.EDITABLE);
                                    txtRadioExternoCaso2.setText(numberFormat.format(radioExternoCaso2), TextView.BufferType.EDITABLE);

                                    longEj1 = Thorzion.footToInch(longEj1);
                                    long2Ej1 = Thorzion.footToInch(long2Ej1);
                                    txtLongEj1.setText(numberFormat.format(longEj1), TextView.BufferType.EDITABLE);
                                    txtLong2Ej1.setText(numberFormat.format(long2Ej1), TextView.BufferType.EDITABLE);

                                    lblRadioIntEj1.setText(Thorzion.getInchNomenclature());
                                    lblRadioExtEj1.setText(Thorzion.getInchNomenclature());
                                    lblLongEj1.setText(Thorzion.getInchNomenclature());

                                    lblRadioInt2Ej1.setText(Thorzion.getInchNomenclature());
                                    lblRadioExt2Ej1.setText(Thorzion.getInchNomenclature());
                                    lblLong2Ej1.setText(Thorzion.getInchNomenclature());

                                    break;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }

                txtTao.setText(tao+"", TextView.BufferType.EDITABLE);
                txtTaoCaso2.setText(taoCaso2+"", TextView.BufferType.EDITABLE);
                txtTorqueEj1.setText(torqueEj1+"", TextView.BufferType.EDITABLE);
                txtTorque2Ej1.setText(torque2Ej1+"", TextView.BufferType.EDITABLE);
                txtRigEj1.setText(rigEj1+"", TextView.BufferType.EDITABLE);
                txtRig2Ej1.setText(rig2Ej1+"", TextView.BufferType.EDITABLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }
}