package com.app.appTorsion.soluciones;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.app.appTorsion.R;
import com.app.appTorsion.activities.ExercisesActivity;
import com.app.appTorsion.activities.ProceduresActivity;
import com.app.appTorsion.helpers.Thorzion;
import com.app.appTorsion.models.Exercise;
import com.app.appTorsion.models.ProcedureActivity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class solucionEj6 extends Fragment implements Serializable {
    Button atras;
    Button resolver;
    Button solucion;

    EditText txtTorqueEj6, txtTorque2Ej6, txtRadioEj6, txtRadio2Ej6, txtRadio3Ej6, txtLongEj6, txtLong2Ej6, txtLong3Ej6, txtRigEj6;

    TextView lblTorqueEj6, lblRadios_LongsEj6, lblRigEj6;
    TextView txtSol1Ej6, txtSol2Ej6, txtSol3Ej6, txtSol4Ej6, txtSol5Ej6;
    TextView txtRadioEngAEj6, txtRadioEngBEj6, txtRadioEngCEj6;
    TextView lblRadios_Longs2Ej6, lblRadios_Longs3Ej6;

    TextView lblTorque2Ej6,lblRadios_Longs4Ej6,lblRadios_Longs5Ej6,lblRadios_Longs6Ej6,lblRadios_Longs7Ej6,lblRadios_Longs8Ej6,lblRadios_Longs9Ej6;

    Spinner opciones, orientacionCaso1, orientacionCaso2;
    Spinner opciones2;

    private Exercise exercise;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_solucion_ej6, container, false);
        exercise = (Exercise) getArguments().getSerializable("exercise");

        txtSol1Ej6 = (TextView) view.findViewById(R.id.txtSol1Ej6);
        txtSol2Ej6 = (TextView) view.findViewById(R.id.txtSol2Ej6);
        txtSol3Ej6 = (TextView) view.findViewById(R.id.txtSol3Ej6);
        txtSol4Ej6 = (TextView) view.findViewById(R.id.txtSol4Ej6);
        txtSol5Ej6 = (TextView) view.findViewById(R.id.txtSol5Ej6);

        txtTorqueEj6 = (EditText) view.findViewById(R.id.txtTorqueEj6);
        txtTorque2Ej6 = (EditText) view.findViewById(R.id.txtTorque2Ej6);
        txtRadioEj6 = (EditText) view.findViewById(R.id.txtRadioEj6);
        txtRadio2Ej6 = (EditText) view.findViewById(R.id.txtRadio2Ej6);
        txtRadio3Ej6 = (EditText) view.findViewById(R.id.txtRadio3Ej6);
        txtRadioEngAEj6 = (EditText) view.findViewById(R.id.txtRadioEngAEj6);
        txtRadioEngBEj6 = (EditText) view.findViewById(R.id.txtRadioEngBEj6);
        txtRadioEngCEj6 = (EditText) view.findViewById(R.id.txtRadioEngCEj6);


        txtLongEj6 = (EditText) view.findViewById(R.id.txtLongEj6);
        txtLong2Ej6 = (EditText) view.findViewById(R.id.txtLong2Ej6);
        txtLong3Ej6 = (EditText) view.findViewById(R.id.txtLong3Ej6);
        txtRigEj6 = (EditText) view.findViewById(R.id.txtRigEj6);

        lblTorqueEj6 = (TextView) view.findViewById(R.id.lblTorqueEj6);
        lblTorque2Ej6 = (TextView) view.findViewById(R.id.lblTorque2Ej6);
        lblRadios_LongsEj6 = (TextView) view.findViewById(R.id.lblRadios_LongsEj6);
        lblRadios_Longs2Ej6 = (TextView) view.findViewById(R.id.lblRadios_Longs2Ej6);
        lblRadios_Longs3Ej6 = (TextView) view.findViewById(R.id.lblRadios_Longs3Ej6);
        lblRadios_Longs4Ej6 = (TextView) view.findViewById(R.id.lblRadios_Longs4Ej6);
        lblRadios_Longs5Ej6 = (TextView) view.findViewById(R.id.lblRadios_Longs5Ej6);
        lblRadios_Longs6Ej6 = (TextView) view.findViewById(R.id.lblRadios_Longs6Ej6);
        lblRadios_Longs7Ej6 = (TextView) view.findViewById(R.id.lblRadios_Longs7Ej6);
        lblRadios_Longs8Ej6 = (TextView) view.findViewById(R.id.lblRadios_Longs8Ej6);
        lblRadios_Longs9Ej6 = (TextView) view.findViewById(R.id.lblRadios_Longs9Ej6);
        lblRigEj6 = (TextView) view.findViewById(R.id.lblRigEj6);

        resolver = (Button) view.findViewById(R.id.btnRes6);
        solucion = (Button) view.findViewById(R.id.btnSol6);

        resolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                exercise.setActivities(new ArrayList<>());

                double torqueEj6 = Double.parseDouble(txtTorqueEj6.getText().toString());
                double torque2Ej6 = Double.parseDouble(txtTorque2Ej6.getText().toString());
                double radioEj6 = Double.parseDouble(txtRadioEj6.getText().toString());
                double radio2Ej6 = Double.parseDouble(txtRadio2Ej6.getText().toString());
                double radio3Ej6 = Double.parseDouble(txtRadio3Ej6.getText().toString());
                double radioEngAEj6 = Double.parseDouble(txtRadioEngAEj6.getText().toString());
                double radioEngBEj6 = Double.parseDouble(txtRadioEngBEj6.getText().toString());
                double radioEngCEj6 = Double.parseDouble(txtRadioEngCEj6.getText().toString());
                double longEj6 = Double.parseDouble(txtLongEj6.getText().toString());
                double long2Ej6 = Double.parseDouble(txtLong2Ej6.getText().toString());
                double long3Ej6 = Double.parseDouble(txtLong3Ej6.getText().toString());
                double rigEj6 = Double.parseDouble(txtRigEj6.getText().toString());


                String nomneclaturePresure = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature():Thorzion.PRESSURE_UNITS.KSI.getNomenclature();

                DecimalFormat format = new DecimalFormat("#.###E0");
                DecimalFormat format2 = new DecimalFormat("##.###E0");
                DecimalFormat format3 = new DecimalFormat("#.####");

                String nomneclatureAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.ANGLE_UNITS.RAD.getNomenclature():Thorzion.ANGLE_UNITS.DEG.getNomenclature();

                double unidadAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torqueEj6, longEj6,radioEj6,rigEj6):Thorzion.getThorzionAngleRigid(torqueEj6, longEj6,radioEj6,rigEj6)*180/Math.PI;

                double unidadAngle2 = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torque2Ej6,long3Ej6,radio3Ej6,rigEj6):Thorzion.getThorzionAngleRigid(torque2Ej6,long3Ej6,radio3Ej6,rigEj6)*180/Math.PI;

                double unidadAngle3 = opciones.getSelectedItem().toString().equals("S.Internacional")?
                Thorzion.getThorzionAngleRigid(Thorzion.getTorqueSum(Thorzion.getTorqueMesh(Thorzion.getForce(torqueEj6,radioEngAEj6),radioEngBEj6),
                        Thorzion.getTorqueMesh(Thorzion.getForce(torque2Ej6,radioEngCEj6),radioEngBEj6)),long2Ej6,radio2Ej6,rigEj6):Thorzion.getThorzionAngleRigid(Thorzion.getTorqueSum(Thorzion.getTorqueMesh(Thorzion.getForce(torqueEj6,radioEngAEj6),radioEngBEj6),
                        Thorzion.getTorqueMesh(Thorzion.getForce(torque2Ej6,radioEngCEj6),radioEngBEj6)),long2Ej6,radio2Ej6,rigEj6)*180/Math.PI;

                double extAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getExtremeThorzionAngle(Thorzion.getThorzionAngleRigid(torqueEj6, longEj6,radioEj6,rigEj6),Thorzion.getThorzionAngle(radioEngBEj6,radioEngAEj6,Thorzion.getThorzionAngleRigid(Thorzion.getTorqueSum(Thorzion.getTorqueMesh(Thorzion.getForce(torqueEj6,radioEngAEj6),radioEngBEj6),
                                Thorzion.getTorqueMesh(Thorzion.getForce(torque2Ej6,radioEngCEj6),radioEngBEj6)),long2Ej6,radio2Ej6,rigEj6))):Thorzion.getExtremeThorzionAngle(Thorzion.getThorzionAngleRigid(torqueEj6, longEj6,radioEj6,rigEj6),Thorzion.getThorzionAngle(radioEngBEj6,radioEngAEj6,Thorzion.getThorzionAngleRigid(Thorzion.getTorqueSum(Thorzion.getTorqueMesh(Thorzion.getForce(torqueEj6,radioEngAEj6),radioEngBEj6),
                        Thorzion.getTorqueMesh(Thorzion.getForce(torque2Ej6,radioEngCEj6),radioEngBEj6)),long2Ej6,radio2Ej6,rigEj6)))*180/Math.PI;


                ProcedureActivity paso1 = new ProcedureActivity(
                        "Hallar τ en el punto AB",
                        "Procedemos a reemplazar los valores en la ecuación <b> T = ("+torqueEj6 + " " +lblTorqueEj6.getText()+" C = "+format.format(radioEj6) + " " +
                                lblRadios_LongsEj6.getText() + ", J = " + format.format(Thorzion.getInertiaConstant(radioEj6))+ lblRadios_LongsEj6.getText()+"^4 + " +
                                "</b>, " + "obteniendo como resultado un Esfuerzo Cortante Permisible = <b>"+
                                format.format(Thorzion.getTaoMaximus(torqueEj6,radioEj6)) + " " + nomneclaturePresure+ "en el punto AB" +"</b>",
                        R.drawable.ec2);

                ProcedureActivity paso2 = new ProcedureActivity(
                        "Hallar τ en el punto EF",
                        "Procedemos a reemplazar los valores en la ecuación <b> T = ("+torque2Ej6 + " " +lblTorqueEj6.getText()+", C = "+ format.format(radio3Ej6)+ "" +
                                lblRadios_LongsEj6.getText() + ", J = " + format.format(Thorzion.getInertiaConstant(radio3Ej6)) + lblRadios_LongsEj6.getText()+"^4 + " +
                                "</b>, " + "obteniendo como resultado un Esfuerzo Cortante Permisible = <b>"+
                                format2.format(Thorzion.getTaoMaximus(torque2Ej6,radio3Ej6)) + " " +nomneclaturePresure + " en el punto EF " + "</b>",
                        R.drawable.ec1);

                ProcedureActivity paso3 = new ProcedureActivity(
                        "Hallar τ en el punto CD",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T=("+Thorzion.getTorqueSum(Thorzion.getTorqueMesh(Thorzion.getForce(torqueEj6,radioEngAEj6),radioEngBEj6),
                                Thorzion.getTorqueMesh(Thorzion.getForce(torque2Ej6,radioEngCEj6),radioEngBEj6)) + " " +lblTorqueEj6.getText()+",  C = "+ format.format(radio2Ej6)+ "" +
                                lblRadios_LongsEj6.getText() + ", J = " + format.format(Thorzion.getInertiaConstant(radio2Ej6)) + lblRadios_LongsEj6.getText()+"^4 + " +
                                "</b>, " + "obteniendo como resultado un Esfuerzo Cortante Permisible = <b>"+
                                format2.format(Thorzion.getTaoMaximus(Thorzion.getTorqueSum(Thorzion.getTorqueMesh(Thorzion.getForce(torqueEj6,radioEngAEj6),radioEngBEj6),
                                        Thorzion.getTorqueMesh(Thorzion.getForce(torque2Ej6,radioEngCEj6),radioEngBEj6)),radio3Ej6))  + "  " +nomneclaturePresure + " en el punto CD" + "</b>",
                        R.drawable.ec1);

                ProcedureActivity paso4 = new ProcedureActivity(
                        "Hallar ángulo de torsión en el eje AB",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T=("+torqueEj6 + " " +lblTorqueEj6.getText()+", L = " + longEj6 + "" + lblRadios_LongsEj6.getText() +
                                ", J = " + format.format(Thorzion.getInertiaConstant(radioEj6)) + lblRadios_LongsEj6.getText()+ ", G = " + rigEj6 + ""+ lblRigEj6.getText()+""+ " </b>, " +
                                "obteniendo como resultado un ángulo de torsión = <b>"+ format3.format(unidadAngle) + " " + nomneclatureAngle+ " en el eje AB" +"</b>",
                        R.drawable.ec5);

                ProcedureActivity paso5 = new ProcedureActivity(
                        "Hallar ángulo de torsión en el eje EF",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T=("+torque2Ej6 + " " +lblTorqueEj6.getText()+", L = " + long3Ej6 + "" + lblRadios_LongsEj6.getText() +
                                ", J = " + format.format(Thorzion.getInertiaConstant(radio3Ej6)) + lblRadios_LongsEj6.getText()+ ", G = " + rigEj6 + ""+ lblRigEj6.getText()+""+ " </b>, " +
                                "obteniendo como resultado un ángulo de torsión en el eje EF = <b>"+ format3.format(unidadAngle2) + " " + nomneclatureAngle +" en el eje EF" +"</b>",
                        R.drawable.ec5);

                ProcedureActivity paso6 = new ProcedureActivity(
                        "Hallar ángulo de torsión en el eje CD",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T=("+Thorzion.getTorqueSum(Thorzion.getTorqueMesh(Thorzion.getForce(torqueEj6,radioEngAEj6),radioEngBEj6),
                                Thorzion.getTorqueMesh(Thorzion.getForce(torque2Ej6,radioEngCEj6),radioEngBEj6)) + " " +lblTorqueEj6.getText()+", L = " + long2Ej6 + "" + lblRadios_LongsEj6.getText() +
                                ", J = " + format.format(Thorzion.getInertiaConstant(radio2Ej6)) + lblRadios_LongsEj6.getText()+ ", G = " + rigEj6 + ""+ lblRigEj6.getText()+""+ " </b>, " +
                                "obteniendo como resultado un ángulo de torsión = <b>"+ format3.format(unidadAngle3) + " " + nomneclatureAngle +" en el eje CD" +"</b>",
                        R.drawable.ec5);

                ProcedureActivity paso7 = new ProcedureActivity(
                        "Hallar ángulo en el punto B",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> R = "+radioEngBEj6 + " " +lblRadios_LongsEj6.getText()+", ΦE = " + unidadAngle2 + "" + nomneclatureAngle +
                                ", ΦC = " + format.format(unidadAngle3) + nomneclatureAngle+ " </b>, " +
                                "obteniendo como resultado un ángulo  = <b>"+ format3.format((radioEngBEj6/unidadAngle2)*unidadAngle3) + " " + nomneclatureAngle + " en el punto B" + "</b>",
                        R.drawable.ec5);

                ProcedureActivity paso8 = new ProcedureActivity(
                        "Hallar ángulo rotacion extremo en el punto A",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> ΦB = "+format3.format((radioEngBEj6/unidadAngle2)*unidadAngle3) + " " +nomneclatureAngle+
                                ", ΦA = " + format3.format(unidadAngle) + "" + nomneclatureAngle + " </b>, " +
                                "obteniendo como resultado un ángulo rotacion extremo = <b>"+ format3.format(extAngle)+ " " +nomneclatureAngle+" en el punto A"+ "</b>",
                        R.drawable.ec5);





                exercise.addActivity(paso1);
                exercise.addActivity(paso2);
                exercise.addActivity(paso3);
                exercise.addActivity(paso4);
                exercise.addActivity(paso5);
                exercise.addActivity(paso6);
                exercise.addActivity(paso7);
                exercise.addActivity(paso8);

                Intent iniciar = new Intent(getActivity(), ProceduresActivity.class);
                exercise.setLayoutID(null);
                iniciar.putExtra("exercise", exercise);
                startActivity(iniciar);
            }
        });

        solucion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                exercise.setActivities(new ArrayList<>());

                double torqueEj6 = Double.parseDouble(txtTorqueEj6.getText().toString());
                double torque2Ej6 = Double.parseDouble(txtTorque2Ej6.getText().toString());
                double radioEj6 = Double.parseDouble(txtRadioEj6.getText().toString());
                double radio2Ej6 = Double.parseDouble(txtRadio2Ej6.getText().toString());
                double radio3Ej6 = Double.parseDouble(txtRadio3Ej6.getText().toString());
                double radioEngAEj6 = Double.parseDouble(txtRadioEngAEj6.getText().toString());
                double radioEngBEj6 = Double.parseDouble(txtRadioEngBEj6.getText().toString());
                double radioEngCEj6 = Double.parseDouble(txtRadioEngCEj6.getText().toString());
                double longEj6 = Double.parseDouble(txtLongEj6.getText().toString());
                double long2Ej6 = Double.parseDouble(txtLong2Ej6.getText().toString());
                double long3Ej6 = Double.parseDouble(txtLong3Ej6.getText().toString());
                double rigEj6 = Double.parseDouble(txtRigEj6.getText().toString());

                 DecimalFormat format = new DecimalFormat("#.###E0");
                DecimalFormat format2 = new DecimalFormat("##.###E0");
                DecimalFormat format3 = new DecimalFormat("#.####");

                String nomneclatureAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.ANGLE_UNITS.RAD.getNomenclature():Thorzion.ANGLE_UNITS.DEG.getNomenclature();

                double unidadAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torqueEj6, longEj6,radioEj6,rigEj6):Thorzion.getThorzionAngleRigid(torqueEj6, longEj6,radioEj6,rigEj6)*180/Math.PI;

                double unidadAngle2 = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torque2Ej6,long3Ej6,radio3Ej6,rigEj6):Thorzion.getThorzionAngleRigid(torque2Ej6,long3Ej6,radio3Ej6,rigEj6)*180/Math.PI;

                double unidadAngle3 = opciones.getSelectedItem().toString().equals("S.Internacional")?
                Thorzion.getThorzionAngleRigid(Thorzion.getTorqueSum(Thorzion.getTorqueMesh(Thorzion.getForce(torqueEj6,radioEngAEj6),radioEngBEj6),
                        Thorzion.getTorqueMesh(Thorzion.getForce(torque2Ej6,radioEngCEj6),radioEngBEj6)),long2Ej6,radio2Ej6,rigEj6):Thorzion.getThorzionAngleRigid(Thorzion.getTorqueSum(Thorzion.getTorqueMesh(Thorzion.getForce(torqueEj6,radioEngAEj6),radioEngBEj6),
                        Thorzion.getTorqueMesh(Thorzion.getForce(torque2Ej6,radioEngCEj6),radioEngBEj6)),long2Ej6,radio2Ej6,rigEj6)*180/Math.PI;

                double extAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getExtremeThorzionAngle(Thorzion.getThorzionAngleRigid(torqueEj6, longEj6,radioEj6,rigEj6),Thorzion.getThorzionAngle(radioEngBEj6,radioEngAEj6,Thorzion.getThorzionAngleRigid(Thorzion.getTorqueSum(Thorzion.getTorqueMesh(Thorzion.getForce(torqueEj6,radioEngAEj6),radioEngBEj6),
                                Thorzion.getTorqueMesh(Thorzion.getForce(torque2Ej6,radioEngCEj6),radioEngBEj6)),long2Ej6,radio2Ej6,rigEj6))):Thorzion.getExtremeThorzionAngle(Thorzion.getThorzionAngleRigid(torqueEj6, longEj6,radioEj6,rigEj6),Thorzion.getThorzionAngle(radioEngBEj6,radioEngAEj6,Thorzion.getThorzionAngleRigid(Thorzion.getTorqueSum(Thorzion.getTorqueMesh(Thorzion.getForce(torqueEj6,radioEngAEj6),radioEngBEj6),
                        Thorzion.getTorqueMesh(Thorzion.getForce(torque2Ej6,radioEngCEj6),radioEngBEj6)),long2Ej6,radio2Ej6,rigEj6)))*180/Math.PI;


                String nomneclaturePresure = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature():Thorzion.PRESSURE_UNITS.KSI.getNomenclature();



                txtSol1Ej6.setText(" R/: " + format.format(Thorzion.getTaoMaximus(torqueEj6,radioEj6)) + " " + nomneclaturePresure+ " Para el esfuerzo cortante permisible en el punto AB");
                txtSol2Ej6.setText(" R/: " + format2.format(Thorzion.getTaoMaximus(torque2Ej6,radio2Ej6)) + " " +nomneclaturePresure  + " Para el esfuerzo cortante permisible en ell punto EF ");
                txtSol3Ej6.setText(" R/: " + format2.format(Thorzion.getTaoMaximus(Thorzion.getTorqueSum(Thorzion.getTorqueMesh(Thorzion.getForce(torqueEj6,radioEngAEj6),radioEngBEj6),
                        Thorzion.getTorqueMesh(Thorzion.getForce(torque2Ej6,radioEngCEj6),radioEngBEj6)),radio3Ej6))  + "  " +nomneclaturePresure  + " Para el esfuerzo cortante permisible en el punto CD");
                txtSol4Ej6.setText(" R/: " + format3.format(unidadAngle) +" "+ nomneclatureAngle+ " Para el angulo en el eje AB, " + format3.format(unidadAngle2) + " "+nomneclatureAngle
                                + " en el eje EF y " + format3.format(unidadAngle3) + " " +nomneclatureAngle+" Para el angulo en el eje CD " );
                txtSol5Ej6.setText(" R/: " + format3.format(extAngle)+ " " +nomneclatureAngle+" Para el angulo en el extremo A ");



                exercise.setLayoutID(null);
            }
        });

        atras = (Button) view.findViewById(R.id.btnAtr6);

        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent atras = new Intent(getActivity(), ExercisesActivity.class);
                exercise.setLayoutID(null);
                atras.putExtra("exercise", exercise);
                startActivity(atras);

            }
        });

        opciones = (Spinner) view.findViewById(R.id.opcSistEj6);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.opciones, android.R.layout.simple_spinner_item);
        opciones.setAdapter(adapter);

        opciones2 = (Spinner) view.findViewById(R.id.opcMedEj6);
/*
        orientacionCaso1 = (Spinner) view.findViewById(R.id.opcOriTor1Ej6);
        ArrayAdapter<CharSequence> adapterC1 = ArrayAdapter.createFromResource(getActivity(), R.array.SentidoTorque, android.R.layout.simple_spinner_item);
        orientacionCaso1.setAdapter(adapterC1);

        orientacionCaso2 = (Spinner) view.findViewById(R.id.opcOriTor2Ej6);
        ArrayAdapter<CharSequence> adapterC2 = ArrayAdapter.createFromResource(getActivity(), R.array.SentidoTorque, android.R.layout.simple_spinner_item);
        orientacionCaso2.setAdapter(adapterC2);
*/
        // Inflate the layout for this fragment
        return view;

    }

    @Override
    public void onStart () {
        super.onStart();
        this.opciones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here

                double torqueEj6 = Double.parseDouble(txtTorqueEj6.getText().toString());
                txtTorqueEj6.setText("0", TextView.BufferType.EDITABLE);

                double torque2Ej6 = Double.parseDouble(txtTorque2Ej6.getText().toString());
                txtTorque2Ej6.setText("0", TextView.BufferType.EDITABLE);

                txtRadioEj6.setText("0", TextView.BufferType.EDITABLE);
                txtRadio2Ej6.setText("0", TextView.BufferType.EDITABLE);
                txtRadio3Ej6.setText("0", TextView.BufferType.EDITABLE);
                txtLongEj6.setText("0", TextView.BufferType.EDITABLE);
                txtLong2Ej6.setText("0", TextView.BufferType.EDITABLE);
                txtLong3Ej6.setText("0", TextView.BufferType.EDITABLE);


                double rigEj6 = Double.parseDouble(txtRigEj6.getText().toString());


                if (position == 0) {
                    torqueEj6 = Thorzion.TORQUE_UNITS.POUND_INCH.unitToInternational(torqueEj6);
                    torque2Ej6 = Thorzion.TORQUE_UNITS.POUND_INCH.unitToInternational(torque2Ej6);
                    rigEj6 = Thorzion.PRESSURE_UNITS.KSI.unitToInternational(rigEj6);

                    lblTorqueEj6.setText(Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature());
                    lblTorque2Ej6.setText(Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature());
                    lblRigEj6.setText(Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature());

                    opciones2 = (Spinner) getView().findViewById(R.id.opcMedEj6);
                    ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.metric, android.R.layout.simple_spinner_item);
                    opciones2.setAdapter(adapter2);
                    opciones2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            double radioEj6 = Double.parseDouble(txtRadioEj6.getText().toString());
                            double radio2Ej6 = Double.parseDouble(txtRadio2Ej6.getText().toString());
                            double radio3Ej6 = Double.parseDouble(txtRadio3Ej6.getText().toString());
                            double radioEngAEj6 = Double.parseDouble(txtRadioEngAEj6.getText().toString());
                            double radioEngBEj6 = Double.parseDouble(txtRadioEngBEj6.getText().toString());
                            double radioEngCEj6 = Double.parseDouble(txtRadioEngCEj6.getText().toString());
                            double longEj6 = Double.parseDouble(txtLongEj6.getText().toString());
                            double long2Ej6 = Double.parseDouble(txtLong2Ej6.getText().toString());
                            double long3Ej6 = Double.parseDouble(txtLong3Ej6.getText().toString());


                            DecimalFormat numberFormat = new DecimalFormat("#.####");
                            switch (position) {
                                case 0:
                                    radioEj6 = Thorzion.milimeterToMeter(radioEj6);
                                    radio2Ej6 = Thorzion.milimeterToMeter(radio2Ej6);
                                    radio3Ej6 = Thorzion.milimeterToMeter(radio3Ej6);
                                    radioEngAEj6 = Thorzion.milimeterToMeter(radioEngAEj6);
                                    radioEngBEj6 = Thorzion.milimeterToMeter(radioEngBEj6);
                                    radioEngCEj6 = Thorzion.milimeterToMeter(radioEngCEj6);
                                    longEj6 = Thorzion.milimeterToMeter(longEj6);
                                    long2Ej6 = Thorzion.milimeterToMeter(long2Ej6);
                                    long3Ej6 = Thorzion.milimeterToMeter(long3Ej6);


                                    txtRadioEj6.setText(numberFormat.format(radioEj6), TextView.BufferType.EDITABLE);
                                    txtRadio2Ej6.setText(numberFormat.format(radio2Ej6), TextView.BufferType.EDITABLE);
                                    txtRadio3Ej6.setText(numberFormat.format(radio3Ej6), TextView.BufferType.EDITABLE);
                                    txtRadioEngAEj6.setText(numberFormat.format(radioEngAEj6), TextView.BufferType.EDITABLE);
                                    txtRadioEngBEj6.setText(numberFormat.format(radioEngBEj6), TextView.BufferType.EDITABLE);
                                    txtRadioEngCEj6.setText(numberFormat.format(radioEngCEj6), TextView.BufferType.EDITABLE);
                                    txtLongEj6.setText(numberFormat.format(longEj6), TextView.BufferType.EDITABLE);
                                    txtLong2Ej6.setText(numberFormat.format(long2Ej6), TextView.BufferType.EDITABLE);
                                    txtLong3Ej6.setText(numberFormat.format(long3Ej6), TextView.BufferType.EDITABLE);


                                    lblRadios_LongsEj6.setText(Thorzion.getMeterNomenclature());
                                    lblRadios_Longs2Ej6.setText(Thorzion.getMeterNomenclature());
                                    lblRadios_Longs3Ej6.setText(Thorzion.getMeterNomenclature());
                                    lblRadios_Longs4Ej6.setText(Thorzion.getMeterNomenclature());
                                    lblRadios_Longs5Ej6.setText(Thorzion.getMeterNomenclature());
                                    lblRadios_Longs6Ej6.setText(Thorzion.getMeterNomenclature());
                                    lblRadios_Longs7Ej6.setText(Thorzion.getMeterNomenclature());
                                    lblRadios_Longs8Ej6.setText(Thorzion.getMeterNomenclature());
                                    lblRadios_Longs9Ej6.setText(Thorzion.getMeterNomenclature());


                                    break;
                                case 1:
                                    radioEj6 = Thorzion.meterToMilimeter(radioEj6);
                                    radio2Ej6 = Thorzion.meterToMilimeter(radio2Ej6);
                                    radio3Ej6 = Thorzion.meterToMilimeter(radio3Ej6);
                                    radioEngAEj6 = Thorzion.meterToMilimeter(radioEngAEj6);
                                    radioEngBEj6 = Thorzion.meterToMilimeter(radioEngBEj6);
                                    radioEngCEj6 = Thorzion.meterToMilimeter(radioEngCEj6);
                                    longEj6 = Thorzion.meterToMilimeter(longEj6);
                                    long2Ej6 = Thorzion.meterToMilimeter(long2Ej6);
                                    long3Ej6 = Thorzion.meterToMilimeter(long3Ej6);


                                    txtRadioEj6.setText(numberFormat.format(radioEj6), TextView.BufferType.EDITABLE);
                                    txtRadio2Ej6.setText(numberFormat.format(radio2Ej6), TextView.BufferType.EDITABLE);
                                    txtRadio3Ej6.setText(numberFormat.format(radio3Ej6), TextView.BufferType.EDITABLE);
                                    txtRadioEngAEj6.setText(numberFormat.format(radioEngAEj6), TextView.BufferType.EDITABLE);
                                    txtRadioEngBEj6.setText(numberFormat.format(radioEngBEj6), TextView.BufferType.EDITABLE);
                                    txtRadioEngCEj6.setText(numberFormat.format(radioEngCEj6), TextView.BufferType.EDITABLE);
                                    txtLongEj6.setText(numberFormat.format(longEj6), TextView.BufferType.EDITABLE);
                                    txtLong2Ej6.setText(numberFormat.format(long2Ej6), TextView.BufferType.EDITABLE);
                                    txtLong3Ej6.setText(numberFormat.format(long3Ej6), TextView.BufferType.EDITABLE);


                                    lblRadios_LongsEj6.setText(Thorzion.getMiliMeterNomenclature());
                                    lblRadios_Longs2Ej6.setText(Thorzion.getMiliMeterNomenclature());
                                    lblRadios_Longs3Ej6.setText(Thorzion.getMiliMeterNomenclature());
                                    lblRadios_Longs4Ej6.setText(Thorzion.getMiliMeterNomenclature());
                                    lblRadios_Longs5Ej6.setText(Thorzion.getMiliMeterNomenclature());
                                    lblRadios_Longs6Ej6.setText(Thorzion.getMiliMeterNomenclature());
                                    lblRadios_Longs7Ej6.setText(Thorzion.getMiliMeterNomenclature());
                                    lblRadios_Longs8Ej6.setText(Thorzion.getMiliMeterNomenclature());
                                    lblRadios_Longs9Ej6.setText(Thorzion.getMiliMeterNomenclature());

                                    break;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else if (position == 1) {
                    torqueEj6 = Thorzion.TORQUE_UNITS.NEWTON_METER.unitToImperial(torqueEj6);
                    torque2Ej6 = Thorzion.TORQUE_UNITS.NEWTON_METER.unitToImperial(torque2Ej6);
                    rigEj6 = Thorzion.PRESSURE_UNITS.PASCAL.unitToImperial(rigEj6);

                    TextView lblTorqueEj6 = (TextView) getView().findViewById(R.id.lblTorqueEj6);
                    lblTorqueEj6.setText(Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature());
                    TextView lblTorque2Ej6 = (TextView) getView().findViewById(R.id.lblTorque2Ej6);
                    lblTorque2Ej6.setText(Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature());
                    lblRigEj6.setText(Thorzion.PRESSURE_UNITS.KSI.getNomenclature());

                    opciones2 = (Spinner) getView().findViewById(R.id.opcMedEj6);
                    ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.ingles, android.R.layout.simple_spinner_item);
                    opciones2.setAdapter(adapter2);

                    opciones2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            double radioEj6 = Double.parseDouble(txtRadioEj6.getText().toString());
                            double radio2Ej6 = Double.parseDouble(txtRadio2Ej6.getText().toString());
                            double radio3Ej6 = Double.parseDouble(txtRadio3Ej6.getText().toString());
                            double radioEngAEj6 = Double.parseDouble(txtRadioEngAEj6.getText().toString());
                            double radioEngBEj6 = Double.parseDouble(txtRadioEngBEj6.getText().toString());
                            double radioEngCEj6 = Double.parseDouble(txtRadioEngCEj6.getText().toString());
                            double longEj6 = Double.parseDouble(txtLongEj6.getText().toString());
                            double long2Ej6 = Double.parseDouble(txtLong2Ej6.getText().toString());
                            double long3Ej6 = Double.parseDouble(txtLong3Ej6.getText().toString());

                            DecimalFormat numberFormat = new DecimalFormat("#.####");
                            switch (position) {
                                case 0:

                                    radioEj6 = Thorzion.inchToFoot(radioEj6);
                                    radio2Ej6 = Thorzion.inchToFoot(radio2Ej6);
                                    radio3Ej6 = Thorzion.inchToFoot(radio3Ej6);
                                    radioEngAEj6 = Thorzion.inchToFoot(radioEngAEj6);
                                    radioEngBEj6 = Thorzion.inchToFoot(radioEngBEj6);
                                    radioEngCEj6 = Thorzion.inchToFoot(radioEngCEj6);
                                    longEj6 = Thorzion.inchToFoot(longEj6);
                                    long2Ej6 = Thorzion.inchToFoot(long2Ej6);
                                    long3Ej6 = Thorzion.inchToFoot(long3Ej6);

                                    txtRadioEj6.setText(numberFormat.format(radioEj6), TextView.BufferType.EDITABLE);
                                    txtRadio2Ej6.setText(numberFormat.format(radio2Ej6), TextView.BufferType.EDITABLE);
                                    txtRadio3Ej6.setText(numberFormat.format(radio3Ej6), TextView.BufferType.EDITABLE);
                                    txtRadioEngAEj6.setText(numberFormat.format(radioEngAEj6), TextView.BufferType.EDITABLE);
                                    txtRadioEngBEj6.setText(numberFormat.format(radioEngBEj6), TextView.BufferType.EDITABLE);
                                    txtRadioEngCEj6.setText(numberFormat.format(radioEngCEj6), TextView.BufferType.EDITABLE);
                                    txtLongEj6.setText(numberFormat.format(longEj6), TextView.BufferType.EDITABLE);
                                    txtLong2Ej6.setText(numberFormat.format(long2Ej6), TextView.BufferType.EDITABLE);
                                    txtLong3Ej6.setText(numberFormat.format(long3Ej6), TextView.BufferType.EDITABLE);


                                    lblRadios_LongsEj6.setText(Thorzion.getFootNomenclature());
                                    lblRadios_Longs2Ej6.setText(Thorzion.getFootNomenclature());
                                    lblRadios_Longs3Ej6.setText(Thorzion.getFootNomenclature());
                                    lblRadios_Longs4Ej6.setText(Thorzion.getFootNomenclature());
                                    lblRadios_Longs5Ej6.setText(Thorzion.getFootNomenclature());
                                    lblRadios_Longs6Ej6.setText(Thorzion.getFootNomenclature());
                                    lblRadios_Longs7Ej6.setText(Thorzion.getFootNomenclature());
                                    lblRadios_Longs8Ej6.setText(Thorzion.getFootNomenclature());
                                    lblRadios_Longs9Ej6.setText(Thorzion.getFootNomenclature());


                                    break;
                                case 1:

                                    radioEj6 = Thorzion.footToInch(radioEj6);
                                    radio2Ej6 = Thorzion.footToInch(radio2Ej6);
                                    radio3Ej6 = Thorzion.footToInch(radio3Ej6);
                                    radioEngAEj6 = Thorzion.footToInch(radioEngAEj6);
                                    radioEngBEj6 = Thorzion.footToInch(radioEngBEj6);
                                    radioEngCEj6 = Thorzion.footToInch(radioEngCEj6);
                                    longEj6 = Thorzion.footToInch(longEj6);
                                    long2Ej6 = Thorzion.footToInch(long2Ej6);
                                    long3Ej6 = Thorzion.footToInch(long3Ej6);


                                    txtRadioEj6.setText(numberFormat.format(radioEj6), TextView.BufferType.EDITABLE);
                                    txtRadio2Ej6.setText(numberFormat.format(radio2Ej6), TextView.BufferType.EDITABLE);
                                    txtRadio3Ej6.setText(numberFormat.format(radio3Ej6), TextView.BufferType.EDITABLE);
                                    txtRadioEngAEj6.setText(numberFormat.format(radioEngAEj6), TextView.BufferType.EDITABLE);
                                    txtRadioEngBEj6.setText(numberFormat.format(radioEngBEj6), TextView.BufferType.EDITABLE);
                                    txtRadioEngCEj6.setText(numberFormat.format(radioEngCEj6), TextView.BufferType.EDITABLE);
                                    txtLongEj6.setText(numberFormat.format(longEj6), TextView.BufferType.EDITABLE);
                                    txtLong2Ej6.setText(numberFormat.format(long2Ej6), TextView.BufferType.EDITABLE);
                                    txtLong3Ej6.setText(numberFormat.format(long3Ej6), TextView.BufferType.EDITABLE);


                                    lblRadios_LongsEj6.setText(Thorzion.getInchNomenclature());
                                    lblRadios_Longs2Ej6.setText(Thorzion.getInchNomenclature());
                                    lblRadios_Longs3Ej6.setText(Thorzion.getInchNomenclature());
                                    lblRadios_Longs4Ej6.setText(Thorzion.getInchNomenclature());
                                    lblRadios_Longs5Ej6.setText(Thorzion.getInchNomenclature());
                                    lblRadios_Longs6Ej6.setText(Thorzion.getInchNomenclature());
                                    lblRadios_Longs7Ej6.setText(Thorzion.getInchNomenclature());
                                    lblRadios_Longs8Ej6.setText(Thorzion.getInchNomenclature());
                                    lblRadios_Longs9Ej6.setText(Thorzion.getInchNomenclature());


                                    break;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }

                txtTorqueEj6.setText(torqueEj6 + "", TextView.BufferType.EDITABLE);
                txtTorque2Ej6.setText(torque2Ej6 + "", TextView.BufferType.EDITABLE);
                txtRigEj6.setText(rigEj6 + "", TextView.BufferType.EDITABLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
    }
}
