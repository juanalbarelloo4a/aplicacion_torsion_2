package com.app.appTorsion.soluciones;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.app.appTorsion.R;
import com.app.appTorsion.activities.ExercisesActivity;
import com.app.appTorsion.activities.ProceduresActivity;
import com.app.appTorsion.helpers.Thorzion;
import com.app.appTorsion.models.Exercise;
import com.app.appTorsion.models.ProcedureActivity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class solucionEj2 extends Fragment implements Serializable {

    Button atras;
    Button resolver;
    Button solucion;

    EditText txtTao,txtTorque,txtTaoEj2Caso2, txtTorqueEj2Caso2;
    EditText txtRadioExtEj2,txtRadioExt2Ej2,txtRadioIntEj2,txtRadioInt2Ej2,txtLongEj2,txtLong2Ej2,txtRigEj2,txtRig2Ej2;

    TextView lblTorqueEj2, lblTaoEj2, txtSol2, txtSol2_1;
    TextView lblTorque2Ej2, lblTao2Ej2,lblRig2Ej2;
    TextView lblRadio_LongsEj2,lblRigEj2, txtSol2_2;
    TextView lblRadio_Longs2Ej2,lblRadio_Longs3Ej2;
    TextView lblRadio_Longs4Ej2,lblRadio_Longs5Ej2,lblRadio_Longs6Ej2;

    Spinner opciones, orientacionCaso1,orientacionCaso2;
    Spinner opciones2;

    private Exercise exercise;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_solucion_ej2, container, false);

        exercise = (Exercise) getArguments().getSerializable("exercise");

        txtSol2 = (TextView) view.findViewById(R.id.txtSolEj2);
        txtSol2_1 = (TextView) view.findViewById(R.id.txtSol2_1);
        txtTao = (EditText) view.findViewById(R.id.txtTao2);
        txtTorque = (EditText) view.findViewById(R.id.txtTorque);
        txtTaoEj2Caso2 = (EditText) view.findViewById(R.id.txtTaoEj2Caso2);
        txtTorqueEj2Caso2 = (EditText) view.findViewById(R.id.txtTorqueEj2Caso2);

        txtRadioExtEj2 = (EditText) view.findViewById(R.id.txtRadioExtEj2);
        txtRadioExt2Ej2 = (EditText) view.findViewById(R.id.txtRadioExt2Ej2);
        txtRadioIntEj2 = (EditText) view.findViewById(R.id.txtRadioIntEj2);
        txtRadioInt2Ej2 = (EditText) view.findViewById(R.id.txtRadioInt2Ej2);
        txtLongEj2 = (EditText) view.findViewById(R.id.txtLongEj2);
        txtLong2Ej2 = (EditText) view.findViewById(R.id.txtLong2Ej2);
        txtRigEj2 = (EditText) view.findViewById(R.id.txtRigEj2);
        txtRig2Ej2 = (EditText) view.findViewById(R.id.txtRig2Ej2);

        lblTorqueEj2 = (TextView) view.findViewById(R.id.lblTorqueEj2);
        lblTaoEj2 = (TextView) view.findViewById(R.id.lblTaoEj2);

        lblTorque2Ej2 = (TextView) view.findViewById(R.id.lblTorque2Ej2);
        lblTao2Ej2 = (TextView) view.findViewById(R.id.lblTao2Ej2);

        lblRadio_LongsEj2 = (TextView) view.findViewById(R.id.lblRadio_LongsEj2);
        lblRadio_Longs2Ej2 = (TextView) view.findViewById(R.id.lblRadio_Longs2Ej2);
        lblRadio_Longs3Ej2 = (TextView) view.findViewById(R.id.lblRadio_Longs3Ej2);

        lblRadio_Longs4Ej2 = (TextView) view.findViewById(R.id.lblRadio_Longs4Ej2);
        lblRadio_Longs5Ej2 = (TextView) view.findViewById(R.id.lblRadio_Longs5Ej2);
        lblRadio_Longs6Ej2 = (TextView) view.findViewById(R.id.lblRadio_Longs6Ej2);

        lblRigEj2 = (TextView) view.findViewById(R.id.lblRigEj2);
        lblRig2Ej2 = (TextView) view.findViewById(R.id.lblRig2Ej2);

        txtSol2_2 = (TextView) view.findViewById(R.id.txtSol2_2);


        resolver = (Button) view.findViewById(R.id.btnRes2);
        solucion = (Button) view.findViewById(R.id.btnSol2);

        resolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                exercise.setActivities(new ArrayList<>());

                double tao = Double.parseDouble(txtTao.getText().toString());
                double torque = Double.parseDouble(txtTorque.getText().toString());
                double taoEj2Caso2 = Double.parseDouble(txtTaoEj2Caso2.getText().toString());
                double torqueEj2Caso2 = Double.parseDouble(txtTorqueEj2Caso2.getText().toString());

                double radioExtEj2 = Double.parseDouble(txtRadioExtEj2.getText().toString());
                double radioExt2Ej2 = Double.parseDouble(txtRadioExt2Ej2.getText().toString());
                double radioIntEj2 = Double.parseDouble(txtRadioIntEj2.getText().toString());
                double radioInt2Ej2 = Double.parseDouble(txtRadioInt2Ej2.getText().toString());
                double longEj2 = Double.parseDouble(txtLongEj2.getText().toString());
                double long2Ej2 = Double.parseDouble(txtLong2Ej2.getText().toString());
                double rigEj2 = Double.parseDouble(txtRigEj2.getText().toString());
                double rig2Ej2 = Double.parseDouble(txtRig2Ej2.getText().toString());


                String nomneclatureDistance = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getMiliMeterNomenclature():Thorzion.getInchNomenclature();

                String nomneclaturePresure = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature():Thorzion.PRESSURE_UNITS.KSI.getNomenclature();

                String nomneclatureAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.ANGLE_UNITS.RAD.getNomenclature():Thorzion.ANGLE_UNITS.DEG.getNomenclature();

                DecimalFormat format = new DecimalFormat("##.###E0");
                DecimalFormat format1 = new DecimalFormat("#.##");
                DecimalFormat format2 = new DecimalFormat("#.######");
                DecimalFormat format3 = new DecimalFormat("#.###");


                double unidadAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torque,longEj2,radioExtEj2,radioIntEj2,rigEj2):Thorzion.getThorzionAngleRigid(torque,longEj2,radioExtEj2,radioIntEj2,rigEj2)*180/Math.PI;

                double unidadAngle2 = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torqueEj2Caso2,long2Ej2,radioExt2Ej2,radioInt2Ej2,rig2Ej2):Thorzion.getThorzionAngleRigid(torqueEj2Caso2,long2Ej2,radioExt2Ej2,radioInt2Ej2,rig2Ej2)*180/Math.PI;


                torque = orientacionCaso1.getSelectedItem().toString().equals(orientacionCaso2.getSelectedItem().toString())?
                        torque+torqueEj2Caso2:
                        torque-torqueEj2Caso2;

                ProcedureActivity paso1 = new ProcedureActivity(
                        "Modulo 1",
                        "En este módulo se procederá a encontrar los diámetros correspondientes al tubo en la sección AB y a la barra en la sección BC por medio del esfuerzo cortante permisible y los torques",
                        R.drawable.mod1);

                ProcedureActivity paso2 = new ProcedureActivity(
                        "Modulo 1: Hallar Diámetro para sección AB",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T ="+torque+" "+ lblTorqueEj2.getText() +", τ ="+tao+
                                " "+ lblTaoEj2.getText() +" "+" </b>, obteniendo como resultado un Diametro = "+ String.format("%.2f", Thorzion.getDiameter(Thorzion.getRadius(tao,torque))*1000) + " " + nomneclatureDistance,
                        R.drawable.ec4);

                ProcedureActivity paso3 = new ProcedureActivity(
                        "Modulo 1: Hallar Diámetro para sección BC",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T ="+torqueEj2Caso2+" "+ lblTorqueEj2.getText() +", τ ="+taoEj2Caso2+
                                " "+ lblTaoEj2.getText() +" "+" </b>, obteniendo como resultado un Diametro = " + String.format("%.2f", Thorzion.getDiameter(Thorzion.getRadius(taoEj2Caso2,torqueEj2Caso2))*1000) + " "
                                + nomneclatureDistance,
                        R.drawable.ec4);

                ProcedureActivity paso4 = new ProcedureActivity(
                        "Modulo 1: Resultados",
                        "Los diámetros mínimos obtenidos con los cuales no se excede el esfuerzo cortante permisible en el sistema son: " +
                                String.format("%.2f", Thorzion.getDiameter(Thorzion.getRadius(tao,torque))*1000) + " " + nomneclatureDistance + " para el diámetro en AB " + " y "
                                + String.format("%.2f", Thorzion.getDiameter(Thorzion.getRadius(taoEj2Caso2,torqueEj2Caso2))*1000) + " " + nomneclatureDistance + " para el diámetro en BC ",
                        R.drawable.ec4);

                ProcedureActivity paso5 = new ProcedureActivity(
                        "Modulo 2",
                        "En este módulo se procederá a encontrar los esfuerzos cortantes permisibles y los ángulos de torsión del tubo en AB y la barra en BC por medio del" +
                                "torque, el módulo de rigidez del material, los radios y las longitudes de cada uno de los objetos",
                        R.drawable.mod2);

                ProcedureActivity paso6 = new ProcedureActivity(
                        "Modulo 2: Hallar esfuerzo cortante permisible Vástago",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T = " + torque + " " + lblTorqueEj2.getText()  + ","+
                                "  J  = " + format2.format(Thorzion.getInertiaConstant(radioExtEj2,radioIntEj2)) + lblRadio_LongsEj2.getText()+"^4, y "
                                + "C = " + radioExtEj2 + lblRadio_LongsEj2.getText() + "\n" +
                                " </b>, obteniendo como resultado un<b> τ= " +
                                format.format(Thorzion.getTaoMaximus(torque,radioExtEj2,radioIntEj2)) + " " + nomneclaturePresure+"</b>",
                        R.drawable.ec2);

                ProcedureActivity paso7 = new ProcedureActivity(
                        "Modulo 2: Hallar esfuerzo cortante permisible Manga",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T = " + torqueEj2Caso2 + " " + lblTorqueEj2.getText()  + ","+
                                "  J  = " + format2.format(Thorzion.getInertiaConstant(radioExt2Ej2,radioInt2Ej2)) + lblRadio_LongsEj2.getText()+"^4, y "
                                + "C = " + radioExt2Ej2 + lblRadio_LongsEj2.getText() + "\n" +
                                " </b>, obteniendo como resultado un<b> τ= " +
                                format.format(Thorzion.getTaoMaximus(torqueEj2Caso2,radioExt2Ej2,radioInt2Ej2)) + " " + nomneclaturePresure+"</b>",
                        R.drawable.ec2);

                ProcedureActivity paso8 = new ProcedureActivity(
                        "Modulo 2: Hallar ángulo de torsión en el Vástago",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T=(" +torque + " " + lblTorqueEj2.getText()+", L = " + longEj2 + "" + lblRadio_LongsEj2.getText() +
                                ", J = " + format2.format(Thorzion.getInertiaConstant(radioExtEj2,radioIntEj2)) + lblRadio_LongsEj2.getText()+ ", G = " + rigEj2 + ""+ lblRigEj2.getText()+""+ " </b>, " +
                                "obteniendo como resultado un ángulo = <b>"+ format3.format(unidadAngle)  + nomneclatureAngle + "</b>",
                        R.drawable.ec5);

                ProcedureActivity paso9 = new ProcedureActivity(
                        "Modulo 2: Hallar ángulo de torsión en la Manga",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T=(" +torqueEj2Caso2 + " " + lblTorqueEj2.getText()+", L = " + long2Ej2 + "" + lblRadio_LongsEj2.getText() +
                                ", J = " + format2.format(Thorzion.getInertiaConstant(radioExt2Ej2,radioInt2Ej2)) + lblRadio_LongsEj2.getText()+ ", G = " + rig2Ej2 + ""+ lblRigEj2.getText()+""+ " </b>, " +
                                "obteniendo como resultado un ángulo = <b>"+ format3.format(unidadAngle2)  + nomneclatureAngle + "</b>",
                        R.drawable.ec5);


                exercise.addActivity(paso1);
                exercise.addActivity(paso2);
                exercise.addActivity(paso3);
                exercise.addActivity(paso4);
                exercise.addActivity(paso5);
                exercise.addActivity(paso6);
                exercise.addActivity(paso7);
                exercise.addActivity(paso8);
                exercise.addActivity(paso9);

                Intent iniciar = new Intent(getActivity(), ProceduresActivity.class);
                exercise.setLayoutID(null);
                iniciar.putExtra("exercise", exercise);
                startActivity(iniciar);
            }
        });

        solucion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                exercise.setActivities(new ArrayList<>());

                double tao = Double.parseDouble(txtTao.getText().toString());
                double torque = Double.parseDouble(txtTorque.getText().toString());
                double taoEj2Caso2 = Double.parseDouble(txtTaoEj2Caso2.getText().toString());
                double torqueEj2Caso2 = Double.parseDouble(txtTorqueEj2Caso2.getText().toString());

                double radioExtEj2 = Double.parseDouble(txtRadioExtEj2.getText().toString());
                double radioExt2Ej2 = Double.parseDouble(txtRadioExt2Ej2.getText().toString());
                double radioIntEj2 = Double.parseDouble(txtRadioIntEj2.getText().toString());
                double radioInt2Ej2 = Double.parseDouble(txtRadioInt2Ej2.getText().toString());
                double longEj2 = Double.parseDouble(txtLongEj2.getText().toString());
                double long2Ej2 = Double.parseDouble(txtLong2Ej2.getText().toString());
                double rigEj2 = Double.parseDouble(txtRigEj2.getText().toString());
                double rig2Ej2 = Double.parseDouble(txtRig2Ej2.getText().toString());

                torque = orientacionCaso1.getSelectedItem().toString().equals(orientacionCaso2.getSelectedItem().toString())?
                        torque+torqueEj2Caso2:
                        torque-torqueEj2Caso2;

                String nomneclatureDistance = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getMiliMeterNomenclature():Thorzion.getInchNomenclature();

                String nomneclaturePresure = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature():Thorzion.PRESSURE_UNITS.KSI.getNomenclature();

                String nomneclatureAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.ANGLE_UNITS.RAD.getNomenclature():Thorzion.ANGLE_UNITS.DEG.getNomenclature();

                DecimalFormat format = new DecimalFormat("##.###E0");
                DecimalFormat format1 = new DecimalFormat("#.##");
                DecimalFormat format2 = new DecimalFormat("#.###");

                double unidadAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torque,longEj2,radioExtEj2,radioIntEj2,rigEj2):Thorzion.getThorzionAngleRigid(torque,longEj2,radioExtEj2,radioIntEj2,rigEj2)*180/Math.PI;

                double unidadAngle2 = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torqueEj2Caso2,long2Ej2,radioExt2Ej2,radioInt2Ej2,rig2Ej2):Thorzion.getThorzionAngleRigid(torqueEj2Caso2,long2Ej2,radioExt2Ej2,radioInt2Ej2,rig2Ej2)*180/Math.PI;


                txtSol2.setText(" R/: " + format1.format(Thorzion.getDiameter(Thorzion.getRadius(tao,torque))*1000) + " " + nomneclatureDistance + " para el diámetro en AB, " +
                        format1.format(Thorzion.getDiameter(Thorzion.getRadius(taoEj2Caso2,torqueEj2Caso2))*1000) + " " + nomneclatureDistance + " para el diámetro en BC ");

                txtSol2_1.setText(" R/: " + format.format(Thorzion.getTaoMaximus(torque,radioExtEj2,radioIntEj2)) + " " + nomneclaturePresure + " para el punto AB, " +
                        format.format(Thorzion.getTaoMaximus(torqueEj2Caso2,radioExt2Ej2,radioInt2Ej2)) + " " + nomneclaturePresure + " para el punto BC " );

                txtSol2_2.setText(" R/: " + format2.format(unidadAngle) + " " + nomneclatureAngle + " para el punto AB, " + format2.format(unidadAngle2) + " " + nomneclatureAngle + " para el punto BC " );



                exercise.setLayoutID(null);
            }
        });
        atras = (Button) view.findViewById(R.id.btnAtr2);

        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent atras = new Intent(getActivity(), ExercisesActivity.class);
                exercise.setLayoutID(null);
                atras.putExtra("exercise", exercise);
                startActivity(atras);

            }
        });

        txtTao = (EditText) view.findViewById(R.id.txtTao2);
        txtTorque = (EditText) view.findViewById(R.id.txtTorque);

        txtTaoEj2Caso2 = (EditText) view.findViewById(R.id.txtTaoEj2Caso2);
        txtTorqueEj2Caso2 = (EditText) view.findViewById(R.id.txtTorqueEj2Caso2);

        opciones =(Spinner)view.findViewById(R.id.opcSistEj2);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.opciones, android.R.layout.simple_spinner_item);
        opciones.setAdapter(adapter);

        orientacionCaso1 =(Spinner)view.findViewById(R.id.opcOriCaso1Ej2);
        ArrayAdapter<CharSequence> adapterC1 = ArrayAdapter.createFromResource(getActivity(), R.array.SentidoTorque, android.R.layout.simple_spinner_item);
        orientacionCaso1.setAdapter(adapterC1);

        orientacionCaso2 =(Spinner)view.findViewById(R.id.opcOriCaso2Ej2);
        ArrayAdapter<CharSequence> adapterC2 = ArrayAdapter.createFromResource(getActivity(), R.array.SentidoTorque, android.R.layout.simple_spinner_item);
        orientacionCaso2.setAdapter(adapterC2);

        opciones2 =(Spinner)view.findViewById(R.id.opcMedEj2);

        return view;
    }

    @Override
    public void onStart(){
        super.onStart();

        this.opciones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                double tao = Double.parseDouble(txtTao.getText().toString());
                double torque = Double.parseDouble(txtTorque.getText().toString());

                txtRadioIntEj2.setText("0", TextView.BufferType.EDITABLE);
                txtRadioExtEj2.setText("0", TextView.BufferType.EDITABLE);

                double taoEj2Caso2 = Double.parseDouble(txtTaoEj2Caso2.getText().toString());
                double torqueEj2Caso2 = Double.parseDouble(txtTorqueEj2Caso2.getText().toString());

                txtRadioInt2Ej2.setText("0", TextView.BufferType.EDITABLE);
                txtRadioExt2Ej2.setText("0", TextView.BufferType.EDITABLE);

                txtLongEj2.setText("0", TextView.BufferType.EDITABLE);
                txtLong2Ej2.setText("0", TextView.BufferType.EDITABLE);

                double rigEj2 = Double.parseDouble(txtRigEj2.getText().toString());
                double rig2Ej2 = Double.parseDouble(txtRig2Ej2.getText().toString());

                DecimalFormat numberFormat = new DecimalFormat("#.####");

                if(position == 0){
                    tao = Thorzion.PRESSURE_UNITS.KSI.unitToInternational(tao);
                    torque = Thorzion.TORQUE_UNITS.POUND_INCH.unitToInternational(torque);

                    taoEj2Caso2 = Thorzion.PRESSURE_UNITS.KSI.unitToInternational(taoEj2Caso2);
                    torqueEj2Caso2 = Thorzion.TORQUE_UNITS.POUND_INCH.unitToInternational(torqueEj2Caso2);

                    lblTaoEj2.setText(Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature());
                    lblTorqueEj2.setText(Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature());
                    lblRigEj2.setText(Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature());

                    lblTao2Ej2.setText(Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature());
                    lblTorque2Ej2.setText(Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature());
                    lblRig2Ej2.setText(Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature());

                    opciones2 =(Spinner)getView().findViewById(R.id.opcMedEj2);
                    ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.metric, android.R.layout.simple_spinner_item);
                    opciones2.setAdapter(adapter2);
                    opciones2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            double radioExterno = Double.parseDouble(txtRadioExtEj2.getText().toString());
                            double radioInterno = Double.parseDouble(txtRadioIntEj2.getText().toString());

                            double radioExternoCaso2 = Double.parseDouble(txtRadioExt2Ej2.getText().toString());
                            double radioInternoCaso2 = Double.parseDouble(txtRadioInt2Ej2.getText().toString());

                            double longEj2 = Double.parseDouble(txtLongEj2.getText().toString());
                            double long2Ej2 = Double.parseDouble(txtLong2Ej2.getText().toString());

                            DecimalFormat numberFormat = new DecimalFormat("#.####");
                            switch(position){
                                case 0:
                                    radioExterno = Thorzion.milimeterToMeter(radioExterno);
                                    radioInterno = Thorzion.milimeterToMeter(radioInterno);
                                    txtRadioIntEj2.setText(numberFormat.format(radioInterno), TextView.BufferType.EDITABLE);
                                    txtRadioExtEj2.setText(numberFormat.format(radioExterno), TextView.BufferType.EDITABLE);

                                    radioExternoCaso2 = Thorzion.milimeterToMeter(radioExternoCaso2);
                                    radioInternoCaso2 = Thorzion.milimeterToMeter(radioInternoCaso2);
                                    txtRadioInt2Ej2.setText(numberFormat.format(radioInternoCaso2), TextView.BufferType.EDITABLE);
                                    txtRadioExt2Ej2.setText(numberFormat.format(radioExternoCaso2), TextView.BufferType.EDITABLE);

                                    longEj2 = Thorzion.milimeterToMeter(longEj2);
                                    long2Ej2 = Thorzion.milimeterToMeter(long2Ej2);
                                    txtLongEj2.setText(numberFormat.format(longEj2), TextView.BufferType.EDITABLE);
                                    txtLong2Ej2.setText(numberFormat.format(long2Ej2), TextView.BufferType.EDITABLE);

                                    lblRadio_LongsEj2.setText(Thorzion.getMeterNomenclature());
                                    lblRadio_Longs2Ej2.setText(Thorzion.getMeterNomenclature());
                                    lblRadio_Longs3Ej2.setText(Thorzion.getMeterNomenclature());

                                    lblRadio_Longs4Ej2.setText(Thorzion.getMeterNomenclature());
                                    lblRadio_Longs5Ej2.setText(Thorzion.getMeterNomenclature());
                                    lblRadio_Longs6Ej2.setText(Thorzion.getMeterNomenclature());

                                    break;
                                case 1:
                                    radioExterno = Thorzion.meterToMilimeter(radioExterno);
                                    radioInterno = Thorzion.meterToMilimeter(radioInterno);
                                    txtRadioIntEj2.setText(numberFormat.format(radioInterno), TextView.BufferType.EDITABLE);
                                    txtRadioExtEj2.setText(numberFormat.format(radioExterno), TextView.BufferType.EDITABLE);

                                    radioExternoCaso2 = Thorzion.meterToMilimeter(radioExternoCaso2);
                                    radioInternoCaso2 = Thorzion.meterToMilimeter(radioInternoCaso2);
                                    txtRadioInt2Ej2.setText(numberFormat.format(radioInternoCaso2), TextView.BufferType.EDITABLE);
                                    txtRadioExt2Ej2.setText(numberFormat.format(radioExternoCaso2), TextView.BufferType.EDITABLE);

                                    longEj2 = Thorzion.meterToMilimeter(longEj2);
                                    long2Ej2 = Thorzion.meterToMilimeter(long2Ej2);
                                    txtLongEj2.setText(numberFormat.format(longEj2), TextView.BufferType.EDITABLE);
                                    txtLong2Ej2.setText(numberFormat.format(long2Ej2), TextView.BufferType.EDITABLE);

                                    lblRadio_LongsEj2.setText(Thorzion.getMiliMeterNomenclature());
                                    lblRadio_Longs2Ej2.setText(Thorzion.getMiliMeterNomenclature());
                                    lblRadio_Longs3Ej2.setText(Thorzion.getMiliMeterNomenclature());

                                    lblRadio_Longs4Ej2.setText(Thorzion.getMiliMeterNomenclature());
                                    lblRadio_Longs5Ej2.setText(Thorzion.getMiliMeterNomenclature());
                                    lblRadio_Longs6Ej2.setText(Thorzion.getMiliMeterNomenclature());

                                    break;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                }
                else if(position == 1){
                    tao = Thorzion.PRESSURE_UNITS.PASCAL.unitToImperial(tao);
                    torque = Thorzion.TORQUE_UNITS.NEWTON_METER.unitToImperial(torque);

                    taoEj2Caso2 = Thorzion.PRESSURE_UNITS.PASCAL.unitToImperial(taoEj2Caso2);
                    torqueEj2Caso2 = Thorzion.TORQUE_UNITS.NEWTON_METER.unitToImperial(torqueEj2Caso2);


                    lblTaoEj2.setText(Thorzion.PRESSURE_UNITS.KSI.getNomenclature());
                    lblTorqueEj2.setText(Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature());
                    lblRigEj2.setText(Thorzion.PRESSURE_UNITS.KSI.getNomenclature());

                    lblTao2Ej2.setText(Thorzion.PRESSURE_UNITS.KSI.getNomenclature());
                    lblTorque2Ej2.setText(Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature());
                    lblRig2Ej2.setText(Thorzion.PRESSURE_UNITS.KSI.getNomenclature());

                    opciones2 =(Spinner)getView().findViewById(R.id.opcMedEj2);
                    ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.ingles, android.R.layout.simple_spinner_item);
                    opciones2.setAdapter(adapter2);
                    opciones2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            double radioExterno = Double.parseDouble(txtRadioExtEj2.getText().toString());
                            double radioInterno = Double.parseDouble(txtRadioIntEj2.getText().toString());

                            double radioExternoCaso2 = Double.parseDouble(txtRadioExt2Ej2.getText().toString());
                            double radioInternoCaso2 = Double.parseDouble(txtRadioInt2Ej2.getText().toString());

                            double longEj2 = Double.parseDouble(txtLongEj2.getText().toString());
                            double long2Ej2 = Double.parseDouble(txtLong2Ej2.getText().toString());

                            DecimalFormat numberFormat = new DecimalFormat("#.####");
                            switch(position){
                                case 0:
                                    radioExterno = Thorzion.inchToFoot(radioExterno);
                                    radioInterno = Thorzion.inchToFoot(radioInterno);
                                    txtRadioIntEj2.setText(numberFormat.format(radioInterno), TextView.BufferType.EDITABLE);
                                    txtRadioExtEj2.setText(numberFormat.format(radioExterno), TextView.BufferType.EDITABLE);

                                    radioExternoCaso2 = Thorzion.inchToFoot(radioExternoCaso2);
                                    radioInternoCaso2 = Thorzion.inchToFoot(radioInternoCaso2);
                                    txtRadioInt2Ej2.setText(numberFormat.format(radioInternoCaso2), TextView.BufferType.EDITABLE);
                                    txtRadioExt2Ej2.setText(numberFormat.format(radioExternoCaso2), TextView.BufferType.EDITABLE);

                                    longEj2 = Thorzion.inchToFoot(longEj2);
                                    long2Ej2 = Thorzion.inchToFoot(long2Ej2);
                                    txtLongEj2.setText(numberFormat.format(longEj2), TextView.BufferType.EDITABLE);
                                    txtLong2Ej2.setText(numberFormat.format(long2Ej2), TextView.BufferType.EDITABLE);

                                    lblRadio_LongsEj2.setText(Thorzion.getFootNomenclature());
                                    lblRadio_Longs2Ej2.setText(Thorzion.getFootNomenclature());
                                    lblRadio_Longs3Ej2.setText(Thorzion.getFootNomenclature());

                                    lblRadio_Longs4Ej2.setText(Thorzion.getFootNomenclature());
                                    lblRadio_Longs5Ej2.setText(Thorzion.getFootNomenclature());
                                    lblRadio_Longs6Ej2.setText(Thorzion.getFootNomenclature());


                                    break;
                                case 1:
                                    radioExterno = Thorzion.footToInch(radioExterno);
                                    radioInterno = Thorzion.footToInch(radioInterno);
                                    txtRadioIntEj2.setText(numberFormat.format(radioInterno), TextView.BufferType.EDITABLE);
                                    txtRadioExtEj2.setText(numberFormat.format(radioExterno), TextView.BufferType.EDITABLE);

                                    radioExternoCaso2 = Thorzion.footToInch(radioExternoCaso2);
                                    radioInternoCaso2 = Thorzion.footToInch(radioInternoCaso2);
                                    txtRadioInt2Ej2.setText(numberFormat.format(radioInternoCaso2), TextView.BufferType.EDITABLE);
                                    txtRadioExt2Ej2.setText(numberFormat.format(radioExternoCaso2), TextView.BufferType.EDITABLE);

                                    longEj2 = Thorzion.footToInch(longEj2);
                                    long2Ej2 = Thorzion.footToInch(long2Ej2);
                                    txtLongEj2.setText(numberFormat.format(longEj2), TextView.BufferType.EDITABLE);
                                    txtLong2Ej2.setText(numberFormat.format(long2Ej2), TextView.BufferType.EDITABLE);

                                    lblRadio_LongsEj2.setText(Thorzion.getInchNomenclature());
                                    lblRadio_Longs2Ej2.setText(Thorzion.getInchNomenclature());
                                    lblRadio_Longs3Ej2.setText(Thorzion.getInchNomenclature());

                                    lblRadio_Longs4Ej2.setText(Thorzion.getInchNomenclature());
                                    lblRadio_Longs5Ej2.setText(Thorzion.getInchNomenclature());
                                    lblRadio_Longs6Ej2.setText(Thorzion.getInchNomenclature());

                                    break;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }

                txtTao.setText(tao+"", TextView.BufferType.EDITABLE);
                txtTaoEj2Caso2.setText(taoEj2Caso2+"", TextView.BufferType.EDITABLE);
                txtTorque.setText(torque+"", TextView.BufferType.EDITABLE);
                txtTorqueEj2Caso2.setText(torqueEj2Caso2+"", TextView.BufferType.EDITABLE);
                txtRigEj2.setText(rigEj2+"", TextView.BufferType.EDITABLE);
                txtRig2Ej2.setText(rig2Ej2+"", TextView.BufferType.EDITABLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}