package com.app.appTorsion.soluciones;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.app.appTorsion.R;
import com.app.appTorsion.activities.ExercisesActivity;
import com.app.appTorsion.activities.ProceduresActivity;
import com.app.appTorsion.helpers.Thorzion;
import com.app.appTorsion.models.Exercise;
import com.app.appTorsion.models.ProcedureActivity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;


public class solucionEj3 extends Fragment implements Serializable {

    Button atras;
    Button resolver;
    Button solucion;

    EditText txtTorqueEj3Caso1, txtTorqueEj3Caso2,txtTorqueEj3Caso3, txtRadioEj3Caso1, txtRadioEj3Caso2, txtRadioEj3Caso3;
    EditText txtTaoEj3, txtTao2Ej3, txtTao3Ej3, txtRigEj3, txtLongEj3, txtLong2Ej3, txtLong3Ej3;

    TextView lblTorqueEj3,lblTorque2Ej3,lblTorque3Ej3, lblRadioEj3,lblRadio2Ej3,lblRadio3Ej3, txtSol3, txtSol3_2, txtSol3_3;
    TextView lblTaoEj3, lblTao2Ej3, lblTao3Ej3, lblRigEj3, lblLongEj3, lblLong2Ej3, lblLong3Ej3, txtSol3_4;

    Spinner opciones,orientacionCaso1,orientacionCaso2,orientacionCaso3;
    Spinner opciones2;

    CheckBox chkTorques;

    private Exercise exercise;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_solucion_ej3, container, false);

        exercise = (Exercise) getArguments().getSerializable("exercise");

        txtSol3 = (TextView) view.findViewById(R.id.txtSol3);
        txtSol3_2 = (TextView) view.findViewById(R.id.txtSol3_2);
        txtSol3_3 = (TextView) view.findViewById(R.id.txtSol3_3);
        txtTorqueEj3Caso1 = (EditText) view.findViewById(R.id.txtTorqueEj3Caso1);
        txtTorqueEj3Caso2 = (EditText) view.findViewById(R.id.txtTorqueEj3Caso2);
        txtTorqueEj3Caso3 = (EditText) view.findViewById(R.id.txtTorqueEj3Caso3);
        txtRadioEj3Caso1 = (EditText) view.findViewById(R.id.txtRadioEj3Caso1);
        txtRadioEj3Caso2 = (EditText) view.findViewById(R.id.txtRadioEj3Caso2);
        txtRadioEj3Caso3 = (EditText) view.findViewById(R.id.txtRadioEj3Caso3);

        txtSol3_4 = (TextView) view.findViewById(R.id.txtSol3_4);
        txtTaoEj3 = (EditText) view.findViewById(R.id.txtTaoEj3);
        txtTao2Ej3 = (EditText) view.findViewById(R.id.txtTao2Ej3);
        txtTao3Ej3 = (EditText) view.findViewById(R.id.txtTao3Ej3);
        txtRigEj3 = (EditText) view.findViewById(R.id.txtRigEj3);
        txtLongEj3 = (EditText) view.findViewById(R.id.txtLongEj3);
        txtLong2Ej3 = (EditText) view.findViewById(R.id.txtLong2Ej3);
        txtLong3Ej3 = (EditText) view.findViewById(R.id.txtLong3Ej3);

        lblTorqueEj3 = (TextView) view.findViewById(R.id.lblTorqueEj3);
        lblTorque2Ej3 = (TextView) view.findViewById(R.id.lblTorque2Ej3);
        lblTorque3Ej3 = (TextView) view.findViewById(R.id.lblTorque3Ej3);
        lblRadioEj3 = (TextView) view.findViewById(R.id.lblRadioEj3);
        lblRadio2Ej3 = (TextView) view.findViewById(R.id.lblRadio2Ej3);
        lblRadio3Ej3 = (TextView) view.findViewById(R.id.lblRadio3Ej3);

        lblTaoEj3 = (TextView) view.findViewById(R.id.lblTaoEj3);
        lblTao2Ej3 = (TextView) view.findViewById(R.id.lblTao2Ej3);
        lblTao3Ej3 = (TextView) view.findViewById(R.id.lblTao3Ej3);
        lblRigEj3 = (TextView) view.findViewById(R.id.lblRigEj3);
        lblLongEj3 = (TextView) view.findViewById(R.id.lblLongEj3);
        lblLong2Ej3 = (TextView) view.findViewById(R.id.lblLong2Ej3);
        lblLong3Ej3 = (TextView) view.findViewById(R.id.lblLong3Ej3);


        //chkTorques = (CheckBox) findViewById(R.id.chkSumTorques);

        resolver = (Button) view.findViewById(R.id.btnRes3);
        solucion = (Button) view.findViewById(R.id.btnSol3);

        resolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                exercise.setActivities(new ArrayList<>());

                double torque = Double.parseDouble(txtTorqueEj3Caso1.getText().toString());
                double radio = Double.parseDouble(txtRadioEj3Caso1.getText().toString());
                double torqueCaso2 = Double.parseDouble(txtTorqueEj3Caso2.getText().toString());
                double radioCaso2 = Double.parseDouble(txtRadioEj3Caso2.getText().toString());
                double torqueCaso3 = Double.parseDouble(txtTorqueEj3Caso3.getText().toString());
                double radioCaso3 = Double.parseDouble(txtRadioEj3Caso3.getText().toString());

                double taoEj3 = Double.parseDouble(txtTaoEj3.getText().toString());
                double tao2Ej3 = Double.parseDouble(txtTao2Ej3.getText().toString());
                double tao3Ej3 = Double.parseDouble(txtTao3Ej3.getText().toString());
                double rigEj3 = Double.parseDouble(txtRigEj3.getText().toString());
                double longEj3 = Double.parseDouble(txtLongEj3.getText().toString());
                double long2Ej3 = Double.parseDouble(txtLong2Ej3.getText().toString());
                double long3Ej3 = Double.parseDouble(txtLong3Ej3.getText().toString());

                torque = orientacionCaso1.getSelectedItem().toString().equals(orientacionCaso2.getSelectedItem().toString())?
                        torque+torqueCaso2:
                        torque-torqueCaso2;

                torque = orientacionCaso1.getSelectedItem().toString().equals(orientacionCaso3.getSelectedItem().toString())?
                        torque+torqueCaso3:
                        torque-torqueCaso3;

                torqueCaso2 = orientacionCaso2.getSelectedItem().toString().equals(orientacionCaso3.getSelectedItem().toString())?
                        torqueCaso2+torqueCaso3:
                        torqueCaso2-torqueCaso3;

                String nomneclaturePresure = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature():Thorzion.PRESSURE_UNITS.KSI.getNomenclature();

                String nomneclatureTorque = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature():Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature();

                String nomneclatureDistance = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getMiliMeterNomenclature():Thorzion.getInchNomenclature();

                String nomneclatureAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.ANGLE_UNITS.RAD.getNomenclature():Thorzion.ANGLE_UNITS.DEG.getNomenclature();

                double unidadAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torque,longEj3,radio,rigEj3):Thorzion.getThorzionAngleRigid(torque,longEj3,radio,rigEj3)*180/Math.PI;

                double unidadAngle2 = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torqueCaso2,long2Ej3,radioCaso2,rigEj3):Thorzion.getThorzionAngleRigid(torqueCaso2,long2Ej3,radioCaso2,rigEj3)*180/Math.PI;

                double unidadAngle3 = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torqueCaso3,long3Ej3,radioCaso3,rigEj3):Thorzion.getThorzionAngleRigid(torqueCaso3,long3Ej3,radioCaso3,rigEj3)*180/Math.PI;


                DecimalFormat format = new DecimalFormat("##.###E0");
                DecimalFormat format1 = new DecimalFormat("#.##");
                DecimalFormat format2 = new DecimalFormat("#.###");
                DecimalFormat format3 = new DecimalFormat("#.######");



                /*if(chkTorques.isChecked()){


                }*/


                ProcedureActivity paso1 = new ProcedureActivity(
                        "Modulo 1",
                        "En este módulo se procederá a encontrar los esfuerzos cortantes permisibles correspondientes " +
                                "al sistema de motor en el eje AB, en el eje BC y en el eje CD por medio de los torques en cada uno de los ejes y los radios",

                        R.drawable.mod1);

                ProcedureActivity paso2 = new ProcedureActivity(
                        "Modulo 1: Hallar τ en el eje AB",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b>τ=("+torque + " " +lblTorqueEj3.getText()+" * ("+radio + " " + lblRadioEj3.getText() +")/ (π/2)* "+ radio+ "" +
                                lblRadioEj3.getText() +"^4" + "   </b>, " + "obteniendo como resultado un τ = <b>"+ format.format(Thorzion.getTaoMaximus(torque,radio))+ " " + nomneclaturePresure + " En el eje AB" + "</b>",
                        R.drawable.ec1);

                ProcedureActivity paso3 = new ProcedureActivity(
                        "Modulo 1: Hallar τ en el eje BC",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b>τ=("+torqueCaso2 + " " +lblTorqueEj3.getText()+" * ("+radioCaso2 + " " + lblRadioEj3.getText() +")/ (π/2)* "+ radioCaso2+ "" +
                                lblRadioEj3.getText() +"^4"+ "   </b>, " + "obteniendo como resultado un τ = <b>"+ format.format(Thorzion.getTaoMaximus(torqueCaso2,radioCaso2))+ " " + nomneclaturePresure +" En el eje BC" + "</b>",
                        R.drawable.ec1);

                ProcedureActivity paso4 = new ProcedureActivity(
                        "Modulo 1: Hallar τ en el eje CD",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b>τ=("+torqueCaso3 + " " +lblTorqueEj3.getText()+" * ("+radioCaso3 + " " +  lblRadioEj3.getText() +")/ (π/2)* "+ radioCaso3+ "" +
                                lblRadioEj3.getText() +"^4"+ "   </b>, " + "obteniendo como resultado un τ = <b>"+ format.format(Thorzion.getTaoMaximus(torqueCaso3,radioCaso3))+ " " + nomneclaturePresure + " En el eje CD"+ "</b>",
                        R.drawable.ec1);

                ProcedureActivity paso5 = new ProcedureActivity(
                        "Modulo 1: Resultados",
                        " Los Esfuerzos cortantes permisibles obtenidos en cada uno de los ejes son: <b>" +( format.format(Thorzion.getTaoMaximus(torque,radio))+ " " + nomneclaturePresure)+
                                "</b> Para el eje AB, <b>" + format.format(Thorzion.getTaoMaximus(torqueCaso2,radioCaso2))+ " " + nomneclaturePresure + "</b> Para el eje BC, y <b>" +
                                format.format(Thorzion.getTaoMaximus(torqueCaso3,radioCaso3))+ " " + nomneclaturePresure + "</b> Para el eje en CD. ",
                        R.drawable.ec1);

                ProcedureActivity paso6 = new ProcedureActivity(
                        "Modulo 2",
                        "En este módulo se procederá a encontrar los torques, los diámetros y los ángulos de torsión en el eje AB, en el eje BC y en el eje CD ",
                        R.drawable.mod2);

                ProcedureActivity paso7 = new ProcedureActivity(
                        "Modulo 2: Hallar Torque en el eje AB",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> τ = " + taoEj3 + " " + lblTaoEj3.getText()  + ","+
                                "  J  = " + format3.format(Thorzion.getInertiaConstant(radio)) + lblRadioEj3.getText()+"^4, y "
                                + "C = " + radio + lblRadioEj3.getText() + "\n" +
                                " </b>, obteniendo como resultado un<b> Torque = " +
                                format2.format(Thorzion.getTorque(taoEj3,radio)) + " " + nomneclatureTorque+" En el eje AB"+"</b>",
                        R.drawable.ec3);

                ProcedureActivity paso8 = new ProcedureActivity(
                        " Modulo 2: Hallar Torque en el eje BC",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> τ = " + tao2Ej3 + " " + lblTaoEj3.getText() +","+
                                " J = " +  format3.format( Thorzion.getInertiaConstant(radioCaso2)) + lblRadioEj3.getText()+"^4, y "
                                + " C = " + radioCaso2 + lblRadioEj3.getText() +
                                " </b>, obteniendo como resultado un <b>Torque = " +
                                format2.format(Thorzion.getTorque(tao2Ej3,radioCaso2)) + " " + nomneclatureTorque+" En el eje BC"+"</b>",
                        R.drawable.ec3);

                ProcedureActivity paso9 = new ProcedureActivity(
                        " Modulo 2: Hallar Torque en el eje CD",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> τ = " + tao3Ej3 + " " + lblTaoEj3.getText() +","+
                                " J = " +  format3.format( Thorzion.getInertiaConstant(radioCaso3)) + lblRadioEj3.getText()+"^4, y "
                                + " C = " + radioCaso3 + lblRadioEj3.getText() +
                                " </b>, obteniendo como resultado un <b>Torque = " +
                                format2.format(Thorzion.getTorque(tao3Ej3,radioCaso3)) + " " + nomneclatureTorque+" En el eje CD"+"</b>",
                        R.drawable.ec3);

                ProcedureActivity paso10 = new ProcedureActivity(
                        "Modulo 2: Hallar Diámetro en el eje AB",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b>C=("+torque+" "+ lblTorqueEj3.getText() +") / ((π/2)*("+taoEj3+
                                " "+ lblTaoEj3.getText() +"))"+" </b>, obteniendo como resultado un Diametro = "+ String.format("%.2f", Thorzion.getDiameter(Thorzion.getRadius(taoEj3,torque))*1000) + " "
                                + nomneclatureDistance+" En el eje AB",
                        R.drawable.ec4);

                ProcedureActivity paso11 = new ProcedureActivity(
                        "Modulo 2: Hallar Diámetro en el eje BC",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b>C=("+torqueCaso2+" "+ lblTorqueEj3.getText() +") / ((π/2)*( "+tao2Ej3+
                                " "+ lblTaoEj3.getText() +"))"+" </b>, obteniendo como resultado un Diametro = " + String.format("%.2f", Thorzion.getDiameter(Thorzion.getRadius(tao2Ej3,torqueCaso2))*1000) + " "
                                + nomneclatureDistance+" En el eje BC",
                        R.drawable.ec4);

                ProcedureActivity paso12 = new ProcedureActivity(
                        "Modulo 2: Hallar Diámetro en el eje CD",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b>C=("+torqueCaso3+" "+ lblTorqueEj3.getText() +") / ((π/2)*( "+tao3Ej3+
                                " "+ lblTaoEj3.getText() +"))"+" </b>, obteniendo como resultado un Diametro = " + String.format("%.2f", Thorzion.getDiameter(Thorzion.getRadius(tao3Ej3,torqueCaso3))*1000) + " "
                                + nomneclatureDistance+" En el eje CD",
                        R.drawable.ec4);

                ProcedureActivity paso13 = new ProcedureActivity(
                        "Hallar ángulo de torsión en el eje AB",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T=(" +torque + " " +lblTorqueEj3.getText()+", L = " + longEj3 + "" + lblLongEj3.getText() +
                                ", J = " + format3.format(Thorzion.getInertiaConstant(radio)) + lblRadioEj3.getText()+ ", G = " + rigEj3 + ""+ lblRigEj3.getText()+""+ " </b>, " +
                                "obteniendo como resultado un ángulo = <b>"+ format2.format(unidadAngle) +" " + nomneclatureAngle +" En el eje AB"+ "</b>",
                        R.drawable.ec5);

                ProcedureActivity paso14 = new ProcedureActivity(
                        "Hallar ángulo de torsion en el eje BC",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T=("+torqueCaso2 + " " +lblTorqueEj3.getText()+", L = " + longEj3 + "" + lblLongEj3.getText() +
                                ", J = " + format3.format(Thorzion.getInertiaConstant(radioCaso2)) + lblRadioEj3.getText()+ ", G = " + rigEj3 + ""+ lblRigEj3.getText()+""+ " </b>, " +
                                "obteniendo como resultado un ángulo = <b>"+ format2.format(unidadAngle2)+ " "  + nomneclatureAngle +" En el eje BC"+ "</b>",
                        R.drawable.ec5);

                ProcedureActivity paso15 = new ProcedureActivity(
                        "Hallar ángulo de torsion en el eje CD",
                        "Procedemos a reemplazar los valores en la ecuación, para obtenerlo de " +
                                "la siguiente manera <b> T=("+torqueCaso3 + " " +lblTorqueEj3.getText()+", L = " + longEj3 + "" + lblLongEj3.getText() +
                                ", J = " + format3.format(Thorzion.getInertiaConstant(radioCaso3)) + lblRadioEj3.getText()+ ", G = " + rigEj3 + ""+ lblRigEj3.getText()+""+ " </b>, " +
                                "obteniendo como resultado un ángulo = <b>"+ format2.format(unidadAngle3) +" " + nomneclatureAngle +" En el eje CD"+ "</b>",
                        R.drawable.ec5);



                exercise.addActivity(paso1);
                exercise.addActivity(paso2);
                exercise.addActivity(paso3);
                exercise.addActivity(paso4);
                exercise.addActivity(paso5);
                exercise.addActivity(paso6);
                exercise.addActivity(paso7);
                exercise.addActivity(paso8);
                exercise.addActivity(paso9);
                exercise.addActivity(paso10);
                exercise.addActivity(paso11);
                exercise.addActivity(paso12);
                exercise.addActivity(paso13);
                exercise.addActivity(paso14);
                exercise.addActivity(paso15);

                Intent iniciar = new Intent(getActivity(), ProceduresActivity.class);
                exercise.setLayoutID(null);
                iniciar.putExtra("exercise", exercise);
                startActivity(iniciar);
            }
        });

        solucion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                exercise.setActivities(new ArrayList<>());

                double torque = Double.parseDouble(txtTorqueEj3Caso1.getText().toString());
                double radio = Double.parseDouble(txtRadioEj3Caso1.getText().toString());
                double torqueCaso2 = Double.parseDouble(txtTorqueEj3Caso2.getText().toString());
                double radioCaso2 = Double.parseDouble(txtRadioEj3Caso2.getText().toString());
                double torqueCaso3 = Double.parseDouble(txtTorqueEj3Caso3.getText().toString());
                double radioCaso3 = Double.parseDouble(txtRadioEj3Caso3.getText().toString());

                double taoEj3 = Double.parseDouble(txtTaoEj3.getText().toString());
                double tao2Ej3 = Double.parseDouble(txtTao2Ej3.getText().toString());
                double tao3Ej3 = Double.parseDouble(txtTao3Ej3.getText().toString());
                double rigEj3 = Double.parseDouble(txtRigEj3.getText().toString());
                double longEj3 = Double.parseDouble(txtLongEj3.getText().toString());
                double long2Ej3 = Double.parseDouble(txtLong2Ej3.getText().toString());
                double long3Ej3 = Double.parseDouble(txtLong3Ej3.getText().toString());

                torque = orientacionCaso1.getSelectedItem().toString().equals(orientacionCaso2.getSelectedItem().toString())?
                        torque+torqueCaso2:
                        torque-torqueCaso2;

                torque = orientacionCaso1.getSelectedItem().toString().equals(orientacionCaso3.getSelectedItem().toString())?
                        torque+torqueCaso3:
                        torque-torqueCaso3;

                torqueCaso2 = orientacionCaso2.getSelectedItem().toString().equals(orientacionCaso3.getSelectedItem().toString())?
                        torqueCaso2+torqueCaso3:
                        torqueCaso2-torqueCaso3;


                String nomneclaturePresure = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature():Thorzion.PRESSURE_UNITS.KSI.getNomenclature();

                String nomneclatureTorque = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature():Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature();

                String nomneclatureDistance = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getMiliMeterNomenclature():Thorzion.getInchNomenclature();

                String nomneclatureAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.ANGLE_UNITS.RAD.getNomenclature():Thorzion.ANGLE_UNITS.DEG.getNomenclature();

                double unidadAngle = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torque,longEj3,radio,rigEj3):Thorzion.getThorzionAngleRigid(torque,longEj3,radio,rigEj3)*180/Math.PI;

                double unidadAngle2 = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torqueCaso2,long2Ej3,radioCaso2,rigEj3):Thorzion.getThorzionAngleRigid(torqueCaso2,long2Ej3,radioCaso2,rigEj3)*180/Math.PI;

                double unidadAngle3 = opciones.getSelectedItem().toString().equals("S.Internacional")?
                        Thorzion.getThorzionAngleRigid(torqueCaso3,long3Ej3,radioCaso3,rigEj3):Thorzion.getThorzionAngleRigid(torqueCaso3,long3Ej3,radioCaso3,rigEj3)*180/Math.PI;


                DecimalFormat format = new DecimalFormat("##.###E0");
                DecimalFormat format2 = new DecimalFormat("#.###");


                txtSol3.setText(" R/: " + format.format( Thorzion.getTaoMaximus(torque,radio)) +" "+ nomneclaturePresure+ " Para el τ en el eje AB, " +
                        format.format(Thorzion.getTaoMaximus(torqueCaso2,radioCaso2)) +" "+ nomneclaturePresure + " Para τ en el eje BC y " +
                        format.format(Thorzion.getTaoMaximus(torqueCaso3,radioCaso3)) +" "+ nomneclaturePresure + " Para τ en el eje en CD. ");

                txtSol3_2.setText(" R/: " + format2.format(Thorzion.getTorque(taoEj3,radio)) +" "+ nomneclatureTorque + " Para Torque en el eje AB, " +
                        format2.format(Thorzion.getTorque(tao2Ej3,radioCaso2)) +" "+ nomneclatureTorque + " Para Torque en el eje BC y "  +
                        format2.format(Thorzion.getTorque(tao3Ej3,radioCaso3)) +" "+ nomneclatureTorque + " Para Torque en el eje CD " );

                txtSol3_3.setText(" R/: " +  format2.format(Thorzion.getDiameter(Thorzion.getRadius(taoEj3,torque))*1000) + " " + nomneclatureDistance + " Para el diámetro en AB, " +
                        format2.format(Thorzion.getDiameter(Thorzion.getRadius(tao2Ej3,torqueCaso2))*1000) + " " + nomneclatureDistance + " Para el diámetro en BC y " +
                        format2.format(Thorzion.getDiameter(Thorzion.getRadius(tao3Ej3,torqueCaso3))*1000) + " " + nomneclatureDistance + " Para el diámetro en CD " );

                txtSol3_4.setText(" R/: " + format2.format(unidadAngle) + " " + nomneclatureAngle + " Para el angulo en el eje AB, " +
                        format2.format(unidadAngle2) + " " + nomneclatureAngle + " Para el angulo en el punto BC y " +
                        format2.format(unidadAngle3) + " " + nomneclatureAngle + " Para el angulo en el punto CD " );



                exercise.setLayoutID(null);
            }
        });

        atras = (Button) view.findViewById(R.id.btnAtr3);

        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent atras = new Intent(getActivity(), ExercisesActivity.class);
                exercise.setLayoutID(null);
                atras.putExtra("exercise", exercise);
                startActivity(atras);

            }
        });


        opciones =(Spinner)view.findViewById(R.id.opcSistEj3);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.opciones, android.R.layout.simple_spinner_item);
        opciones.setAdapter(adapter);

        opciones2 =(Spinner)view.findViewById(R.id.opcMedEj3);

        orientacionCaso1 =(Spinner)view.findViewById(R.id.opcOriCaso1Ej3);
        ArrayAdapter<CharSequence> adapterC1 = ArrayAdapter.createFromResource(getActivity(), R.array.SentidoTorque, android.R.layout.simple_spinner_item);
        orientacionCaso1.setAdapter(adapterC1);

        orientacionCaso2 =(Spinner)view.findViewById(R.id.opcOriCaso2Ej3);
        ArrayAdapter<CharSequence> adapterC2 = ArrayAdapter.createFromResource(getActivity(), R.array.SentidoTorque, android.R.layout.simple_spinner_item);
        orientacionCaso2.setAdapter(adapterC2);

        orientacionCaso3 =(Spinner)view.findViewById(R.id.opcOriCaso3Ej3);
        ArrayAdapter<CharSequence> adapterC3 = ArrayAdapter.createFromResource(getActivity(), R.array.SentidoTorque, android.R.layout.simple_spinner_item);
        orientacionCaso3.setAdapter(adapterC3);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        this.opciones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here

                double torque = Double.parseDouble(txtTorqueEj3Caso1.getText().toString());
                txtRadioEj3Caso1.setText("0", TextView.BufferType.EDITABLE);
                double torqueCaso2 = Double.parseDouble(txtTorqueEj3Caso2.getText().toString());
                txtRadioEj3Caso2.setText("0", TextView.BufferType.EDITABLE);
                double torqueCaso3 = Double.parseDouble(txtTorqueEj3Caso2.getText().toString());
                txtRadioEj3Caso3.setText("0", TextView.BufferType.EDITABLE);

                double taoEj3 = Double.parseDouble(txtTaoEj3.getText().toString());
                txtLongEj3.setText("0", TextView.BufferType.EDITABLE);
                double tao2Ej3 = Double.parseDouble(txtTao2Ej3.getText().toString());
                txtLong2Ej3.setText("0", TextView.BufferType.EDITABLE);
                double tao3Ej3 = Double.parseDouble(txtTao3Ej3.getText().toString());
                txtLong3Ej3.setText("0", TextView.BufferType.EDITABLE);

                double rigEj3 = Double.parseDouble(txtRigEj3.getText().toString());


                if (position == 0) {
                    torque = Thorzion.TORQUE_UNITS.POUND_INCH.unitToInternational(torque);
                    torqueCaso2 = Thorzion.TORQUE_UNITS.POUND_INCH.unitToInternational(torqueCaso2);
                    torqueCaso3 = Thorzion.TORQUE_UNITS.POUND_INCH.unitToInternational(torqueCaso3);
                    taoEj3 = Thorzion.PRESSURE_UNITS.KSI.unitToInternational(taoEj3);
                    tao2Ej3 = Thorzion.PRESSURE_UNITS.KSI.unitToInternational(tao2Ej3);
                    tao3Ej3 = Thorzion.PRESSURE_UNITS.KSI.unitToInternational(tao3Ej3);
                    rigEj3 = Thorzion.PRESSURE_UNITS.KSI.unitToInternational(tao3Ej3);



                    lblTorqueEj3.setText(Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature());
                    lblTorque2Ej3.setText(Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature());
                    lblTorque3Ej3.setText(Thorzion.TORQUE_UNITS.NEWTON_METER.getNomenclature());
                    lblTaoEj3.setText(Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature());
                    lblTao2Ej3.setText(Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature());
                    lblTao3Ej3.setText(Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature());
                    lblRigEj3.setText(Thorzion.PRESSURE_UNITS.PASCAL.getNomenclature());


                    opciones2 = (Spinner) getView().findViewById(R.id.opcMedEj3);
                    ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.metric, android.R.layout.simple_spinner_item);
                    opciones2.setAdapter(adapter2);
                    opciones2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            double radio1 = Double.parseDouble(txtRadioEj3Caso1.getText().toString());
                            double radio2 = Double.parseDouble(txtRadioEj3Caso2.getText().toString());
                            double radio3 = Double.parseDouble(txtRadioEj3Caso3.getText().toString());
                            double longEj3 = Double.parseDouble(txtLongEj3.getText().toString());
                            double long2Ej3 = Double.parseDouble(txtLong2Ej3.getText().toString());
                            double long3Ej3 = Double.parseDouble(txtLong3Ej3.getText().toString());

                            DecimalFormat numberFormat = new DecimalFormat("#.####");
                            switch (position) {
                                case 0:
                                    radio1 = Thorzion.milimeterToMeter(radio1);
                                    radio2 = Thorzion.milimeterToMeter(radio2);
                                    radio3 = Thorzion.milimeterToMeter(radio3);
                                    txtRadioEj3Caso1.setText(numberFormat.format(radio1), TextView.BufferType.EDITABLE);
                                    txtRadioEj3Caso2.setText(numberFormat.format(radio2), TextView.BufferType.EDITABLE);
                                    txtRadioEj3Caso3.setText(numberFormat.format(radio3), TextView.BufferType.EDITABLE);
                                    lblRadioEj3.setText(Thorzion.getMeterNomenclature());
                                    lblRadio2Ej3.setText(Thorzion.getMeterNomenclature());
                                    lblRadio3Ej3.setText(Thorzion.getMeterNomenclature());

                                    longEj3 = Thorzion.milimeterToMeter(longEj3);
                                    long2Ej3 = Thorzion.milimeterToMeter(long2Ej3);
                                    long3Ej3 = Thorzion.milimeterToMeter(long3Ej3);
                                    txtLongEj3.setText(numberFormat.format(longEj3), TextView.BufferType.EDITABLE);
                                    txtLong2Ej3.setText(numberFormat.format(long2Ej3), TextView.BufferType.EDITABLE);
                                    txtLong3Ej3.setText(numberFormat.format(long3Ej3), TextView.BufferType.EDITABLE);
                                    lblLongEj3.setText(Thorzion.getMeterNomenclature());
                                    lblLong2Ej3.setText(Thorzion.getMeterNomenclature());
                                    lblLong3Ej3.setText(Thorzion.getMeterNomenclature());




                                    break;
                                case 1:
                                    radio1 = Thorzion.meterToMilimeter(radio1);
                                    radio2 = Thorzion.meterToMilimeter(radio2);
                                    radio3 = Thorzion.meterToMilimeter(radio3);
                                    txtRadioEj3Caso1.setText(numberFormat.format(radio1), TextView.BufferType.EDITABLE);
                                    txtRadioEj3Caso2.setText(numberFormat.format(radio2), TextView.BufferType.EDITABLE);
                                    txtRadioEj3Caso3.setText(numberFormat.format(radio3), TextView.BufferType.EDITABLE);
                                    lblRadioEj3.setText(Thorzion.getMiliMeterNomenclature());
                                    lblRadio2Ej3.setText(Thorzion.getMiliMeterNomenclature());
                                    lblRadio3Ej3.setText(Thorzion.getMiliMeterNomenclature());

                                    longEj3 = Thorzion.meterToMilimeter(longEj3);
                                    long2Ej3 = Thorzion.meterToMilimeter(long2Ej3);
                                    long3Ej3 = Thorzion.meterToMilimeter(long3Ej3);
                                    txtLongEj3.setText(numberFormat.format(longEj3), TextView.BufferType.EDITABLE);
                                    txtLong2Ej3.setText(numberFormat.format(long2Ej3), TextView.BufferType.EDITABLE);
                                    txtLong3Ej3.setText(numberFormat.format(long3Ej3), TextView.BufferType.EDITABLE);
                                    lblLongEj3.setText(Thorzion.getMiliMeterNomenclature());
                                    lblLong2Ej3.setText(Thorzion.getMiliMeterNomenclature());
                                    lblLong3Ej3.setText(Thorzion.getMiliMeterNomenclature());


                                    break;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else if (position == 1) {
                    torque = Thorzion.TORQUE_UNITS.NEWTON_METER.unitToImperial(torque);
                    torqueCaso2 = Thorzion.TORQUE_UNITS.NEWTON_METER.unitToImperial(torqueCaso2);
                    torqueCaso3 = Thorzion.TORQUE_UNITS.NEWTON_METER.unitToImperial(torqueCaso3);
                    taoEj3 = Thorzion.PRESSURE_UNITS.PASCAL.unitToImperial(taoEj3);
                    tao2Ej3 = Thorzion.PRESSURE_UNITS.PASCAL.unitToImperial(tao2Ej3);
                    tao3Ej3 = Thorzion.PRESSURE_UNITS.PASCAL.unitToImperial(tao3Ej3);

                    TextView lblTorqueEj3 = (TextView) getView().findViewById(R.id.lblTorqueEj3);
                    lblTorqueEj3.setText(Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature());
                    TextView lblTorque2Ej3 = (TextView) getView().findViewById(R.id.lblTorque2Ej3);
                    lblTorque2Ej3.setText(Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature());
                    TextView lblTorque3Ej3 = (TextView) getView().findViewById(R.id.lblTorque3Ej3);
                    lblTorque3Ej3.setText(Thorzion.TORQUE_UNITS.POUND_INCH.getNomenclature());

                    TextView lblTaoEj3 = (TextView) getView().findViewById(R.id.lblTaoEj3);
                    lblTaoEj3.setText(Thorzion.PRESSURE_UNITS.KSI.getNomenclature());
                    TextView lblTao2Ej3 = (TextView) getView().findViewById(R.id.lblTao2Ej3);
                    lblTao2Ej3.setText(Thorzion.PRESSURE_UNITS.KSI.getNomenclature());
                    TextView lblTao3Ej3 = (TextView) getView().findViewById(R.id.lblTao3Ej3);
                    lblTao3Ej3.setText(Thorzion.PRESSURE_UNITS.KSI.getNomenclature());

                    TextView lblRigEj3 = (TextView) getView().findViewById(R.id.lblRigEj3);
                    lblRigEj3.setText(Thorzion.PRESSURE_UNITS.KSI.getNomenclature());

                    opciones2 = (Spinner) getView().findViewById(R.id.opcMedEj3);
                    ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.ingles, android.R.layout.simple_spinner_item);
                    opciones2.setAdapter(adapter2);

                    opciones2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            double radio1 = Double.parseDouble(txtRadioEj3Caso1.getText().toString());
                            double radio2 = Double.parseDouble(txtRadioEj3Caso2.getText().toString());
                            double radio3 = Double.parseDouble(txtRadioEj3Caso3.getText().toString());
                            double longEj3 = Double.parseDouble(txtLongEj3.getText().toString());
                            double long2Ej3 = Double.parseDouble(txtLong2Ej3.getText().toString());
                            double long3Ej3 = Double.parseDouble(txtLong3Ej3.getText().toString());

                            DecimalFormat numberFormat = new DecimalFormat("#.####");
                            switch (position) {
                                case 0:
                                    radio1 = Thorzion.inchToFoot(radio1);
                                    radio2 = Thorzion.inchToFoot(radio2);
                                    radio3 = Thorzion.inchToFoot(radio3);
                                    txtRadioEj3Caso1.setText(numberFormat.format(radio1), TextView.BufferType.EDITABLE);
                                    txtRadioEj3Caso2.setText(numberFormat.format(radio2), TextView.BufferType.EDITABLE);
                                    txtRadioEj3Caso3.setText(numberFormat.format(radio3), TextView.BufferType.EDITABLE);
                                    lblRadioEj3.setText(Thorzion.getFootNomenclature());
                                    lblRadio2Ej3.setText(Thorzion.getFootNomenclature());
                                    lblRadio3Ej3.setText(Thorzion.getFootNomenclature());

                                    longEj3 = Thorzion.inchToFoot(longEj3);
                                    long2Ej3 = Thorzion.inchToFoot(long2Ej3);
                                    long3Ej3 = Thorzion.inchToFoot(long3Ej3);
                                    txtLongEj3.setText(numberFormat.format(longEj3), TextView.BufferType.EDITABLE);
                                    txtLong2Ej3.setText(numberFormat.format(long2Ej3), TextView.BufferType.EDITABLE);
                                    txtLong3Ej3.setText(numberFormat.format(long3Ej3), TextView.BufferType.EDITABLE);
                                    lblLongEj3.setText(Thorzion.getFootNomenclature());
                                    lblLong2Ej3.setText(Thorzion.getFootNomenclature());
                                    lblLong3Ej3.setText(Thorzion.getFootNomenclature());


                                    break;
                                case 1:
                                    radio1 = Thorzion.footToInch(radio1);
                                    radio2 = Thorzion.footToInch(radio2);
                                    radio3 = Thorzion.footToInch(radio3);
                                    txtRadioEj3Caso1.setText(numberFormat.format(radio1), TextView.BufferType.EDITABLE);
                                    txtRadioEj3Caso2.setText(numberFormat.format(radio2), TextView.BufferType.EDITABLE);
                                    txtRadioEj3Caso3.setText(numberFormat.format(radio3), TextView.BufferType.EDITABLE);
                                    lblRadioEj3.setText(Thorzion.getInchNomenclature());
                                    lblRadio2Ej3.setText(Thorzion.getInchNomenclature());
                                    lblRadio3Ej3.setText(Thorzion.getInchNomenclature());

                                    longEj3 = Thorzion.footToInch(longEj3);
                                    long2Ej3 = Thorzion.footToInch(long2Ej3);
                                    long3Ej3 = Thorzion.footToInch(long3Ej3);
                                    txtLongEj3.setText(numberFormat.format(longEj3), TextView.BufferType.EDITABLE);
                                    txtLong2Ej3.setText(numberFormat.format(long2Ej3), TextView.BufferType.EDITABLE);
                                    txtLong3Ej3.setText(numberFormat.format(long3Ej3), TextView.BufferType.EDITABLE);
                                    lblLongEj3.setText(Thorzion.getInchNomenclature());
                                    lblLong2Ej3.setText(Thorzion.getInchNomenclature());
                                    lblLong3Ej3.setText(Thorzion.getInchNomenclature());



                                    break;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }

                txtTorqueEj3Caso1.setText(torque + "", TextView.BufferType.EDITABLE);
                txtTorqueEj3Caso2.setText(torqueCaso2 + "", TextView.BufferType.EDITABLE);
                txtTorqueEj3Caso3.setText(torqueCaso3 + "", TextView.BufferType.EDITABLE);
                txtTaoEj3.setText(taoEj3 + "", TextView.BufferType.EDITABLE);
                txtTao2Ej3.setText(tao2Ej3 + "", TextView.BufferType.EDITABLE);
                txtTao3Ej3.setText(tao3Ej3 + "", TextView.BufferType.EDITABLE);
                txtRigEj3.setText(rigEj3+"", TextView.BufferType.EDITABLE);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }
}