package com.app.appTorsion.models;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class AbstractProcedureActivity implements Serializable {

    private String title;

    private String description;

    private int image;

    public AbstractProcedureActivity(String title, String description, int image){
        this.title = title;
        this.description = description;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public abstract double getResult();

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
