package com.app.appTorsion.models;

import com.app.appTorsion.helpers.Thorzion;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ProcedureActivity extends AbstractProcedureActivity implements Serializable {

    private double torque;

    private double radius;

    public ProcedureActivity(String title, String description, int image) {
        super(title, description, image);
        this.torque = 0;
        this.radius = 0;
    }

    public double getTorque() {
        return torque;
    }

    public void setTorque(double torque) {
        this.torque = torque;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getResult() {
        return Thorzion.getTaoMaximus(this.torque,this.radius);
    }
}
