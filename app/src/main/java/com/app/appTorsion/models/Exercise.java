package com.app.appTorsion.models;

import androidx.fragment.app.Fragment;

import com.app.appTorsion.functions.ThorzionFunction;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("serial")
public class Exercise implements Serializable {

    private int image;

    private String title;

    private String description;

    private String description2;

    private List<AbstractProcedureActivity> activities;

    private ThorzionFunction<Double, Double, Double, Double> solution;

    private Fragment layoutFragment;

    public Exercise(int image, String title, String description, String description2, Fragment layoutFragment) {
        this.image = image;
        this.title = title;
        this.description = description;
        this.description2 = description2;

        this.activities = new LinkedList<>();
        this.layoutFragment = layoutFragment;
    }

    public List<AbstractProcedureActivity> getActivities() {
        return activities;
    }

    public void addActivity(AbstractProcedureActivity activity){
        activities.add(activity);
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public void setActivities(List<AbstractProcedureActivity> activities) {
        this.activities = activities;
    }

    public ThorzionFunction<Double, Double, Double, Double> getSolution() {
        return solution;
    }

    public void setSolution(ThorzionFunction<Double, Double, Double, Double> solution) {
        this.solution = solution;
    }

    public Fragment getLayoutFragment() {
        return layoutFragment;
    }

    public void setLayoutID(Fragment layoutClass) {
        this.layoutFragment = layoutClass;
    }
}
