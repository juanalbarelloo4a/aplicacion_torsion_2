package com.app.appTorsion.helpers;

public class Thorzion {

    public enum UNIT_SYSTEM {
        INTERNATIONAL_UNIT_SYSTEM("Sistema Internacional de unidades"),
        IMPERIAL_UNIT_SYSTEM("Sistema anglosajon de unidades");

        private String spanish;

        private UNIT_SYSTEM(String spanish){
            this.spanish = spanish;
        }

        public String getSpanish(){
            return this.spanish;
        }
    }

    public enum TORQUE_UNITS{
        NEWTON_METER(1,8.8507,"N*m"),
        POUND_INCH(0.112985,1,"Lb*in");


        private double internationalConversionUnit;
        private double imperialConversionUnit;
        private String nomenclature;

        private TORQUE_UNITS(double internationalConversionUnit, double imperialConversionUnit, String nomenclature){
            this.internationalConversionUnit = internationalConversionUnit;
            this.imperialConversionUnit = imperialConversionUnit;
            this.nomenclature = nomenclature;
        }

        public double getInternationalConversionUnit(){
            return this.internationalConversionUnit;
        }

        public double getImperialConversionUnit(){
            return this.imperialConversionUnit;
        }

        public double unitToImperial(double units){
            return units * this.getImperialConversionUnit();
        }

        public double unitToInternational(double units){
            return units * this.getInternationalConversionUnit();
        }

        public String getNomenclature() {
            return nomenclature;
        }
    }

    public enum SPEED_UNITS{
        RPM(1,0.1047,"RPM"),
        RAD_S(9.5492,1,"RAD/S");

        private double internationalConversionUnit;
        private double imperialConversionUnit;
        private String nomenclature;

        private SPEED_UNITS(double internationalConversionUnit, double imperialConversionUnit, String nomenclature){
            this.internationalConversionUnit = internationalConversionUnit;
            this.imperialConversionUnit = imperialConversionUnit;
            this.nomenclature = nomenclature;
        }

        public double getInternationalConversionUnit(){
            return this.internationalConversionUnit;
        }

        public double getImperialConversionUnit(){
            return this.imperialConversionUnit;
        }

        public double unitToImperial(double units){
            return units * this.getImperialConversionUnit();
        }

        public double unitToInternational(double units){
            return units * this.getInternationalConversionUnit();
        }

        public String getNomenclature() {
            return nomenclature;
        }

    }

    public enum ANGLE_UNITS{
        RAD(1,57.2958,"RAD"),
        DEG(0.0174533,1,"DEG");

        private double internationalConversionUnit;
        private double imperialConversionUnit;
        private String nomenclature;

        private ANGLE_UNITS(double internationalConversionUnit, double imperialConversionUnit, String nomenclature){
            this.internationalConversionUnit = internationalConversionUnit;
            this.imperialConversionUnit = imperialConversionUnit;
            this.nomenclature = nomenclature;
        }

        public double getInternationalConversionUnit(){
            return this.internationalConversionUnit;
        }

        public double getImperialConversionUnit(){
            return this.imperialConversionUnit;
        }

        public double unitToImperial(double units){
            return units * this.getImperialConversionUnit();
        }

        public double unitToInternational(double units){
            return units * this.getInternationalConversionUnit();
        }

        public String getNomenclature() {
            return nomenclature;
        }

    }



    public enum POWER_UNITS{
        KW(1,1.3410,"KW"),
        HP(0.7457,1,"HP");

        private double internationalConversionUnit;
        private double imperialConversionUnit;
        private String nomenclature;

        private POWER_UNITS(double internationalConversionUnit, double imperialConversionUnit, String nomenclature){
            this.internationalConversionUnit = internationalConversionUnit;
            this.imperialConversionUnit = imperialConversionUnit;
            this.nomenclature = nomenclature;
        }

        public double getInternationalConversionUnit(){
            return this.internationalConversionUnit;
        }

        public double getImperialConversionUnit(){
            return this.imperialConversionUnit;
        }

        public double unitToImperial(double units){
            return units * this.getImperialConversionUnit();
        }

        public double unitToInternational(double units){
            return units * this.getInternationalConversionUnit();
        }

        public String getNomenclature() {
            return nomenclature;
        }
    }

    public enum FORCE_UNITS{
        N(1,0.2248,"N"),
        LB(4.4482,1,"Lb");


        private double internationalConversionUnit;
        private double imperialConversionUnit;
        private String nomenclature;

        private FORCE_UNITS(double internationalConversionUnit, double imperialConversionUnit, String nomenclature){
            this.internationalConversionUnit = internationalConversionUnit;
            this.imperialConversionUnit = imperialConversionUnit;
            this.nomenclature = nomenclature;
        }

        public double getInternationalConversionUnit(){
            return this.internationalConversionUnit;
        }

        public double getImperialConversionUnit(){
            return this.imperialConversionUnit;
        }

        public double unitToImperial(double units){
            return units * this.getImperialConversionUnit();
        }

        public double unitToInternational(double units){
            return units * this.getInternationalConversionUnit();
        }

        public String getNomenclature() {
            return nomenclature;
        }
    }

    public enum PRESSURE_UNITS{
        PASCAL(1,0.000145038, "N/m^2"),
        KSI(6894.76,1,"Lb/in^2");

        private double internationalConversionUnit;
        private double imperialConversionUnit;
        private String nomenclature;

        private PRESSURE_UNITS(double internationalConversionUnit, double imperialConversionUnit, String nomenclature){
            this.internationalConversionUnit = internationalConversionUnit;
            this.imperialConversionUnit = imperialConversionUnit;
            this.nomenclature = nomenclature;
        }


        public double getInternationalConversionUnit(){
            return this.internationalConversionUnit;
        }

        public double getImperialConversionUnit(){
            return this.imperialConversionUnit;
        }

        public double unitToImperial(double units){
            return units * this.getImperialConversionUnit();
        }

        public double unitToInternational(double units){
            return units * this.getInternationalConversionUnit();
        }

        public String getNomenclature() {
            return nomenclature;
        }
    }

    public static String getMeterNomenclature(){
        return "m";
    }

    public static String getMiliMeterNomenclature(){
        return "mm";
    }

    public static String getInchNomenclature(){
        return "in";
    }

    public static String getFootNomenclature(){
        return "ft";
    }

    public static String getRadNomenclature(){
        return "rad";
    }
    public static String getDegNomenclature(){
        return "°";
    }

    public static double milimeterToMeter(double milimeter)
    {
        return milimeter / 1000;
    }

    public static double meterToMilimeter(double meter) { return meter * 1000; }

    public static double footToInch(double foot)
    {
        return foot * 12;
    }
    public static double inchToFoot(double inch)
    {
        return inch / 12;
    }

    public static double radToDeg(double rad) {return rad * 180/Math.PI;}
    public static double degToRad(double deg) {return deg * Math.PI/180;}

    public static double getRadius(double diameter){
        //r=d/2
        return diameter/2;
    }

    public static double getDiameter(double radius){
        //d=r*2
        return radius*2;
    }

    public static double getInertiaConstant(double externalRadius, double internalRadius){
        //J = (PI/2)*(exR^4 - intR^4)
        return ((Math.PI/2)*(Math.pow(externalRadius,4) - Math.pow(internalRadius,4)));
    }
    public static double getInertiaConstant(double radius){
        //J = (PI/2)*(exR^4 - intR^4)
        return ((Math.PI/2)*(Math.pow(radius,4)));
    }

    public static double getTaoMaximus(double torque, double radius){
        // J = (PI/2)*r^4
        // Tao = (T*r)/J
        return  (torque * radius)/((Math.PI/2)*Math.pow(radius,4));
    }

    public static double getTaoMaximus(double torque, double externalRadius, double internalRadius){
        // J = (PI/2)*(exR^4 - intR^4)
        // Tao = (T*r)/J
        return (torque * externalRadius) / ((Math.PI/2)*(Math.pow(externalRadius,4) - Math.pow(internalRadius,4)));
    }

    public static double getTorque(double tao, double radius){
        // J = (PI/2)*r^4
        // T = (J*Tao)/ r
        return ((Math.PI/2)*Math.pow(radius,4) * tao)/radius;
    }

    public static double getTorque(double tao, double externalRadius, double internalRadius){
        // J = (PI/2)*(exR^4 - intR^4)
        // T = (J*Tao)/ r
        return ((Math.PI/2)*(Math.pow(externalRadius,4) - Math.pow(internalRadius,4)) * tao) / externalRadius;
    }

    public static double getRadius(double tao, double torque){
        // Tao = (T*r)/J
        // J = (PI/2)*r^4
        // r = raiz3(T/((PI/2)*Tao))

        return Math.cbrt((torque)/((Math.PI/2)*tao));
    }

    public static double getSumAngles(double angle1, double angle2) {
        // Φ = Σ(Angulo1 + Angulo2
        return angle1 + angle2;
    }

    public static double getSumAnglesRest(double angle1, double angle2) {
        // Φ = Σ(Angulo1 - Angulo2
        return angle1 - angle2;
    }

    public static double getDespPoint(double radiusD, double angleSum) {
        // S = p*Φ
        return radiusD*angleSum;
    }


    public static double getPower(double torque, double angularSpeed){
        //pow = T * W
        return torque*angularSpeed;
    }

    public static double getClearTorque(double timeTorque, double angularSpeed){
        //T = timeTorque / W
        return timeTorque/angularSpeed;
    }

    public static double getClearPower(double power){
        return (power*550)*12;
    }

    public static double getAngularSpeed(double speed){
        return (speed*(2*Math.PI))/60;
    }

    public static double getTorquePwASp(double power, double speed){ return power/speed;}

    public static double getTorqueMesh(double force, double meshRadius){
        //Indica el torque obtenido por la fuerza y el radio de un engrane
        return force * meshRadius;
    }

    public static double getForce(double torque, double meshRadius){
        //Indica que la fuerza en un engrane va a ser la misma fuerza en otro engrane
        return torque/meshRadius;
    }


    public static double getTorqueSum(double torque, double torque2){
        //Indica que la fuerza en un engrane va a ser la misma fuerza en otro engrane
        return torque + torque2;
    }


    public static double getSum(int [] values){
        double result = 0;

        for (int i = 0; i< values.length; i ++){
            result += values[i];
        }
        return result;
    }
    public static double getThorzionAngleRigid(double torque, double longitud, double radius, double rigid){
        return (torque*longitud)/((Math.PI/2)*Math.pow(radius,4)*rigid);
    }

    public static double getThorzionAngleRigid(double torque, double longitud, double externalRadius, double internalRadius, double rigid){
        return (torque*longitud)/(((Math.PI/2)*(Math.pow(externalRadius,4) - Math.pow(internalRadius,4))*rigid));
    }

    public static double getThorzionAngle(double meshRadius, double anotherMeshRadius, double thorzionAngle){
        return (meshRadius/anotherMeshRadius)*thorzionAngle;
    }

    public static double getExtremeThorzionAngle(double thorzionAngle1,double thorzionAngle2){
        return thorzionAngle1 + thorzionAngle2;
    }
}
