package com.app.appTorsion.functions;

import java.io.Serializable;

@FunctionalInterface
public interface ThorzionFunction<T,U,V,R> extends Serializable {
    R apply(T t,U u, V v);
}
