package com.app.appTorsion.opciones;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.app.appTorsion.R;

public class IntroduccionTorsion extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduccion);
    }
}